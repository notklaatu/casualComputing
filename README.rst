Casual Computing
====================

These are the source files for my 2016 book about open source (a
follow up, as it happens, to last year's `Computing Without Compromise
<https://gitlab.com/notklaatu/computingWithoutCompromise>`_).


Get It For Free
-------------------

If you want to read it, it's $0 from `Smashwords
<https://www.smashwords.com/books/view/686135>`_.


Build it From Scratch
-----------------------

If you want to build it from scratch, that's pretty easy,
too. Assuming you're using an open source stack (so, Linux or BSD, for
instance), you should install:

* `Pandoc <http://pandoc.org>`_
* some distributions also require a package called ``pandoc-data``
* `GNU make <https://www.gnu.org/software/make/>`_ (you probably alread have it) 

Clone this repository and build:

.. code::
   $ git clone \
   https://gitlab.com/notklaatu/casualComputing.git \
   casualComputing.clone
   $ cd casual*clone
   $ make epub

That's it.

If you want to build to other formats, you can do:

.. code::
   $ make book

Which simply concatenates all relevant parts to ``book.tmp.md``, which
you can use as the basis for whatever other kind of conversion you
please.

All of the text in the book is Creative Commons `BY-SA 4.0 <https://creativecommons.org/licenses/by-sa/4.0/legalcode>`_, and all
images are `CC-0 <https://creativecommons.org/share-your-work/public-domain/cc0/>`_.

Enjoy!

