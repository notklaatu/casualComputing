# Ode to Slackware

![](images/pipe.png)

I thought I'd sit down and write about why I like Slackware, because
that's one of those questions that people do ask you sometimes, and
that you even ask yourself, because running Slackware in the age of
Red Hat and the pop-age of Ubuntu does have weight. Whether you mean
to or not, you're taking a stand by running Slackware, so the natural
question is *why*?  Or maybe, to put words into mouths, the question
is in fact *why bother*?

Like everything else, Slackware is on a sliding scale. To a hip and
modern Linux user, Slackware is the most arcane system you could
possibly run, but to a BSD user it's just a clone and doesn't qualify
as literal UNIX. So I'm going to write, deliberately, about why I use Slackware
Linux.

I had a notion, initially, that I ought to just give The One Reason
for using Slackware. That's not to say that there is only one reason,
but I thought maybe I could settle on the one, most important, most
significant thing about Slackware that appeals to me.

Turns out, there isn't just one reason. There really are several
reasons, and one **one reason** leads to another **one reason**, and
pretty soon there are **one reason**s lying all over the place. So
let's dive in.

## The Classic Reason(s)

The trope about Slackware is this: "it's stable". Most people who say
that don't really understand what that means. Does it mean that the OS
never crashes? does it mean the applications running on the OS never
crash? and if so, what about Slackware makes applications never crash?
why don't they Never Crash on other distros? Or maybe it means you can
run it for 8 years without ever requiring a reboot? Or is it just,
*ya know*, stable?

It actually means none of these things. When I admit to people that, in
spite of it being trite to say so, Slackware is stable, I mean that
Slackware is predictable.

It takes a long time for Slackware to get assembled. I'm not on the
core team myself, so I don't really know for certain, but I suspect
it's because picking out a sensible release from 1481 different
projects, without picking releases too far back that they negatively
effect one group of users but not so recent that it's unproven, is a
pretty serious and difficult task. But that's what the core team
behind Slackware does; they survey the landscape, they see what kind
of new developments have been happening, what's getting updated,
what's changing, and then at some point in this cacophonous melange of
bits, they take a snapshot and put it on a disc with the intent that
the disc will be in use for the next 3 or 4 years.

It's not magic, they don't do anything terribly special to the
software they collect, they just do a whole lot of sensible, informed,
and street-smart configuring, adjusting, and testing.

In a way, I've just described any given open source unix
distribution. And yet, I haven't; a good number of distributions out
there focus on different aspects of software. Some aim to deliver
frequent updates so that the user feels like they're getting the
important latest versions of software. Others aim to deliver cutting
edge code so that developers have a sandbox with all the latest, and
potentially buggiest, features. Others are interested in delivering a
platform for a specific audience, so it may be strong in one area but
lacking in another. You get all kinds, and that's good. With
Slackware, you get a predictable, known-quantity of software that
appeals to a general user base without any bias toward any one group.

## Software and /Extras

What many modern Linux users forget (or don't realise) is that at one
point, the software you got on a Linux disc *was* your
repository. There weren't fancy online repositories with an
ever-rotating stock of updated software and new releases; you got the
wares on your disc, and you installed what you needed when you needed
it. If you wanted more, then you could get more elsewhere (like
SunSite, and other archives), but generally your *distribution* was
the disc you brought home with you from the computer store.

Slackware still works under that model, and it's downright
refreshing. I know that the 1400 packages I install from my install
disc are "official" Slackware packages. The core team and anyone brave
enough to run -current have tested these packages and are at least
nominally familiar with them. There's a sense of curation. The
software on the disc and in the optional stuff in the disc's `/extra`
directory are what make up Slackware.

You want more? You can get more from
[SlackBuilds.org](http://slackbuilds.org) and
[Slacky.eu](http://slacky.eu) and
[AlienBOB](http://www.slackware.com/~alien) and so on. But those
aren't Slackware.

Slackware is Slackware.

It's maybe a subtle distinction, but other distributions are harder to
pin down. Is distribution **Foo** the ISO you download and `dd` to
your thumbdrive, or is it that ISO plus 25,000 packaged applications
in its online repository? Do I believe all 25,000 packages have been
carefully tested and vetted? How does a user keep up with the changes
happening to all their personally essential software? What if I want
to update `libQuux` without also updating `libCorge`?


## Upstream

Hey, speaking of software!

Slackware packages software by doing three things:

1. Download the software
2. Configure the software according to the software's build options
3. Package the software

And that's it. Here are some of the things it doesn't do:

* Arbitrarily define requirements and dependencies when no runtime
  dependency exists
* Configure builds based on politics or whimsy
* Add scripts to intercept or interrupt a normal install
* Provide default post-install configuration
* Integrate advertising deals and search result payment plans such
  that users generate income for the distribution
* Bundle untrusted software at the risk of user privacy or
  distribution stability

And that's probably not even a complete list.

With Slackware, upstream is king. Not everything can be packaged
together in a pure "vanilla" state, because a lot of software out
there has lots of config options depending on what other software it's
being bundled with, but generally Slackware ships what it gets from
upstream sources.

This is huge, for me. I've used many distributions (and remember, I'm
including both Linux and BSD, here) that provide "processed"
software, as in *hot dogs* instead of the whole cow. Sometimes that's
nice; I'll admit that installing something and actually being able to
launch it without reading hundreds of pages of documentation on how to
fill in its configuration files can be nice, but at the same time the
burden for that experience really is upon the upstream project. It's
nice that some people make it easy *post facto*, but I'd much rather
have add-on scripts that I could run as needed rather than defaults
that I might not expect or want.


## Packaging

I'm handy with RPMs, and even handier with `rpmbuild`, and I quite
like the format in general. Macros make the packaging process easier,
and the RPM spec file is clean and intuitive.

But let's be honest: POSIX was built for scripting. Anything you can
do manually on a POSIX system, you can script, and you can share that
script so other people can do what you did. That's the true beauty of
POSIX, and it's the very thing that lies at the core of the Slackware
user experience, from top to bottom.

The `SlackBuild` package format (such as it is) involves a
loosely-standardised (by example) shell script that unpacks,
configures, and builds software downloaded straight from upstream. If
*you* do it once, then you can do it 100 other times, and 100 of your
friends can do it 100 times. It's simple, it's de-centralised, it's
lightweight and scales 100% according to the computer it's run on, and
it works. As a package maintainer for
[SlackBuilds.Org](http://slackbuilds.org) and for
[Slackermedia](http://slackermedia.info), I can attest that it works.

The great thing about this model is that the user can modify the build
script with almost no effort. Even less effort, if you write the
script to accept a few key variables. It's all done in BASH, so the
barrier to entry is that you use Linux. What all of this means is that
there are no arbitrary configurations. I can install new software with
complete custom options, and I can even re-install stock software by
revising Slackware's buildscripts and re-building. It sounds complex,
but it's shockingly simple.

The scripting doesn't stop there; the tools that install, upgrade,
remove, and track the software are all written in BASH and are not
just easy to hack but even portable (I've ported them myself).

## Track Record

I might make mention of Slackware's long history. It's been built and
distributed by the same guy since 1992, without interruption, and
without any major change. I booted into [Slackware
1.01](http://www.qemu-advent-calendar.org/2014), Slackware 3.4, and
14.2 over a weekend, and outside of `pkgtools` there was basically no
difference in Slackware core.

To be fair, the same general statement can be made about POSIX in
general; it has a pretty good track record and largely has remained
unchanged in how it operates, at least at its core. But there has been
some major tech that has come and gone over the years, whether it's
sound or init systems or package kits or desktops. You just don't get
that with Slackware.


## Paid

I don't actually like money, and I could go on and on about how money
affects things and how humans interacting with money affects things,
but I'll stay on topic here and just say that I actually *like* that
Slackware offers a subscription plan. I like that when I want to give
money to Slackware, I can. I can donate, or I can purchase something
from the store. I have the option to do that.

The reason I like this is because some people do have money, and
everyone needs money. I'm not saying this is good, in itself, I'm just
saying that realistically, the people building Slackware have to eat
(and buy computers, and pay for electricity, and so on). To that end,
I like to be able to contribute, when I have the ability to do so,
monetarily. Doubly so, since Slackware is the foundation upon which I
have built my own income; if it weren't for Slackware, my employment
for the past 6 years would have looked a lot different. Sure, I could
have built a career on CentOS or Mageia or SUSE or some other stable
distribution, but Slackware is what I chose early on, and that's what
I use. So I owe it something, and I like to be able to support it.


## Unix

And finally, Slackware's stated mission is to be "the most UNIX-like
Linux distribution out there". I'm a fan of UNIX. I like UNIX. And
this is a bold and possibly heretical statement, but in my opinion
Slackware out-UNIXes some of them UNIXes out there (notice how I'm
politely refraining from naming names).

The devil is the details, and Slackware's details are really nice.

[EOF]

Made on Free Slackware.

