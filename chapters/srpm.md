# Source RPMs




The benefit of running [Red Hat](http://redhat.com) or
[CentOS](http://centos.org) or [Scientific
Linux](http://scientificlinux.org) as a desktop is that you get a
great Linux distribution with long-lasting support and a stable life cycle.

The disadvantage is that you don't have nearly as many installable
packages to choose from. I'm talking, a mere sub-set. You can install the EPEL repository and try to limp by, but eventually you'll hit a wall, trust me.

So, you learn to build your own RPMs, but not from scratch! You can grab perfectly well-formed *source* RPMs from Fedora, re-build them for your machine, and install. It's three steps.

1. Install the RPM development toolchain.

        # yum grouplist -v
        # yum groupinstall fedora-packager

2. Find the package you want to install in either the Fedora package database at [apps.fedoraproject.org/packages](https://apps.fedoraproject.org/packages/) or in the RPMFusion repositories: [download1.rpmfusion.org/free/fedora/releases](http://download1.rpmfusion.org/free/fedora/releases) and [download1.rpmfusion.org/nonfree/fedora/releases](http://download1.rpmfusion.org/nonfree/fedora/releases).

    Download the source RPM you want. It needs to be a *source RPM*; otherwise, yum will complain that it's not an RPM meant for your system. Source RPMs generally use the `src.rpm` label in the file name.

3. Rebuild the SRPM:

        $ rpmbuild --rebuild rare-nonfree-package.fc24.src.rpm

4. Rebuilt RPMs end up in your user's `~/rpmbuild/RPMs/` directory, in the appropriate architecture:

        $ sudo yum install rpmbuild/RPMS/`uname -m`/rare-nonfree-package.centos7.rpm


OK, I lied, it was four steps (but only three need repeating after you have the first step complete).

Either way, pretty easy.

[EOF]

Made on Free Software.

