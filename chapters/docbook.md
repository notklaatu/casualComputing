# Joy of Docbook




When I discovered the wonderful world of editing text without a word
processor, I felt like I'd "come home", as they say, in the spiritual
and emotional sense. The earliest writing experiences I had, on a
computer at least, had been on a non-GUI text editor, using markup to
indicate style and formatting. When I took "computer literacy" courses
in school, I used word processors, and then continued to use them
later, but I think deep down I never felt that word processing really
made much sense, especially since opening a document in a different
word processor always seemed to require re-formatting, and word
processor upgrades often broke things too. Furthermore, the text
always seemed married to the format; a word processors seem to assume
that if you wrote and designed a document for A4 paper, then you will
never ever want to output that document to any other paper size or
media. Ever.

Eventually, I discovered `vim`, and later `emacs`, and I have never
looked back. I'd successfully liberated (in every sense of the word)
my text from inherent formatting, and that alone cut my workload
down. Never again would I have to manually re-format content, because
I was writing in plain text.


## Markup and Markdown

Turns out plain text will only get you so far in life. Well, that's
not true; plain text is the foundation upon which the very civilised
computing world rests, but saying to use "plain text" is really only
telling part of the story. Plain text is a lot more work if there is
no consistent structure to it. Early in the lifespan of ebooks, back
when there were no ebooks, people would make electronic books and post
them as plain text online, and very often each person would have their
own peculiar style. That's great, until you re-visit your collection
years later to convert them into a modern format like EPUB and find
that no two books use the same convention to mark chapter breaks, and
some break pages to emulate the size of the book it has transcribed
while others had no breaks, and so on.

So the computer experts came up with markup languages.


### XML and Docbook

When I learned about Docbook, I fell in love with it instantly. An
HTML-like markup language that produced text that you could run
through any number of processors and end up with all your original
content in nearly any format you could ever want: HTML, plain text,
ODT, EPUB, you name it.

When you get involved with Docbook, you enter into a serious
relationship. Docbook is a frequently-marked language; there's a tag
for everything. Paragraph breaks, links, images, not two but *four*
list types, sections, simple sections, chapters, books, metadata, and
far too much to even mention. It can be a little overwhelming.

And if you get the markup wrong, Docbook refuses to process. This
isn't HTML, this is strongly-typed XML. I'm not just talking about
mistyping a &#60;para&#62; tag, which will cause Docbook to fail, or
even forgetting to close it, which will also cause Docbook to fail,
but getting the parenting of elements (putting a &#60;simplelist&#62;
outside of a &#60;para&#62;, for instance) will break Docbook. It is
utterly unforgiving.

And the toolchain for processing Docbook documents can be pretty
heavy. There are several choices, and none are particularly light
(although `xmlto` seems pretty reasonable to me). But just because
it's "just plain text" does not mean it's a walk in the park,
especially if you're authoring a 300 page book with images that need
re-sizing and footnotes that need resolving, and styles that need
applying, and so on.

So why am I still in love with this Docbook thing, especially with the
rise of Markdown and other up-and-comers? Well, before we investigate
that, let's look at the alternatives.


### The Markdown Agenda

Docbook has been around for a very long time, and longer if you count
its earliest implementations prior to even being known as
"docbook". Nobody argues that it isn't complex.

Presumably, in an effort to provide a simpler way to utilise the
universality of plain text in conjunction with the power of
consistently parse-able formatting, the idea of *markdown* got
popular. And we're all very happy that it did.

There are a few varieties of "markdown" languages. There's *markdown*
itself, with its admirable goal of just bringing consistency to plain
text. As long as you keep it basic, markdown proper is nearly
intuitive. It's almost difficult to not write it. It mostly looks the
way you would write a plain text document anyway.

And yet, it doesn't. Bizarrely, there are things that markdown
dictates that just doesn't come naturally at all, by which I mean no
one in any plain text doc every did it that way. The headings, for
instance, are preceded by hashes, and links to images are preceded by
an exclamation mark, and code blocks are simply indented more like a
blockquote than a code block.

There are other "markdown" languages, though, which improve upon the
original. Github, in fact, has its own modified version of markdown
which seeks to augment the syntax, but you could argue that it's
geared strongly toward Github, which isn't exactly a super-common
target so much as it is, well, version control hosting.

I recently learnt from my friend SoundChaser that
[Pandoc](http://pandoc.org), God's *actual* gift to the world of text
conversion, has its own markdown implementation, as well, called
[Pandoc's Markdown](http://pandoc.org/MANUAL.html#pandocs-markdown)
and I don't exaggerate even one bit when I say that the initial
inventors of Markdown should acquiesce immediately by removing the
markdown "spec" from the Internet and 301'ing the pages to Pandoc. Everything that Markdown lacks, Pandoc Markdown accounts for, whether it's the inability to have code blocks after a list without making the code block part of the list, or having internal links, or something more obscure, Pandoc Markdown provides not only the spec but also an amazing parser.

If you're going to use markdown, use Pandoc's Markdown.

Similar to Pandoc's Markdown is Restructured Text (rST), used in
several Python projects, including the all-in-one doc package,
[Sphinx](http://www.sphinx-doc.org), easily the best markdown-based
replacement for Docbook available.

Restructured Text uses far more intuitive conventions in some places
(underlines to mark chapters and sections, for instance), simple
markup for code blocks, but then deviates into obscurity for URLs and
xref- or xinclude- style cross-references, string substitution, index
terms, and things like that. When you have to break out a reference
guide to know how to write your "natural" markdown text, it stops
being markdown; it is now certifiably *markup*.

Complaints aside, markdown systems are really neat, and I do
appreciate them and I do use them. They enforce a standard format on
otherwise plain text so that conversion is possible, and yet they
maintain the readability of plain old plain text.

But that's the funny thing about markdown. The reason markdown is cool
is that markdown imposes order on text for better parsing. Like, ya
know, Docbook. Only, less explicitly and with a lot less clarity.


## When Lenience is Strict

Maybe we're not yet back to Docbook, but there's a definite problem
with markdown. It's a lenient format, since it pretty much defaults to
outputting as plain text anything not marked down to something
specially styled. That means that as long as you type a plain text
document, even if it has a bunch of incorrectly-indented code examples
and unmarked section headings, your markdown processor will output
what you typed as unstyled run-of-the-mill text. It defaults to
success, even in the case of failure. And that's darned user-friendly.

But the danger to markdown (rST included) is in its
permissiveness. You're free to write in structure, or outside of
it. When I write it down that way, it seems like a choice that you
consciously make. But what happens more often is that you *believe*
you are writing in proper structure, but have accidentally deviated
from it. And guess what happens after you render it to your fancy HTML
page or EPUB or ODT document? All of that pretty formatting you
thought you were doing is gone or completely different than what you
intended. All because you forgot to indent properly, or because you
forgot to add a line break, or whatever.

And what about finding and fixing those problems? You more or less
have some idea of where the breakage is happening: it's near the part
where all your formatting gets messed up. But what exactly is the
problem? More often than not, "fixing" a markdown problem is a series
of blindly adding whitespace in hopes of appeasing the indentation and
line break police. List not behaving the way you want? try adding some
spaces. Didn't fix it? remove that line break. No? OK what if we just
get rid of the code sample in the middle of the list. Nobody needs a
code sample anyway, right? Still no? OK, let's just manually code it
in HTML.

And yes, 7 times out of 10, I think that's been the fix for most
markdown issues: use HTML instead. You can do this because markdown
will happily process non-escaped HTML. Yeah, so markdown's solution
for complex formatting is, essentially, to fall back on HTML, an
XML-based markup language.

You see the irony: in markdown's attempt to eliminate the need for
"complex" markup syntax, it confirms the need for markup syntax.

Another problem with markup, especially when used for large documents,
is that it fails silently. Believe me, that's nice at the time of
conversion, because, darnit, it just works. That is, until you send
the book off to the printer, or you upload it to your site, or push it
to your distributor, and then you forget about it until a month later
when all the bug reports start coming in. Why are these weird
characters at the start of each chapter title? why are there asterisks
around this one word? why did all the lines in this one code example
run together? and so on. Yes, errors are frustrating but it's better
to know about them *before* shipping your product.

## Docbook

The Docbook method is nowhere near as gentle as markdown.

It requires you to know lots of markup keywords, and, worse still, the
*proper order* those keywords are allowed to appear when you write. It
argues with you if you deviate from its syntax.

But it tells you where you went wrong. Sure, sometimes it points you
to line 432 when the beginning of the error actually starts on line 4,
but if you're using a good text editor with folding or syntax error
detection, you'll find that. And in a pinch, you can just keep
scrolling up, counting "open/close" tags until you find the tag you
forgot to close.

The important thing is, Docbook won't let you convert a broken
document, meaning that your audience won't be finding your formatting
mistakes. You will.

Docbook is also a complete solution. You won't find yourself falling
back on HTML when you can't get lists to cooperate, because Docbook
specialises in structure. You can style it anyway you like; the
*structure* of the document will be perfect no matter what.

I guess what Docbook really delivers, at the end of the day, is
*clarity*. I know it sounds deluded to look at XML and say "there,
look at how clear it is!" but the truth is, that's exactly what XML
provides. Where does a paragraph *really* end? look at the closing
tag. What's part of a code block and what's part of an admonishment?
look at the tags. Is it one asterisk for bold or two? don't use
asterisks, use tags.

The list goes on and on.

But don't get me wrong. I use markdown in real life, and I sincerely
appreciate it (I don't mean that in a patronizing way; I am an actual
markdown and rST user). This article was written in markdown and
converted to HTML with a shell script and Aaron Swartz's markdown
converter. There's definitely a place for markdown in this world,
because there are absolutely times where Docbook could be seen as
overkill (or it might not be; depends on the user and the workflow and
personal preference). But if we're recognising limitations in one then
we should recognise limitations in the other. What those limitations
are depends on your use-case and your tolerance levels for
inefficiencies in each.

For me, Docbook is a great solution for authoring content. It's a
comfortable system that I enjoy.

Use Docbook if you want your document to be clearly and cleanly
structured, and if you want that structure enforced. And if you use something else, when you sit back in bewilderment as your document is rendered incorrectly, then think of me when you say under your breath "shoulda used docbook!"

[EOF]

Made on Free Software.

