# Solo RPG and Tabletop Gaming




It's one of those unsolvable paradoxes; was I lucky to have gotten
into tabletop gaming after I'd acquired a gamer girlfriend, or did I
finally get into tabletop gaming only *because* I got a willing
partner? As an extension of that question, what would I have done had I
decided to explore tabletop gaming prior to finding a steady partner?
could I have still explored the hobby?

This all relates to possibly the most basic and important RPG puzzle
of all: what do you do when you have no one to play an RPG with?

Well, as it turns out, yes, there are some "solitaire" equivalents out
there in the fancy tabletop game market. There are also games that are
meant for two or even more players that you can, in a pinch, adapt for
solo play just by stepping in as both or all players. Is playing a
solitaire game as fun as playing a game with others? well, I guess it
depends on your ability to enjoy time spent alone, in your own head. I
personally love it. I'm quite happy to privately obsess over a
gameworld, and the characters I build, and their personal development
through the encounters that they have in a game world. I don't need an
audience to enjoy that, I don't need other people to validate the
game, and I don't necessarily need the camaraderie to enjoy a fantasy
world. Solo tabletop gaming is definitely a viable option for me.

On the other hand, those other-player perks are nice to have,
sometimes. So I'm not saying that solitaire is the *only* way to
game. Not only do other players add a social dimension to the game
experience, they also add degrees of entropy that AI and truly cannot
approximate.

With that said, here are some games I've found to be really good for
solo tabletop gaming, and also some tricks and ideas for converting a
two-player game down to a single-player one.


## Dark oCCult

One of the problems with solo RPG is that one of the main game
mechanics of RPG is the active narrative. The players around the table
each speak aloud what their character is doing, and what each player
speaks aloud determines how the game goes.

You could narrate your solo game, but your narration doesn't affect
anyone else (because there is no one else around), and your narration
is not structured by anything external of your own head. Even if you
write down two paths in which your story could go, and then roll dice
to determine which path you have to take, you've still only made a
choice between possible narratives from your own mind. There's been no
external influence on your "game". In short, there's no game happening
here, it's just a really inefficient way to write a short story.

So how do you turn this process into a game?

The first solo-tabletop game I ever played was my own personal
[Creative Commons](http://creativecommons.org) revival of an
out-of-print 1983 card game called [Dark
Cults](https://gitlab.com/notklaatu/darkoccult). The game is long
forgotten, but there's a site somewhere out there on the internet that
had posted scans of the original deck and rulebook, so I re-created
the deck with free art assets and transcribed the rules, including the
single-player mod.

The game is a story-telling game, and in a way, you're actually
playing as the GM (game master) to a non-existent role player. I guess
if you really wanted to, you could play as the character, narrating
the game in the first-person, but I just play as the GM, telling the
story of the character.

In a nutshell, the game play goes something like this: you imagine a
main character or draw from the pre-built character deck, then you
start drawing from the main story deck, structuring a storyline around
the cards as they happen. Eventually, the cards demand that you draw a
Threat card, which may lead to a neutral or an evil encounter with
some Character card. An encounter with evil always leads to an End
card (if you're brave) or to a Save card (if you're conservative):
these don't end the game, they just end that "chapter", meaning that
your character either escapes or dies. Assuming your character has
escaped, you start a second storyline, continuing your character's
journey through the dangerous world.

The game is nicely self-contained. It's a big stack of cards (which
get split into five or six decks, so it does require table space), and
that's it. No dice, no tokens, no stats. It's just the cards and your
imagination.

It's the perfect single-player game. It's atmospheric, it's easy to
play but requires enough imagination to impose a story onto the cards
you draw, while providing just enough guidance and randomness to
prevent you from just sitting around inventing a story in your
head. As long as you enforce some basic minimum-sentence requirements,
it never becomes mindless card-drawing, either; it does force you to
put thought into justifying how the adventure is taking place.

The cards are the obvious thing that makes the process a game; they
provide prompts, restrictions to what your character can do, and
unpredictable results to even your best efforts to spare the life of
your sometimes over-adventurous character. But there's some free will,
here; you can always gamble on how far you want to tempt fate. There's
always the Save card that you can fall back onto, but you'll probably
avoid that, because it's a 100% guarantee that your character survives
the night, and who wants 100% guarantee? More likely, you'll tempt
fate and play cards right up until an evil encounter, at which point
there's a 50-50 chance of survival. It's just thrilling.

### Problems

In terms of pure RPG, it's the binary randomness of Dark oCCult that
could be seen as its greatest weakness. You don't build your
character, you don't actually build your character, and there's only
three ways the story can end: your character is Saved and goes home
for the night, or your character either Escapes or Dies, depending on
a single card draw.

Is that any different than dice-controlled combat?  not really, it's
arguably just a more efficient way to the same two possible
outcomes.

Does it feel less RPG than dice combat? yes, it feels very
binary. Sometimes you feel especially cheated when a character is
armed with a gun and yet still gets overcome by an escaped convict.

Is that a problem? Well, not really. Life happens in funny ways, and
the way I see the 50-50 split of the End card deck is that it reflects
how real life actually plays out. I think we like to think that life
gets determined based on stats and dice rolls and bonuses, but I've
found that the 50-50 card deck feels a lot more realistic, in the end.

### Mods

At the time of this writing, I am slowly working toward an optional
mod of Dark oCCult that will incorporate more RPG elements; I already
incorporated the pre-built characters (adapted from the
[OpenD6](https://ogc.rpglibrary.org/index.php?title=OpenD6) adventure
rulebook), so there are characters with defined skills and backstories
to start things off. It's not too hard to assign those characters
stats, to attribute stats to evil Characters, and then allow the
player to determine the outcomes of encounters with dice.

A more obvious mod is to create new cards. Sure, **Dark oCCult** has
12 locations (11 from **Dark Cults** and one that I added), so why not
make 4 or 8 or 12 more for yourself, and swap out location sets (you
can't just throw in new cards, or else you'll throw off the ratio, so
swap cards out instead). Create an alternate set of Atmosphere cards,
or Object cards.

Better still, as proposed by Kenneth Rahman (the creator of **Dark
Cults**) himself in the original rulebook, why not create an alternate
deck entirely? His original deck was based on a kind of Horror Movie
setting; an AnyTown setting with the usual assortment of urban bad
guys. What if you made a deck filled to the brim with science fiction
story elements on an alien planet? or a fantasy deck? or a crazy mad
scientist deck with a crop of 50's B-movie tropes? or how about a
nonsensical cartoon world? The possibilities are endless.


### Verdict

Dark oCCult is a perfect single-player game; it inspires you to be
imaginative, it's got the perfect mix of game-to-story ratio, and it
scales to 2+ players when you have friends around.

## Combat Heroes

Not counting vicarious observations of my friends back in elementary
school, the first RPG material I personally owned were two paperback
books called [Combat Heroes: Black
Baron](https://www.projectaon.org/en/Main/BlackBaron) and [Combat
Heroes: White
Warlord](https://www.projectaon.org/en/Main/WhiteWarlord). I couldn't
figure out how to play them and had no patience for reading the rules,
so I mostly just looked at it and let my imagination run wild; I lost
track of the books but never forgot them.

It turns out that there are four books in the **Combat Heroes**
collection, in sets of two pairs. They can each be played as a
simplified solo game, or they can be played with two players, with one book
being played against the other.

The book is light on narrative, consisting mostly of full-page
illustrations of corridors; the story is that you're in a maze, and
must navigate through it to find 9 treasures (or kill your opponent,
in two-player mode). It looks very much like a storyboard for a video
game, and amazingly it plays like one, too.

It's a brilliant design with surprising flexibility in mechanics;
between the solo and two player games, you can move, hide, attack, and interact
with objects to find treasure. The complexity somewhat boggles the
mind, and it's actually completely playable with a good PDF viewer
like [xpdf](http://www.foolabs.com/xpdf/home.html), which has single
page turns and page turns in increments of 10, so you can move back
and forth between pages quickly as you navigate the maze, look up
riddles and results, and so on.

### Problems

The solo game is, admittedly, more limited than the two-player
mode. You move through the maze, solve riddles to collect treasure,
sometimes endure traps, and that's pretty much it. It's not as complex
as the two-player game, and I don't see any comfortable way to play it
as a two player game aside from flipping desktops from one PDF to the
other. Granted, the books are complex enough that this tactic might
actually work; but it would obviously change the dynamic and become
far more complex, since you'd be managing the stats and inventory of
both characters, plus their location on the map, and so on. Not
impossible, but it might become a lot more about logistics than
fantasy, which you may or may not want from a game.

There's not much replay-ability as a solo game; once you experience
the maze and riddles once, you might go through it a second time just
to see what happens if you play it a little differently, but there's
no AI and all choices are scripted, but there are four books and each
of them playable at $0 at this point, so they're worth trying at least
once each!

### Verdict

**Combat Heroes** is an interesting experiment in analogue visual game
design. As a solo game, it lacks AI, so you probably won't play it
again and again, but it's a lot of fun once or twice as a solo game.



## Lone Wolf Saga

While I was scouring the Internet in an effort to re-discover **Combat
Heroes**, I stumbled upon [Project Aon](https://www.projectaon.org), a
concentrated and thorough effort to preserve and revive the works of
game designer Joe Dever (and collaborators Rob Adams, Paul Bonner,
Gary Chalk, Melvyn Grant, Richard Hook, Peter Andrew Jones, Cyril
Julien, Peter Lyon, Peter Parr, Graham Round, and Brian Williams). It
turns out that Dever not only created the **Combat Heroes** game,
but also a whole series of RPG books called the **Lone Wolf** saga,
and they're all available for download! (As EPUBs, no less!)

The Lone Wolf books (and a few books set in a different universe, but
using the same mechanics) are, essentially, an RPG mod for
Choose-Your-Own-Adventure (or maybe the other way around; not sure
which came first). That's an over-simplification, though, because each
book is complex enough that you can progress through it at least twice
and get unique progression, if not a wholly unique storyline.

The mechanics will feel familiar to any RPG player. You are the last
of the Kai warriors, known as a the Lone Wolf; in the beginning of the
book, you get stats (by using the random number table provided in the
back of the book, but I just use a d6 or Python
`random.randrange(0,10)`), you pick your weapons, and then start your journey.

The journey progresses much like a MUD or text adventure game; you
navigate through paragraphs by following prompts. This obviously
limits the story a lot; if you want to go North at an east-west
crossroads, you can't, because there's no text for that. On the other
hand, you can go east or west, and you can only do one of those things
each play-through, so some replay-ability is inherent. If you don't
believe me, then refer to Project Aon, which has provided proof in the
form of charts to show just how divergent each book can become. Yes,
they all share a common entry point and all successful paths
eventually lead to a common ending, but since getting there is half
the fun, you can rest assured that each Lone Wolf book has at least
three or five main branches of play. There'll be repetition,
obviously, but that's true of any game upon re-play; it's the variety
that counts, and with over 20 books (with at least three story
branches each) in the series, you have *plenty* to work with.

Combat encounters use your stats; your enemy's stats are provided, and
you battle in the usual RPG way, calculating hits and damage and so
on. As with any analogue solo game, there's nothing to enforce
anything you do, but I assume that you want to game and not treat the
story as a less interactive Choose-Your-Own-Adventure book.


### Problems

The only problem with the Lone Wolf saga is the same as any game with
static content, analogue or otherwise; there are only a a finite
amount of stories being told, and you can only explore that which the
content creator thought to write down for you. It's no more a
deal-breaker than a linear video game.

This highlights, for me, one of the strengths of the **Dark oCCult**
game; the narrative comes from your head, so you *can* explore
anything in any direction. But even with **Dark oCCult** there is a
finite set of cards, which eventually will get "old" no matter how you
dress them up with new story ideas.

### Mods

I can imagine, in a world where I didn't have a day job, writing
expansions for some of these books. What if, at that east-west
crossroad, a player *did* go north? What would the player find? what
if there were objects in each and every location that the player could
pick up, examine, or use?

Alternately, there's the ultimate mod: write your own adventure, and
form a community of other solo players who write these adventures, and
trade amongst yourselves.

### Verdict

The **Lone Wolf** and **Freeway Warrior** books are priceless. They're
fun, inspiring, imaginative, and true to the RPG spirit.


## Tunnels & Trolls and Dungeon Delvers

**Dungeons & Dragons**, having been the first on the scene, at least
in my mind, the standard by which any other game system is
measured. **D&D** rules are familiar, multi-faceted, the basis for
several video games, the progenitor of the Open Game License, and very
very complete. Even if you've never played **D&D**, you're probably at
least a little familiar with **D&D** rules whether you realise it or
not. And if you're playing **D&D** and have a question about how the
gameworld works, you can rest assured that there's an authoritative
answer out there.

Then again, you don't get that kind of coverage without accepting that
your Core rulebook is going to be hundreds of pages, plus all the
extra modules you can dream up. **D&D** is complex.

Sometimes I like complexity when playing solo, because it provides me
something to really sink my teeth into (see, for example,
**Dungeoneer**). There's a certain feeling you get after you've spent
a week or two reading over the Core rulebook, and then designing your
character, and then you sit down to play through a campaign. I think
the technical term for the feeling is called "obsession", and if you
that's what you want, and you can afford to invest that much time and
thought into a game, then a properly complex system is
perfect. However, if your aim is to just sit down and kill two hours
without any real investment, then you might benefit from a simplified
rule system.

Shortly after **Dungeons & Dragons** appeared on the scene, there came
a game called **Tunnels & Trolls** which was, apparently, a
deliberately simplified alternative RPG rule set. To this day, that
does seem to be the main appeal of **T&T**: simpler rules and possibly
a dash more whimsy. For whatever reason, it also *seems* (to me, at
least) that solo games are firmly and historically embedded into
**T&T** culture, more-so than in **D&D**.

If you're familiar at all with **D&D** rules, then **Tunnels &
Trolls** is a breeze to pick up. If you're new to RPG rule systems (or
you've only ever encountered them through an RPG video game), then
**Tunnels & Trolls** is a really nice and gentle introduction to all
the typical concepts. **T&T** provides just enough stats and inventory
management to keep you engaged, but not so much to turn the game into
an exercise in accounting.

To a hardened **D&D** player, these rules may seem at first
over-simplified (what do you mean there's no attack-of-opportunity?),
but especially as a solo game, I think the simplified ruleset makes
the game easier to sink into.

There are two ways to get **Tunnels & Trolls**. To get a feel for it,
you can grab the abridged [free
edition](http://www.drivethrurpg.com/product/54407/Tunnels--Trolls-Free-Rulebook?manufacturers_id=2238)
that was given away on the first [Free RPG
Day](http://freerpgday.com), which is enough to get familiar with the
basic rules, and includes a solo game for you to play.

Or you can purchase the latest rulebook as a
[PDF](http://www.drivethrurpg.com/product/152613/Deluxe-Tunnels--Trolls?affiliate_id=289839)
or a hardcopy from any book store with a good gaming section.

I started with the free rules, and expanded to the paperback almost
immediately, because it's just that good. I say it's "just that good"
because it really does have, for me, the right ratio of accounting and
management to imaginative fun. I don't have an issue with more complex
rules, like **Shadowrun** or **D&D**, but there's a place for
simplification, too, and certainly there's always a place for *well
and clearly written* rules.

The ultimate appeal, though, is the proliferation of solo adventures
written specifically for **T&T**. There's not really a go-to location
for **T&T** content, so you'll have to look around the Net for them,
and you'll probably have to settle for whatever format you happen to
find. I prefer EPUB because it's a lightweight text-based format that
I can read on anything (e-ink reader, computer, mobile) but these solo
adventures come in every shape and size; some are PDFs generated from
an office application, others are PDFs scanned from old
publications, others are interactive websites, and so on. So you'll
have to wade through the different offerings and find what works for
you, or else be adaptable.

A few places to start your search:

* [drivethruRPG.com](http://www.drivethrurpg.com) features a few $0 [Trollzines](http://www.drivethrurpg.com/product/99363/TrollsZine-4?src=also_purchased) that happen to bundle solo adventures in them, plus a whole (virtual) bookshelf of [T&T paraphernalia](http://www.drivethrurpg.com/browse.php?filters=0_0_44297_0_0) to purchase.

* [freedungeons.com](http://www.freedungeons.com/) is a site with a lot of **T&T** content and some links to other sites offering T&T solo games. Much of this stuff pre-dates EPUB, and rather assumes that you are online looking for a good time.

* Not surprisingly, the publisher of **T&T** themselves, [Flying Buffalo](http://www.flyingbuffalo.com/solo.htm) have solo games for sale.

There are others, I'm sure, and it wouldn't be impossible to write
your own and trade with friends, but those are the sources I know
about.

### Dungeon Delvers

The RPG culture has, at least as far as I could ever tell, long been
based on sharing and modding. Like a lot of other casual bonds between
community and corporation, though, questions eventually arose about
just how much of a game the publishing company owned and how much the
community that supported it could claim as theirs.

The [Open Gaming
License](http://www.opengamingfoundation.org/ogl.html) helped define
some of that, and several games also get licensed as Creative Commons.

Both are great licenses. Depending on their exact terms, they, at the
very least, ensure that a game can be self-contained unto itself.

With the most liberal Creative Commons license available, it's quite
possible that you can buy (for $0, in some cases) a game and you
always own that game *plus* all the intellectual "dependencies"
cascading from it. In other words, if I develop a solo game adventure
for a Creative Commons gaming system and post it online, then anyone
who downloads it not only owns the solo adventure, but also the game
system it was written for, and the right to share those with your
friends, and to modify and re-distribute them.

It's a somewhat esoteric distinction, because if you buy any rulebook
and memorise the rules, you can teach other people how to play it, you
can develop house rules, and you can invent your own adventures for
it; looks and feels identical to Creative Commons. Then again, digital
downloads of music looks and feels the same as recording songs off the
radio or making a mix CD, but it's been drummed into people's heads
that it's evil and sinful. The same could easily happen to tabletop
gaming, if ever enough money got involved. It's nice to have licensing
that explicitly identifies what players and content creators are
expected to do with what they buy, and indeed whether or not they may
in turn sell the work they do based on someone else's game system.

There are several games out there that release under Creative Commons,
but one of my favourites is [Dungeon
Delvers](http://brentnewhall.com/games/doku.php?id=games:dungeon_delvers)
by Brent Newhall. It's pretty close to **Tunnels & Trolls**. In fact,
of the 8 character attributes in **Tunnels & Trolls** 7th Edition, six
are also in **Dungeon Delvers**, so it's trivial to "port" a solo game
written for **T&T** to **Dungeon Delvers**, if you either prefer a
Creative Commons base or simply cannot afford a current **T&T** rulebook.


### Problems

Solo adventures written for **Tunnels & Trolls** (or **Dungeon
Delvers** or any other lightweight rule system) have the same
"problem" as **Lone Wolf** or any scripted adventure: there's limited
play-ability, especially in the mini adventures. You play it twice,
maybe thrice, and then you shelve it.

This isn't a deal breaker, it's just something to keep in mind.

### Mods

You could write your own adventures.

### Verdict

An easy RPG system, it might be nice for new RPG players, and
certainly it's nice for those times that you don't want to spend all
your free time dealing with rules and exceptions and lots of stat
management, especially since in a solo game you're just playing
against a script and a die roll. **T&T** is a fun and whimsical
system, good for any RPG player.


## Non-Solo Games

Those are the RPGs I know about that are intended to be played, at
least optionally, with a single-player. I'm sure there are many more
out there, it's just a matter of finding them!

Another option available to the solitary player is to play a game
intended for two (or more) players in some modified form such that it
plays as a single-player game. I've tried this, to some degree, and
did not enjoy it, but I know that some people do. I don't tend to do
this, so I'll only discuss it broadly.

### Mechanics and Game Design

It seems that some people can separate themselves from the thrill of
being "in" the game, and just enjoy the game itself, from either a
story-telling or a purely mechanical perspective. If you're one of
those people, then you can probably adapt nearly any game to
single-player; just play both sides and revel in the way the game
progresses.

The great thing about an RPG, like the **D&D** board game, is that the
game and even the story is only *part* of the experience. The other
half of the fun is the character progression and the score keeping, so
if you imagine a really good party of adventurers, with different
motivation and intriguing backstories, you can play the game and enjoy
every last bit of the story, even over one or two re-plays, and just
play the development of your characters. You'll have to play GM, all
party members, and the enemies, but that's not all that different than
what a novelist does during writing. I think there's a lot of
potential for fun and creativity, as long as you approach the game as
a story rather than approach it as just a game.

Since the RPG community tends to be so darned creative, there are
plenty of pre-written campaigns and outlines for **D&D**,
**Pathfinder**, and any other system you can find.

I do think that a card or board based structure is important for solo
play, though; if it's a pure RPG (a rulebook and a campaign, and
nothing more), then there's no structure imposed upon you, so there's
no rhythm or, dare I say, ritual. You're just sitting alone in a room
imagining fantasy battles, but your mind starts to wander, and your
story structure just starts to fall apart. If you have cards or a
board with tokens, then structure is imposed on your imagination, and
things stay a little more balanced.


### Cooperatives

Related to the idea of playing all party members, any game that
concentrates on cooperative play is great for solo gaming. In such a
game, the players work together against a built-in AI enemy, so
condensing two or more players into one is a pretty natural
option. I've seen the co-op game **Mansions of Madness** played in
single-player mode and it works quite well.


### Solitaire Mods

The first time I heard of "mods" for board games was in idle
conversation with an experienced tabletop gamer; I asked if a specific
game required more than two players, and he just shrugged and said
"Yes, but just look up two player mods online", and blew my mind. The
idea that games could just be "modded" by any random person online
(much less *myself*) had never even occurred to me, and now I think
it's one of the most persuasive reasons to play tabletop games; if
something doesn't work for you, change it!

Turns out, if you want to play a game as a solitaire game, chances are
someone else out there wanted to do the same, and probably posted
their new rule ideas online. For instance, **Gloom**, which is not an
RPG but does take inspiration from **Dark Cults**, can be played as a
solitaire game, as detailed on
[boardgamegeek.com](https://boardgamegeek.com/thread/398164/single-player-rules)
by a very clever user (and then boosted by other clever users with
ideas of their own). Just with one internet search ("gloom solitaire",
for the record), a simple 2-4 player game is transformed into an
open-ended story-telling game for one player (and for the record, the
story telling element doesn't come through on its own, so you have to
make that deliberate, just as you do with **Dark oCCult**).

An "RPG card game" called **Dungeoneer** (think **Dungeons & Dragons**
combined with **Magic: The Gathering**) has a very effective [solo
mod](https://gitlab.com/notklaatu/dungeoneerfix) that'll keep you busy
for at least 90 minutes per game. It doesn't enforce a story the way
**Dark oCCult** does, but if you're pining for some classic stat
accounting and dice combat, **Dungeoneer** will blow you away.

Those aren't the only games with solitaire rules from communities of
players.  The more you get used to the techniques of modding game
mechanics, you might start developing the knack for it, yourself (for
instance, by forcing positive points on characters the longer they
stay in play, the Gloom mod gracefully develops an antagonistic AI
that you must race against as you attempt to kill off each character;
it's simple, yet brilliantly effective). So if you do get a knack for
modding rules, give it a go, and post your results online so others
can try your mods!

[EOF]

Made on Free Software.

