# Proposal for Distributionism 

The tradition of Linux distribution goes a little something like this:

In the beginning, you downloaded the kernel, compiled it, put it on a
hard drive, and then started adding applications that would make the
computer do useful things. Basically, [Linux from
Scratch](http://linuxfromscratch.org).

Very quickly, somebody decided to collect the common components on
some disks and distribute them as a collection or, as we say, a
"distribution".

And then, just as predictably, somebody else decided that those
applications could be distributed *better*, and so they also
distributed them.

As any tried-and-true Linux user will tell you, distributions are a
good thing. They demonstrate that Linux is a healthy and open
environment where people can take work that someone has done, change
it and add to it, and re-distribute it. If we, as users, lose that,
then we'll lose something integral to Linux.

But I see a little loophole here, and that's in the "change it and add
to it" clause. If we're honest, a lot of distributions out there don't
actually add that much to what they're re-distributing; they might add
a few custom scripts, or maybe even a whole new desktop environment,
but generally they're re-distributions of distributions.

Don't get me wrong: I don't think there's a problem with
that. Re-distributing a "remix" of something is a powerful feature of
Linux, so I don't fault anyone for it, and I don't think it needs to
stop.

Then again, I do wonder if there's an alternative that we should be
looking at, when appropriate.

## Mods

I came to Linux partly because I wanted to find a new interface into
my computer. I was finding the desktop model clunky, and certainly I'd
completely outgrown the one desktop made available to me by my closed
source vendor. I had supplemented it as much as I could, but it just
wasn't working for me any more.

As part of my initial exploration into the concept of modding my
computing experience, I came across a lot of online videos by guys
doing case mods. They'd build custom towers, or they'd take existing
computer cases and take drills and dremels to them to make something
new. I always thought this was cool, and once I got into Linux proper,
I took inspiration from these hardware modders when designing my own
interfaces.

Conveniently, there's an analogy to be made here.

* Hardware mods are creative.

* They build upon existing material.

* And they don't require you to swap out your computer for a new one; they create a new one with the thing already sitting on your desk.

Now, read all those points again. Carefully.

And apply it to software distribution.

Linux is a *smart* technology, so if you're distributing a collection of applications and the Linux kernel, which would you rather be able to say:

"It's easy! just back up your existing install, re-format your drive, install my distribution, restore your data, and you're done!"

or...

"It's easy! download and run my setup script."

Additions to a system may justify, but rarely should *require* a user
to download Yet Another ISO and a full re-install. I understand that
that's the "easy" delivery method from a dev point of view, but that
doesn't make it right for the user.

As Linux users and supporters, we should strive to keep the technology
streamlined, smooth, and simple. If that means we need to develop easy
ways to distribute *components* of a distribution rather than an
*entirely separate* distribution, then we should do that. The fact is,
there are only a handful of distributions with their own unique
infrastructure. Canonical's own controversial move to bar other
distributions from using Ubuntu infrastructure and resources is a
prime example of this; derivative distributions coast on the
foundations of their parents. It's pretty common.

I feel that there's a point at which we, as users with ideas to
contribute, should stop and ask ourselves how we want to contribute
them for others to use. Do we need to spin an ISO? Do we need a
website and a forum and a mission statement about how our distribution
of Linux will bring people to Linux like no other distribution can?

Or can we publish a set of packages that build upon the core of
something already widely available? The steps could be, instead, 1) be
running SUSE and then 2) run the Haxx0r Edition install
scripts. Suddenly, you've got you obligatory pen-testing edition of
your favourite distro, complete with your custom-designed desktop
environment, your icon set, your fancy hazmat logo, and all the
additional tools you want your users to have.

It's a custom distro without the distro.

## Proof of Concepts

I use Slackware, and I have, for a long time, worked in industries
surrounding the "film industry". As a result of the latter, I get a
lot of requests from clients to make specific things work on the
former. Sometimes these requests are pretty obvious; get a good video
editing workflow going, or setup a recording environment, or
whatever. These requests aren't specific to Slackware, it's just a
general setup request. Other times, there are requests to setup
something that, as far as I can tell, no one else has implemented,
like setting up an ACES-compliant workflow, or coming up with a stop
animation solution, and so on.

To document the solution to these requests, I set up a site called
[Slackermedia](http://slackermedia.info). It's not a fancy site, and it
doesn't offer a whole lot, except a complete outline of how to create
a reliable and robust multimedia studio infrastructure based on
Slackware Linux.

The parent distribution, obviously, is
[Slackware](http://slackware.com). I rely entirely upon Slackware's
maintainer, upon Slackware's servers and mirrors, and upon Slackware
itself. But that's not all. I also rely upon the
[Slackbuilds.org](http://slackbuilds.org) community and maintainers to
provide 92% of the "extra" packages that Slackermedia instructs a user
to install to achieve a flexible and complete multimedia studio OS.

Slackermedia doesn't provide a downloadable ISO. It doesn't provide
packages. It's just text.

For that reason, I call Slackermedia a "distro-from-text". 

Because it's Slackware, I expect a user to read the docs and learn how
to intelligently compile and install the software; and then how use
the software in a production environment. So I don't even provide
install scripts (aside from those that I write and contribute to
Slackbuilds.org), although I do provide a list of packages that they
can install in bulk and get about 95% of the way to a full
"Slackermedia" install.

It's a distribution that distributes nothing, but it would be trivial
to compile packages and point people to them; a distribution of
software without the ISO. There are existing examples of this, too;
there's the [Planet
CCRMA](http://ccrma.stanford.edu/planetccrma/software) for Fedora,
[kxstudio](http://kxstudio.linuxaudio.org/) for Ubuntu, and
[Studioware](http://studioware.org/) for Slackware. All of them are
arguably "distributions" in their own right, and not one ISO or
re-install to be seen.

It's time to re-think the concept of "fragmentation". Do multiple
Linux distributions cause fragmentation? Well, by definition, yes, but
not destructively. The real problem is technological
inefficiency. Linux doesn't mix well with that, so let's not try to
force it into something so clunky.

Let's re-think and re-make the Linux *distribution*. It could and
should be so much simpler.

[EOF]

Made on Free Software.

