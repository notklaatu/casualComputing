# Why Does Linux Need Users?

I have never really been comfortable with the idea of "advocacy", a
concept that in the "real" world usually means that a person is kept
busy standing on a soapbox proclaiming how important something is, and
how other people should do something about it. The idea, I think, is
that a critical mass of "advocates" for something will be reached,
forcing the hand of those in power, who will, essentially, be bullied
into performing some action to fulfill the desire of the advocates.

Advocacy doesn't just exist in politics, it's a tech trend too. I
see it crop up in several areas, and it's a tricky thing because on
the surface it feels good.

## Good Advocating, Bro

Advocacy feels good, because it's (sometimes) a positive statement in
favour of something that someone likes (or, it's the kind of negative
that makes you happy, but I'm going to ignore that style of
advocacy). This seems like a good thing, because it's a voice in
support of your cause, and that provides a confident boost and adds to
your perceived number.

The problem is, it doesn't add to your number. It only *appears* to
add to your number, but there's no actual support.

(It also presumes that there is a "critical mass" that, once reached,
will influence higher powers, meaning that it assumes a higher power
that will grant you your wish, which is a whole other problem. Surely
the idea is not to petition for something as much as it is to "be the
change", to quote a probable inspirational poster.)

I've read several tech journalists and "tech leaders" who go on and on
about how "Linux is a fine operating system, nearly ready for prime
time" or how "Linux is important" and "we love Linux". It's usually
something that's said with the unspoken caveat "but I don't know much
about it, and I'm not going to learn". The larger message is that
Linux, while powerful, de-centralised, independent, and probably the
best solution for technology going forward, is something I'm not going
to investigate personally because I'm comfortable with the status quo,
however broken down it may be, so I'll keep using and promoting the same
stuff I've been using and promoting for decades.

What's the point? It's like a parent smoking a cigarette whilst
telling their child not to smoke. The words and actions don't match
up, and one is more powerful than the other.


## Advocacy or Solution

Advocacy is like a sympathy card; it's a nice thought, but does
nothing to solve the problem.

And the problem that we should be solving in the tech world is
ineffectiveness in technology. Why are we promoting closed-source tech
that has a monetary barrier-to-entry, that keeps secrets from its
users, and that most people are being forced to learn and use in order
to interact with schools and government? 

There are some things that just ought to be provided to people, and
those things compound along with society. Started out that
civilisation offered the basics; safety in numbers, shared resources
such as food and water, shelter, and an agreed location for waste
disposal. Pretty good reasons to join in the whole "civilisation"
trend. But the world got more complex, and expectations
increased. Running water, plumbing, a sense of community, arts and
entertainment, health, and other "luxuries" got rolled into the
baseline for the definition of "civilisation".

Today, of course, the baseline includes things like education, job
opportunities, and the gateway to it all: technology.

Why, then, are we treating technology the same way water companies are
starting to treat water: if you bottle it up and sell it, you can
charge people for clean water instead of providing it to people
*gratis*, which is one of the main selling points for even bothering
with the notion of a civilised society in the first place.

This is a bad idea.

Advocating for a better solution is all but empty if you don't
actually move toward that solution. "Yes, somebody should do that"
pales in comparison to being the one doing it.


## Advocacy Through Action

Don't get me wrong: it makes me happy to see a tutorial site using a
screenshot of Ubuntu when providing a tutorial, and it warms my heart
when sites acknowledge that yes! Linux exists, and that people use it for
real life, everyday things!

That's great. I'm grateful for that. I don't want to lose it, and I'm
not by any means saying that only people who use Linux are allowed to
release software for or show support for or acknowledge or comment on Linux.

But there's support and there's advocacy. When software vendors and websites and articles "support" Linux, what they usually mean is that they do not shut Linux out, and enable Linux users to use their product (whether the product is a tutorial with relevant screenshots, or software with a compatible download, or a widget on the site that requires some browser plugin, or whatever). That kind of support is real, and it's what pretty much exactly what I mean when I say you shouldn't advocate but *use*. In those cases, the "use" is reversed; instead of using Linux, they are enabling their product to be used on Linux.

Things that are not that are examples of empty advocacy, which has
several problems that pop up in unexpected places.

Sure, the obvious danger is that even a statement of
quote-support-unquote becomes, at best, a back-handed
compliment. "Linux is great! who knows? maybe someday it'll grow up to
be a real operating system that I can actually use!" That's often the
implication when Linux is praised *but not used*, because there's the
implication that something needs to happen *in Linux* for it to be
usable. But this ignores, and possibly even spites, the fact that
there are millions of users consciously and intentionally using Linux
(I'm not counting the billions of people who "use" Linux between the
internet, Android, embedded devices, and so on; I am talking about
desktop users specifically) on a daily basis to do real work. What was
intended as advocacy ends up invalidating millions of users, as if
they don't even exist; how can they exist? Linux isn't yet usable (but
hey, "it's nice, and it's getting close!").

A nice caveat to avoid this would be something like "Linux is neat and
as soon as I stop being a lazy technologist, I'm going to use it!"

More subtle, though, are the good-intentioned advocates, often from
within the pool of existing Linux users, but also from outside. People
get excited about open source, and in that excitement sometimes things
get praised very loudly. But not all praise is informed praise; some
of it is just plain old fashioned over-enthusiasm. To make matters
worse, the internet is the internet and so the praise gets amplified
if enough content-echo sites pick up on it. Why is this a problem?

The problem is that uninformed support is empty support. If you sing
the praises of, for instance, GIMP because you heard that it was a
really snazzy graphics application, to someone looking for an
alternative to Adobe's closed source Illustrator, then you'd be doing
a great disservice to the user and the software (because GIMP has only
rudimentary path support; the correct answer would be Inkscape). I've
read a great number of articles on how great `$FOO` software is, based
entirely on the claims `$FOO` itself makes about itself, and a handful
of screenshots that certainly make it look like it's powerful. This
doesn't do anyone any good, and in fact threatens to make a horrible
first-impression on users looking to switch to open source.

If you don't use something, you don't know it; if you don't know
something, you can't advocate for it. You can recommend that someone investigate it as a possible solution, but you should not position it as their Problem Solved.

The same holds true when people advocate Linux without using
it. Naturally, I personally believe that Linux can be recommended in
practically any case, but even I will admit that there are sometimes conditions
going along with that recommendation. But if I am not using Linux on a
daily basis, I can't intelligently provide those conditional warnings
or notes. Worse yet, I might provide incorrect warnings.

Let's say someone is thinking of switching to an open source,
Linux-based solution. As a non-user, you give them a list of things to
take into consideration based on your general understanding of the
current state of Linux, combined with that one time you tried Linux,
plus maybe a quick web search. So you suggest Linux, and tell them a
few general cautionary notes plus a few added tips, painting a
completely incorrect picture of the current state of Linux. Then they
try Linux expecting one thing, only to find that what you told them
would work doesn't, and what you told them wouldn't work is a
one-click install. Are they doing something very right? or very wrong?

Or maybe you're a Linux user, and you've done your research, at least
to the point that you have been able. For instance, I don't personally
do CAD. I work in VFX, so I do a lot of 3d rendering and I'm in 3d
applications a lot, but I have never had the need to learn or even try
architectural rendering or design. I'm pretty good with Linux, and I'm
pretty good at figuring out applications, given enough time and a
million monkeys, so it's entirely within my ability to install some
CAD applications and take them for a spin. However, everything I would
do with these applications would be entirely without context, and
without comparison. Now, admittedly, when most people evaluate
software they could do with a lot **less** comparison, but even so I'd
be doing architects a major disservice if I pretended like I was an
expert architectural-software-on-Linux consultant and assured them
that Linux-based CAD was exactly what they needed.

(To reiterate: I have no experience with CAD, so this is a perfect
example; to that end, I am neither recommending or cautioning against
CAD on Linux.)

(By contrast, I'm very experienced with tools like GIMP, Inkscape,
Scribus, and anything having to do with video. So my recommendations
in that area are pretty reliable.)

I've personally seen lists of these "reviews" all over the
internet. Sometimes they're even written by occasional users of Linux,
but to anyone who uses Linux daily, they almost always read like those
book reports you used to do in school when you didn't want to actually
read the book, so you just watched the movie instead. That is to say,
there's some kernel of truth there, but the emphasis is on the wrong
thing, and there are other things that are just completely wrong.


## Staying Out of Each Other's Kitchens

This isn't about not wanting someone's sympathy vote, or being
sensitive about backhanded compliments, or feeling patronised. It
isn't about being possessive, or trying to exclude anyone from trying
something new and commenting about it.

My point is that being "supportive" of something vocally and then not
following through in actions is at best lazy and at worst
hypocrisy. I'll be the first to admit that the opposite can feel
almost as bad; companies and software vendors that actually use and
support Linux make no mention of it, adding to the perceived void of
Linux support.

It would be nice to have the complete package in both scenarios. If
you use and support Linux, give it first class treatment, the same as
everything else. Stop defaulting to one platform and treating others
as after-thoughts, because they aren't! take credit for your work,
your support, and your dedication to open technology. On the other
hand, if you're uninformed about Linux and only want to mention it
because you know that it exists and want to acknowledge it, then
qualify your statements so that people understand that you are not
reporting on research, but on assumptions and second-hand information.

This is nothing more than I'd expect on *any* topic, tech or otherwise.

The benefit to everyone is that advocates become users instead of
observers, and their feedback, both positive and negative, becomes far
more valuable. It's easy to critique things, especially when you don't
actually use it. So get to know the thing first, and then instead of
critiquing it, help make it better. But don't wait around for it to
get "good enough" for you to actually use, because as long as you
refuse to use it, that day will never arrive.

[EOF]

Made on Free Software.

