# Voluntary Paywall




It's a modern world, we have modern conveniences, and we have been
conditioned that everything is available for a price. That's one of
those "facts" of life that we grow up with and that we learn to both
love and hate. After all, it's socially unjust that anything is
possible for wealthy people but withheld from normal folk. And yet
there is some comfort there, isn't there? Anything is possible! if we
just save up enough money, or else wait until someone rich happens to
want the same thing as we do, it might be purchased into
reality. Money brings us hope and comfort. Money can make all things
possible.

Maybe that's why it's so scary to us when we come across someone or
something for whom money holds no appeal. Seriously, go hunt down
someone who does not want your money. It's like staring into the cold
black eyes of someone who has no soul. They are impossible to
reach. It's unsettling to modern man. How can someone refuse money?
How can my money not force something into going *my* way?

This is a big topic, and one that should be considered from many
angles. Socially, it's a very powerful thing, and it's why governments
are less concerned about money (an imaginary construct) and more about
land ownership; everyone needs land because everyone has to exist
someplace. You take land away, and then you start to get the reactions
out of people you really want to provoke. It's a very effective
strategy.

But let's look at this concept as it applies to software and open source.

## Software

Sometimes I observe people using non-open source software. It's
interesting to see how they deal with problems.

First, there are the work-arounds; the things that take effort but are
still somehow more convenient. You know the kind; we all do
them. Can't seem to make the printer cooperate? well, we'll export to
PDF, put the file on a thumbdrive, and walk it over to the printer to
print straight off the stick. Whatever. The problems are small and
mysterious, the work-arounds are stupid and technically less
convenient than finding the actual fix, but it fits into our rhythm
and so we adapt.

Then there's the usual internet search and investigation phase. The
workaround is becoming problematic or annoying, we really want to get
this fixed, so we look online to see what other people are doing.

And finally, there's the payment stage. We couldn't fix it ourselves,
so we pay someone else to make the problem go away. Sometimes that
means we buy better software, or a better version of the same software
so we can unlock otherwise forbidden features. Other times it means
paying some high school kid from the local computer store to figure
out the issue for us. It doesn't really matter how the problem gets
solved, as long as we can put money down on the counter and have the
problem go away. That's what we're after, because that's what we've
been trained to expect.

Modern technology makes everything possible. You just have to keep
feeding it money.

The impression most of us have, deep down, is that nothing is
impossible. Technically, anything is possible in this world (because
of technology!), it's just that some features are locked behind a
paywall. And we're OK with that. Want to send emails in bulk without
getting throttled by your ISP? pay a little extra and you can! Want to
get faster internet? upgrade your plan and it shall be granted. Heck,
we even see diseases as a pay-to-play scheme; have an incurable
disease? pay extra to unlock the cure.

Bringing it back to software, the result of this mindset is that if we
have a problem that we cannot solve, we can pay for it to be solved
for us.

The problem with that is that *somebody* had to come up with the
solutions we're paying for. We're an advanced civilisation, so surely
we don't actually believe that the **Buy Now** button in our web
browser is actually auto-generating, from the depths of the Abyss, the
answer to our problem, on-demand.

Right?...we don't actually think that, do we?

Sometimes I wonder.


## Open Source

Since so many computer users are trained to believe that everything is
available just around the corner of the next paywall, I notice that
open source can be unsettling to people.

With open source, a lot of the problems (past basic configuration and
trouble-shooting) are problems that can't be ushered away with a
payment plan. Believe me, we've tried it, and it doesn't end well,
because it's *open source*. There isn't one definitive set of problems
that everyone in the world working on open source can sit down to
solve; we each have our own list of things we need done, and it's not
money that's the problem. The problem is that a solution hasn't been
invented yet, but that's what we're working on, just as quickly as we
find them.

And by "we", I mean *each individual*.

You see, a lot of people seem to have an illusion that **Open Source**
is the name of a company. A place where we all go each day and sit at
our desks, or we remote-in because hey that's what high-tech people
do, and we have meetings, and things are mapped out and things get
done. That does happen in some places, but if we see open source like
that, we're looking at it backwards. You see, open source isn't
something *you* go to in order to join, open source is a label that
gets stuck onto you *post facto*. If I make some software for myself
or for someone I'm helping, and I post its source code online, then
when the Internet gnomes come round at night, they put a label on it,
declaring it Open Source. If I add a license explicitly stating that
you may use it and change it and share it, then it gets an upgraded
label that says it's liberated software; it's software that I gift to
you with the promise that I can never take it back from you. You may
use it and keep a copy of it forever.

To do that, I didn't have to sign up with anybody, I didn't have to
fill out any registration forms or log my time or submit my code for
approval. I just wrote some stuff and made it available to others.

Now, if you come round and offer to pay me to make my code better, I
may or may not agree. First of all, I might not agree that what you
want is "better". Secondly, I may not agree that the pay you are
offering me is worth the effort. Thirdly, I might not want to get
involved in something that brings along with it a new dimension of
expectation; if you *pay* me for something, surely that thing is
subject to your approval.

And that's unsettling to some people. It's the classic bit: "what do
you MEAN my money's no good here? DO YOU KNOW WHO I AM?!"


## Open for Business

The obvious sympathetic question is why anyone would ever want to use
a product in which there is no recourse when something goes wrong. In
fact, how can open source be taken seriously at all if there's no way
for its customer base to call for features and improvements?

Well, that's the thing about open source, though, isn't it? with open
source, just as anyone can choose to *not* be swayed by money, anyone
else can choose to work for nothing *but* money. Open source is
liberated software; if you want to pay someone to hack on it and add
features, *you can*. If you want to rally up a group of people who
want the same features, you can all pitch in to pay for those features
to appear.

The difference is that the driving force comes from the need for
something, while in the closed, corporate model software is churned
out and customers build their workflows based upon it. It's a little
like the paywall itself; demand-driven development requires effort
from users, who must figure out what they want, and that's a tall
order, especially since we're all being trained on a daily basis that
*adapting* is the way we are supposed to work. You give me something,
and I'll work around it: that's how non-open software development
works. When I want something better, I pay extra.

We can work with this. It's predictable, it's secure, there are clear
definitions on what our rights as users are. As long as we pay, we get
supplied with infrastructure that might not be exactly what we want or
need, but it's close enough that we can adapt. If we need something
different, we pay extra. If we need something that simply does not
exist, then and only then, we get up from behind our desks.

And we make.

Something.

Happen.

That's open source. It's the act of getting up from behind your
desk. It's not as comfy as the corporate version of life. It takes
thought and effort, and sometimes you'd rather be doing something
else. But when the money runs out, or the money simply cannot buy what
you need, it's the principles of open source that's going to come to
the rescue.

Because open source is production for use, not for profit.

[EOF]

Made on Free Software.


