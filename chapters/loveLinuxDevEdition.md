# Why I Love Linux, Developer Edition




I am periodically stricken by emotional responses to Linux, so I've
written several times about why I love it as an operating system, a
culture, an environment, a tool, an inspiration, and whatever
else. Usually these articles are written from the perspective of a
user, because I am a Linux user both at work and at home, or as a sys
admin, because I am also a sys admin. Lately, though, I've been doing
a significant amount of development, and it's made me see Linux from
an entirely different angle, and develop a whole new set of feelings
for it.

I've done minor development on Linux before; I started out, like many
Linux users, hacking stuff together in order to make life easier. That
was, after all, one of the reasons that I got into Linux in the first
place: I saw on my old OS that there were processes that seemed so
repetitive, or so clunky and laborious, that it just felt like doing
them manually was counter to the very point of doing them on a
computer at all. We humans invented computers to enhance our ability
to work, not to increase our work (is that still the story we're
sticking to?) so why were there things on my computer that I could not
automate or improve?

And that was a great thing to get out of Linux. After a childhood of
not understanding *why* I did not have the power to make my computer
work better for me, I had gained the ability to customise my
tools. This was literally life-changing, and frankly it's reason
enough to love Linux from a developer standpoint, however "trivial"
the code behind the development might have been. Heck, even my first
shell script ever, the automation of connecting to my wireless
network, signified that in one month of Linux use, I had been empowered
to learn more about computing than I had in 5 years of struggling in
earnest with my former OS.

One of the funniest, and yet painfully true, articles I've read on the
topic of developing on something that is not Linux is by Ted
Dziuba. You should read it sometime.

http://harmful.cat-v.org/software/operating-systems/osx/osx-unsuitable-web-development

Ted's article is specific to web development, but looking at this
issue more broadly is important, too, because it actually gets even
better.

When you're developing software, you don't see things the same way you
do when you're just being a user, or when you're a sys admin. Don't
get me wrong: I'm not saying that your genetics change or that you
become spiritually elevated, I'm just saying that the needs and
concerns of someone wearing a developer hat are different from someone
wearing a user hat. Here are some ways that a developer's life is
better on Linux:


## OS as a Platform

Do you think IDEs are neat? then why not buy some property? Unix has
developed a truly unique method of making software available to its
users: essentially, it stores practically every library and every
application *in existence* on your computer. Ah, but wait, when I say
"your computer", I mean in the Sun Microsystems sense, where the
**Network is the computer**; so, really Unix stores every library and
application in existence on a server somewhere, and when you want to
use it, you grab it from your network, it gets dropped into place, and
you're up and running. It's a push-button system that makes it trivial
to get the library you need.

Of course, I exaggerate when I say "every library and application in
existence", but not by much. If you're developing your own solutions,
then there's practically nothing you'll be without. Obviously, if you
were entrenched in platform-specific technology prior to moving to
Linux, then this won't hold true for you (that's kinda the definition
of "platform-specific"), but if you're using cross-platform tools and
are willing to use cross-platform frameworks, then you'll just keep
finding more and more to use in your work.

The amazing thing about this is that it's *so* darned profound that
users of other platforms have "borrowed" the idea. You don't have to
look far to find implementations of Linux-style package management on
other platforms, because even if you never touch Linux, you just can't
argue with effective development systems. And that's where Unix has
really excelled in the development world; it not only set the pace,
but it defined the very paradigm. Do you have to develop this way? no,
you don't; and some groups don't. But on the whole, the Open Source
model pervades, and it's empowered by effective, convenient package
management.



## Everything is an API

On Linux, I was used to having direct access to anything I wanted:
webcams, tablets, gamepads, printers, the entire pci bus, USB
profiles...literally everything attached to the computer, whether
internally or externally. Simple example: let's say I'm writing an
application to take a snapshot from the user's webcam. On Linux, it is
literally as simple as gathering data from ``/dev/video0`` (or ``1``,
or whatever number of attached cameras you want to use). It's that
simple.

You don't really realise just how simple that is until you try doing
the same thing on a non-open operating system. On those, you have to
use an SDK. Don't get me wrong, that's not always a bad thing by any
means, but it is a different way of working. Sometimes, it makes
things easier; after all, an SDK usually implies that a "kit" of lots
of fancy ready-made tricks come bundled with the SDK. No programmer's
going to argue with that. But why does the SDK have to cover
*everything* and be the only avenue to where you want to go? What if
you don't want to program in the language the SDK uses? What if all
you need to do access one tiny component of the computer, but
everything else in your code works perfectly? why should you have
download, install, and learn a whole SDK frontend just to do a simple
task in otherwise SDK-free code?

The problem with the closed-source "you have to use an SDK" model is
that it introduces a layer of abstraction that is non-optional. You
*have* to use the SDK, pretty much whether you like it or not. I'm not
opposed to getting a bunch of royalty-free code dumps that I can use,
but at the same time, I don't always need it.


## Dev Tools Implicit

It's a strangely subtle luxury that sneaks up on you, but on Linux the
dev tools are woven into the very fabric of the OS. I don't just mean
GCC, because not all Linux distributions do include GCC in your
initial install, but even without that, there are so many ways to
develop on Linux that you almost can't avoid but be technologically
creative. The whole system encourages creativity. Maybe you haven't
installed GCC, but maybe you'd like to just quickly shell script a
repetitive task? or maybe you want to quickly invent a little Python
application, or a Perl application or script. Maybe you need a plugin
to convert thumbnails or sound files, or to send emails, or to take a
photo for a time lapse art project; the sky's the limit, and that's
without anything more than a base install of Linux. I'm talking about
a system that could fit on a CD ROM. I've seen closed source
applications that can't even boast that kind of download size.

Need bigger and better tools? they're a download and install
away. That sounds rather unspectacular, because it is. It's just that
simple. You decide you want to develop a C++ GUI application one
moment, and you're doing just that ten minutes later.

On a closed system, things are a lot different both technologically
and culturally. There are users and there are developers; and the two
aren't really supposed to intermingle. And it shows. On Mac OS X,
Apple wants you to install Xcode to do any kind of development
work. Sounds simple, especially since all the websites declare proudly
that Xcode is an easy, free download! Cool!

Except it's not cool. To download Xcode, you need to create a
"developer account" with Apple. That's free as well. Sure, it's a
little invasive, but you can use a fake email. Then you have to verify
your email address, and then log into your new account from within the
OS, and then finally download a 4GB compressed disk image.

Wait, 4GB for development tools? Heck, I downloaded a "bloated" Linux
OS the other day and it was 4GB; it included the OS, every driver
imaginable, not one but five desktop environments, a graphics editing
application, an office suite, development headers, development
libraries, a compiler or two, and a lot more. So how can Xcode
possible amount to 4GB just for a compiler and an IDE suite?

Well, actually it's not really 4GB. That would be crazy. After the
compressed archive has been downloaded, you install it once only to
discover that the installer is broken (at least during February 2016,
the shell installer was silently broken) and so you try again using
the GUI installer, and then you wait while it installs, and finally,
at long last, you see that the final dev suite for this OS is no less
than *eleven* gigabytes.

I'm not sure which is worse: the convoluted path to the final install,
or the final insult of discovering that just to compile a silly "hello
world" C application, you had to install 11GB of software with no
option for modularity.

Are there hacks around this?

Yes, of course.

Should you have to scheme against your own computer?

Absolutely not.


## installing apps, integrating with the system

windows: nsis makes things better but I'll be danged if .lnk files
ain't binary mac: rave all you will about .app bundles, but YOU try to
create one



## Open Source Licenses

Sounds silly to point this out, but the fact that you can live and
work in an open source environment without any real effort is one of
the most liberating things about developing on Linux. If you've ever
had to develop in a closed environment, then you know that *usually*
(hopefully) you have the tools your job requires, but when you haven't
got a library for something, you're well and truly blocked. To get
back into the swing of writing code, you have to:

* Find a library that does what you need
* Verify that the license permits you to use it
* Before you buy: investigate that the lib will work for you
* Pay the licensing fee for the library
* Learn the library
* Use the library

When you're living in an open source version of reality, those steps get refined:

* Find a library that does what you need (you may already be using it on your system)
* Learn the library
* Use the library

It's half the work, really, at an eighth of the stress.

In practise, the only people who will relate to this benefit are those
who've worked in large, closed source corporations. That's where you
actually run into these problems. For the home developer who just taps
into the Mac or Windows "dev community", it's usually a hybrid
situation: you use as much boilerplate code from your parent company
as you can, and then grab whatever open source stuff you need to make
life easier on yourself. There may be some licensing hoops to jump
through when you want to use a library but cannot legally ship it
because you are not releasing as open source) but mostly it's just a
matter of giving credit where credit is due, or coming up with a silly
installer work-around (like how Audacity helps users download and
install the LAME library for fear of violating MP3 patents were they
to ship it along with their product).

But that doesn't mean it's not valid. For the people who toil in the
closed source world by day, open development is a life-changing,
liberating breath of fresh air.



## Port Everything

There's arguably a syndrome in open source software and free culture
whereby people rave about how great it is to have access to source
code, or the ability to do one thing or another, but when you look at
how often they actually *do* the thing they are excited about, you
find that if it had been taken away from them a year ago, they'd never
have noticed.

I find that in the worst example of this, it's essentially accurate;
nobody's looking at the code, nobody's using the code, but in those
cases, the project is by definition true. In reality, even though a
handful of users may not ever bother looking at code and may never
bother writing a plugin for an extensible application, there are other
users who do.

And it's from those users that open source benefits, and the liberty
of being able to look at code and change code is justified.

I say this both as a user and a packager, myself. As a rule, I'm one
of those niche users who always seems to dig up old forgotten software
with the sudden need to use an obscure feature that nobody else has
needed for decades. Or, on the other end of the spectrum, I find
myself very often picking up the latest software wanting to run it on
not just old hardware, but old hardware with (relatively; to most
people) obscure architectures, running Linux.

So what's that mean, in the end? It usually means that I need the
source code so that I can make tweaks here and there in order for it
to compile and run on the platform I'm using.

There aren't really degrees in this game. It's either on or off; you
either have access to source code, or you don't. And when you're
somebody like me, you notice it when you don't have access, because
there's no negotiation when you try to get the thing running on
less-popular platforms.

The interesting thing about this sort of problem is that's rarely a
matter of a platform not being supported because it would take too
much work to port the code over; it's simply a matter that the code
never got compiled for that platform because it just isn't in the
80%-20% split of the platforms the developer felt it necessary to
build on. So there's no *reason* it shouldn't run on a given platform,
it just doesn't.

Imagine a light switch. If the light is off, then the solution is to
flip the switch to the On position. With closed source software, in
this context, there is no switch.


## Using the Source

If I were to say that one of the main benefits of Linux is the fact
that there are examples of code all over the place, I mean:

There are examples of code **all over the place**. There is example
code in *literally* (in the literal sense of the word "literally")
everything you use when you use an open source stack. Want to write a
plugin for GIMP or Qtractor or some other media editor? look at the
plugins you have already been using and learn. Want to learn a
programming language?  look at a tool written in that language. Want
to write a clone of a tool that's almost perfect but you think could
be better? look at its code and avoid the same mistakes. Want to
improve something by adding a feature or fixing a bug? look at the
code.

Open source means that the source is available for you to look at, at
the very least, and quite likely it's within your right to change
it. Either way, open source gives you working, in-production samples
of development languages and tools that you might be evaluating for
your own project. I myself benefit from this profoundly, and in fact I
usually have the luxury of approaching it from the other direction; I
get used to using certain tools in real life, and then when I decide
to write an application, I choose to use the frameworks that are in
use on my desktop already. It's the very definition of
seamless integration.


## Free Agency

There's open source and then there's open source. And Linux is as open as it gets.

Of course, to the pedantic, the distinction is summarised by the term
"open" as opposed to "free", but the term "free" is ambiguous at best
(it still amazes me to think that there is no adjective form of
"liberty" in the whole of the English language). But to me, dwelling
on the words themselves tend to lose the impact. The point is that on
Linux, you're entirely at liberty to do whatever you want, especially
as a developer.

For this point, I must use the term "developer" pretty loosely,
because I exist on that spectrum and have for a very long time. As a
power user of computers, even when I was or am acting as a regular
everyday user, I still have that trace of developer tendencies. I
don't take applications as finished products, ever. That's just not
how I ever learned to look at computers.

And when I put a developer hat on and do serious coding, I definitely
don't look at existing conventions as the boundaries for what's
possible for me to create. Linux, with its diverse inhabitants and
applications, its multitude of frameworks and toolkits and
subsystems, and its complete openness, allows developers, and users
who won't settle for less than exactly what they want, to construct
anything. Not just what other developers think you want to build, and
not only things that fit inside an existing container or within the
limits of a specific use case, but anything you can imagine and figure
out how to create.

Why would you settle for anything less?


[EOF]

Made on Free Software.

