# Windows Sub System for Linux




At the end of March 2016, Microsoft announced that they had written a
compatibility layer, technically known as WSL (Windows SubSystem for
Linux) or something like that, so that ELF binaries (the format used
by Linux, and so colloquially synonymous to "Linux binaries") could run
on top of Windows. I don't usually comment on Microsoft news, because
mostly it doesn't affect me, and also it's more subject to change than
UNIX tips and general tech trends. In a sense, though, this really is
about a tech trend, so I thought I'd jot down a few notes on this
development from the viewpoint of someone watching it happen, if for
no other reason than for a historical perspective.

First a little background:

In 2015, Microsoft went public and announced that "Microsoft Loves
Linux", and more broadly that Microsoft loves open source.

In October 2015, I went to the **All Things Open** conference and was,
like everyone in attendance, surprised to see that Microsoft had
purchased a booth there. Only, no one was actually surprised;
Microsoft had gone out and bought a brand new costume just for this
event, handing out stickers carefully designed to be quirky and
non-traditional (a cat riding a T-Rex, whilst waving a Windows flag,
wow they really GET my generation!), and super casual booth reps, it
was like They really were One Of Us!

And then at the end of March 2016, during their own **Build 2016**
conference, they unveiled their Linux Subsystem, built into the NT
kernel and usable as a beta in "dev mode" on Windows. What it does,
specifically, is translate POSIX sys calls to what NT listens for and
responds accordingly, and provides an abstraction layer so that
filesystem locations in Windows are recognised logically by the POSIX
processes (a Windows user folder becomes `$HOME`, external devices end
up in `/media`, and so on).

None of it is emulated, none of it is virtualised. It's a native NT
library (or libraries, probably) running the exact same compiled ELF
binary that you would run on Debian.

To sum up: Windows now runs native Linux binaries.

Exciting stuff, right?


## Yes but What Does it All Mean?

There are lots of implications around MS condoning, and presumably in
some way supporting, ELF binaries.

You might see it as an admission of defeat; Windows binaries can be
unofficially run on Linux through the sheer power of independent
hacker dedication, and now Linux binaries get corporate sponsorship on
Windows. Not only did Linux do it first and without help, but the
great and powerful Microsoft has bowed to pressure and finally
admitted that there just might be something to this whole open source
Linux GNU software thing. Like, something big and serious.

Or you might say that Windows is attempting to gobble up open source
culture. After all, Windows tried it the one way for decades and
decades, fighting tooth and nail to bury Linux as an amateur,
insecure, risky operating system that no one professional should ever
dare tamper with. And now they're suddenly embracing it, as if they
themselves just discovered this cool new thing that they just gotta
bring to the world as a Genuine Microsoft product. That's what Linux
is, right? Microsoft Lite? Just a development tool, for geeks who
haven't yet learned Microsoft yet.

In fact, maybe you're a developer who has worked on some of those GNU
and BSD tools that Microsoft is now gleefully running on Windows
without ever having given your project a dime for your troubles. Not
that you needed a dime, but after all the insults, the anti-Linux
marketing, the subterfuge, the sabotage, the veiled threats, and the
outright lawsuits, one might have thought Microsoft would have a shred
of decency so much as to at least publicly apologise, and at best
contribute something tangible (and no, KVM modules so that Azure will
run Linux doesn't count).

Or maybe you see it as the perfect win-win for everyone involved.
Microsoft adopts some GNU applications, and Linux gets a stamp of
approval from what many see as the very definition of what computing
is, plus some pretty major exposure, worldwide, by way of the OS that
ships on most computers made.

And maybe you're someone who stands to benefit from this; if you're
someone who's stuck on Windows all day or even prefers to use Windows
(in which case, heaven knows how you found this site, of all sites),
then having access to actual GNU applications on Windows might be a
big benefit to you. I don't personally see the convenience of running
ELF binaries through a translation library on Windows (it seems like
compiled native binaries would be more useful, like with Cygwin).

Yes, there are lots of ways to see this development, lots of angles
from which to look at it, and lots of emotions.

So what are we to make of it all?


## Open Source Open Share

The GPL, BSD, and similar licenses state and even encourage people and
companies to take code and use it, and just as importantly, to share
it. So sharing software isn't just in the culture of Open Source, it's
written in our by-laws. Microsoft ingesting ELF binaries is, really, a
good thing. It's exactly what Open Source has been encouraging people
to do. I've written several articles myself on how frustrating it is
that closed-source companies refuse to integrate free code, especially
when that free code would make things so much easier for users and
support staff.

In short, we *want* Microsoft to do this. We want Microsoft users
(willing or otherwise) to have the option, at least, to own their data
plus the *code* they used to create their data. We even want it to be
an option for them to look at that source code and learn from it or
improve it, if that's what they want. And last but not least, we want
them to be able to take their data and the tools they used to make it
and migrate to any OS they want to migrate to, depending on their
needs and interests; no vendor lock in, no data held hostage.


## What Can Open Source Do For Me?

I realise that Microsoft, as a corporation, is by no means a newbie,
but their attitude to open source, so far, very much has them looking
like one. This isn't unusual; most of us take this approach at
first. I didn't download VLC for the first time as a kid in order to
see what I could do for the project, I downloaded it because it did a
better job of playing media than what I had.

It feels, to me, like a they had a meeting at Microsoft and looked at
the latest Github survey results and saw how many developers Linux
had, and asked "How can we get some of those developers on our side?"

So, I imagine, they looked at all the data and tried to find what it
was that was attracting all these really good open source devs to
Linux. Ultimately, someone must have chimed in and said "well, they
seem to like the words Open and Source..." and everyone got really
excited and decided that that was the key. Attract open source
developers by offering open...source.

You're probably seeing the bizarre irony here. Developers have been
developing open source tech on Windows for decades, just as much as
they develop open source on any other platform. Why? because it's open
access. Anyone can contribute. It isn't rocket science. If you don't
let people see your code, people can't work on it, and sometimes they
can't even work *with* it.

So Microsoft started flirting with officially-sanctioned open source.

Great!

Only, let's look at what they are open sourcing. Dev tools, almost
invariably. That seems pretty obvious, because if you want to attract
developers to your platform, then you would open source the tools
they'll need, but it's essentially a nice way of saying "come on in,
give us stuff for free!"

Or, in the case of Azure, "come on in, use free stuff on our closed
platform!"

Don't get me wrong, the open source code is appreciated. But the
expectation is blatantly clear; the source is open for developers, so
that developers can program on and for Windows. Windows itself, for
instance, is not open source. You can't see that code. The major
Microsoft applications aren't open source. You don't get those. You
get the tool shed out back, so that you can contribute into Microsoft,
but you still can't come inside.

And even the Linux subsystem library is a little awkward, if you stop
with the obligatory clichés and deliberate double-takes ("Linux on
Windows! I can't wait to see what Richard Stallman has to say about
that!"). Why did Microsoft need Canonical developers to work with them
(or *for* them?) to make the GNU-applications-on-Windows project a
reality? it's open source, so why not just take the code and integrate
it as needed? If they needed expertise, why not hire it? It feels odd
that two money-driven companies would suddenly team up to work on
bringing free source code to developers when all that's required is
for one of those companies to download some code, build, integrate,
and release.

But most of all, and most embarrassing, is the fact that the WSL (the
Linux subsystem translation libraries) is not, itself, open source.

So, to be clear:

1. Microsoft loves open source.
2. Canonical is vendor of open source.
3. They work together to bring open source to developers...
4. By producing closed-source code.

Obviously, the attitude here is still very much *what can you do for me?*

If you're an open source developer, you're welcome at Microsoft. Just
as long as you purchase the Windows license, and work within
Microsoft's guidelines. Whatever you do, don't think about the closed
source surrounding you; just put the stickers on your laptops like the
"real hackers" do, and listen to the webinars about how much Microsoft
adores open source, and get back to work.

## Love and Rent

It's nice that Microsoft loves open source, but love, as they say,
does not pay the rent, nor does paying our rent prove love. So what do
we want?

Well, for decades Microsoft has held, essentially, a monopoly (not, I
am told by the higher courts of "Justice", legally) in the computer
market. How do I know this? Because everything (not literally) gets
released first and foremost for Windows. Every device you purchase
comes bundled with a Windows driver. None of this is "bad", but the
fact that it's exclusive very much *is* bad.

So the fact that Microsoft tells us that they love Open Source means
very little, and will continue to mean little of substance, until we
start getting more than just love notes. I'm not saying Microsoft has
to write the drivers for us, but then again I also didn't ask
Microsoft to tell the world how much they adore us Open Source
townspeople, either. My point is simply that if they are so very
interested in the success of Open Source, why not use their 900 pound
gorilla to influence vendors to play nice across platforms? shoehorn
an industry standard into drivers that insist upon open source code or
at least open APIs that can be shipped to customers legally, without
threat of legal action over firmware blobs and video codecs.

Why not provide Open Source with meaningful contribution?

Well, Microsoft sees no reason to provide meaningful contribution to
open source because in the eyes of Microsoft, open source is already
doing frightening well. That's the thing, I think, that many of us
open source users can't comprehend: *they* are scared of *us*. Who'd
have thought?


## Shallow Source

The thing is, all of this is fine. If that's how Microsoft wants to
treat open source, it's perfectly within their rights to do so. But I
think they'll find it's very shallow. I don't mean intellectually
shallow, I mean it will be like a shallow fountain. Open source
thrives because it is a give-and-take economy. A developer takes code
from one place, improves it, releases it, it gets improved or it's
used as a training ground for someone else, and something new springs
up from that. It is thrives because people come to it earnestly with
honest intentions, and they participate. When they don't, it usually
ends badly; they get disgusted with how open source isn't making them
enough money, or because people are looking at their code and pointing
out errors, or changing it out from under them. And they leave. And
that's not what anyone wants. Participation is valued, and it's also
rewarding.

But if Microsoft expects to treat open source as nothing but a freely
flowing idea-fountain, from which they can draw and draw without ever
giving anything meaningful back, I think they'll find, eventually, the
experience to be unfulfilling. It's not going to bring them the influx
of passionate developers that it wants, it's not going to make their
closed source code any better, it's not going to make people trust
them or love them more. It's going to disappoint, and possibly disgust
them, and they're going to back away, disenfranchised by their own
lack of participation.

And nobody really wants that.

So, hopefully Microsoft will surprise me, and start contributing back
in a meaningful and "selfless" way. They own their code, so it's up to
them to open it. They also, by extension, own the much of the data
their users produce; maybe a good start in earning trust would be to
use open standards, open formats, and honest interest in user freedom
and choice.

Open source isn't a club. Microsoft doesn't have to buy its way in, or
trick us into believing that they are Open. You get called "open
source" by producing open source; start doing that, and things will
have truly changed.

[EOF]

Made on Free Software.

