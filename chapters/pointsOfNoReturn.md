# Points of No Return




At the time of this writing, I've been a Linux user for a long time.
How does one define "a long time"? Good question. Let's ponder.

I've said it before, but it truly does seem like only yesterday that
Linux seemed like a big, mysterious unfathomable puzzle just calling
out to me to be solved. And of course, the more you use it and the
better you get at, the more you realise that you never "solve" a
really good system, you just get comfortable with it. There should,
ideally, never be a point at which you can walk away from a
computational system feeling like it's all sorted. There should always
be a possibility that has not yet been tested, or a combination not
yet tried, or a new corollary to formulate.

That's what Linux is; being completely open source, you can look at
every line of code and learn how to use it, but once you do, you can
hook into it and build your own system. "Solving" open source is the
*start* of open source, not the end.


## Reality Check

One thing I have noticed, along the meandering path that is a life of
open source, is the gradual fading of closed source (for lack of a
better term) "awareness".

This takes many forms:


### No Authority

The first is the way one sees closed source software as the measure by
which everything else is assessed. It's natural. If you grew up using
Microsoft Word, then it's pretty natural that you'll use it as the
basis of comparison for any other word processor you use. Lucky for
me, I was always into "shareware", so I was always downloading new
applications to try out, to the point that it's somewhat a specialty
of mine (at least, that's what people tell me) that I can pick up a
new application, learn it very well, and then teach it to others. So I
don't feel like I've had to get over *much*, but there were definitely
paradigms that I was used to, and which I latently insisted were the
"right" ways of doing things.

Gradually, though, that measure slips away. You just kind of forget
them. I guess it's a little like transitioning from school as a child
into university life. You go to class, you call your professors by
their last names, you ask for permission to do everything until you
finally notice that nobody requires or even wants you to do
that. Maybe a professor just tells you outright: you're an adult now,
do what you want.


### Reaching the Point of No Return, And Liking It

The other form this takes is losing touch with what is even going on
in the closed source world. Obviously, whether you can do this depends
on your daily encounters with closed source operating systems; I've
been lucky enough for years now to not have to work on anything but
Linux at home and on the job. As a result, closed source software is
something that I know exists but pragmatically I have no awareness of
it, much less of what is going on in that industry.

For that reason, I often find myself genuinely surprised when I hear
about the latest antics of some corporate software (or social network,
or service, or whatever). Not only am I surprised by what new
inventive way they've come up with to cheat their customers, but I'm
surprised even more by the fact that their customers continue, to this
day, to pay to be screwed over. Do these people not already have
enough obstacles in their life, that they have to pay a company to
invent new ones for them?

This leads me to two conclusions.

First, I see now that me finding and using open source was, basically,
inevitable. If I hadn't found it and switched years ago, I'd have
found it today. I know myself enough to know that I'm just not the
sort of person to sit obediently while a company takes my money only
to deliver something worse than what I had to begin with, and which in
fact abuses my trust.

Secondly, that I have reached a point of no return. I heard about some
"new features" (to put it politely) of some of the latest proprietary
operating systems this morning, and tried to imagine myself switching
away from Linux. Imagine me losing interest in open source, at least
enough to accept a closed foundation upon which I run a sludge of
proprietary and open source software; pretty much, the way I used to
compute before I knew Linux (or "open source", for what it is)
existed. The question, after these imagined presumptions were made, is
which OS would I end up using?

The answer actually took me my surprise: I was a man without a
country. There's is no closed source OS for me to "fall back upon". If
I decided to walk away from open source (Linux, BSD, Ilumos, and all
the others), there's honestly no OS I could settle on and be
happy. Mac would have been the obvious choice, since that's what I
grew up with, but I've learnt so much about how closed and
aggressively non-standard it is (it's non-standard even when using
standards, if you can believe it), I just can't see myself wanting to
deal with that. I have no experience with Windows but have been told
it is easier to program for, but frankly their security (I don't
mean vulnerabilities as much as I mean proven underhandedness) track
record, their ideology (crush the competition by any means necessary,
even if it means not playing fair or in the best interests of your
customers), and arbitrary roadblocks (pay-to-play features) don't
quite match up with my ideas of a good time.

I can imagine other scenarios, like going proprietary-Unix or
wondering off into a land of obscure EOL stuff (like RISC OS, OS/2,
and whatever else), or opting for a research or hobby OS (Plan 9,
Kolibri, Haiku).

The point is that somewhere along this adventure, I've crossed a point
of no return. I have settled so comfortably into freedom and
flexibility that I cannot imagine giving it up. I'm not a Linux user
because I like a $0 OS (I actually pay for my install discs via a
subscription to Slackware and RHN, but I absolutely insist upon the
guarantee of not being blocked-until-payment), not because I like the
community (although I do), the geek cred, the desire to go against the
grain, or even to reject capitalism-over-humanitarianism. I'm an open
source user, in a way, because open source is the only system that
delivers on the promise of intelligent technology, and delivers the
liberty and autonomy to do what I want.

Why would I ever subject myself to anything less?

Well, I wouldn't. And I'm quite happy to say so.

[EOF]

Made on Free Software.

