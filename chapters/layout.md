# The I-Told-You-So Unix System Layout

![](images/files.png)

Lately some people have gotten up in arms about the complexity of the
layout of the Unix filesystem. They say it's over-complicated, it
smacks of archaicism, and surely it ought to be simplified.

For a while, I was cautiously receptive to this idea. I'm still
partially receptive; I'm not sanctimonious about the filesystem
layout, but I do feel that the gut reaction of *oh, that's too
complicated* is often borne of not having a personal use-case for all
of its features.

Two things reminded me of why the layout of the unix system was such
a smart design: I got an SSD drive, and I started designing multimedia
workflows for people and places larger than, say, two or three friends
making movies in their backyard.


> Just as an aside: if you're not using an SSD drive, you should get
> one. Now. It's 2016 (at the time of this writing), SSD drives are
> beyond blazingly fast, and you should have one.


The thing about the system layout of a Unix or unixy system is that
it's modular. We UNIX folk beat the drum of modularity a lot, almost
to the point that it loses meaning. We say "keep it modular" or "do
one thing and do it well", which prompts the question "if you like
modularity so much, why does the `ls` command have 57 flags available
to it?", and the truth is that yes, sometimes modularity is difficult
to nail down.

In this case, though, the lines are pretty clear. And that's the nice
thing about the system layout: it softly delineates parts of your
computer, one from the other, so that you have the finest-grained
control over where your data is stored.

Why does this matter?

Admittedly, on a single-user system, it doesn't. Or rather, it didn't.

Let's back up: a long time ago, it mattered very much, because
computers were multi-user, hard drive space was expensive, user
permissions were significant, and system design was critical.

Then computers ended up on people's desk and laps, and hard drives got
cheap, and all that data got normalised; throw it in a bucket, call it
My Computer, and let the user sort it out.

But now we're all using SSD drives, because they're super fast, but
also expensive. Suddenly it behooves us to re-think the
harddrives-are-cheap mantra, because yes they are but the fast ones
are decidedly *not*.

And throughout that history, the reality that BIG systems actually
*do* exist has not gone away. To a lot of us home computerists, that's
not a part of our reality, but sometimes you start out as a home PC
geek and end up a Linux consultant for multi-million dollar movies
using multi-million dollar render farms, and you had better believe
that a little bit of flexibility in such an environment goes a l-o-n-g
way.


The Big UNIX I-Told-You-So
------------------------

Here's the thing: this is exactly why Unix "greybeards" and Unix purists
are so dang conservative. For a good 10 years, people had written off
the need to ever think about storage ever again. Hard drives were
bigger and cheaper than ever! Unix should get with the times. Who
needs separate directories for system-dependent executables and
non-essential executables? why bother having an `/opt` or a `/usr/local`?
None of it means anything any more!

Well, first of all, that wasn't true even during the decade of cheap
drives, because big mainframe-like systems persisted. Secondly, it all
comes back around; with SSD, we're back where we started, and had we
thrown out the structure designed 40 years ago then we'd be a lot
worse off.

I'm not saying change is bad, but I am saying that arbitrary change
should be avoided, and just because we don't see a use for something
in our small bubble, it doesn't mean there is NO use for it
anywhere. That's been my experience with a *lot* in Linux and open
source; one year, I'm shaking my head at some "pointless" project
because "who even uses that any more?" and then the next year I'm
working at a place where the very existence of my job relies on that
project. That's a slight exaggeration, but there have been examples
where that's very nearly true. The point is, the world is a big and
diverse place, and just because the one or five or 100 computers you
interact with on a daily basis are configured one way, and the one or
five or 100 users you interact with see the world in one specific
way, I guarantee you there's another institution, company, or country
that sees things completely differently.

And that's why, sometimes, when someone staunchly puts their foot down
and says "I don't want to do it that way, because I've always done it
*this* way", they aren't just clinging to tradition or succumbing to
their own force of habit. Sometimes, there are reasons behind the
conservatism, and it's the *right* thing to do.


Proof
----

Wait, so what's this got to do with system layout, anyway?

Right. My point, I forgot to make my point.

The thing about the layout of a Unix system is that it is modular. It
splits up all kinds of information and throws it all over the place on
a system. Is that confusing? not if you understand it. Is it annoying?
sure, sometimes, but if you anticipate it, then you can actually
override it if you want.

And yet I speak of it as a good thing.

Well, take an SSD drive. It's fast, it's small, and it's
expensive. You don't want to waste space on your ultra-fast drive with
silly things like family vacation photos; you want to put the
*important* stuff on there; the stuff that your computer needs to boot
up, to launch applications, to run code. You want that to be fast.

So you split up your installs, such that executable code and libraries
get installed to the super fast directories, with all the extraneous
assets and documentation files get installed to slow directories. In
this way, you get the performance of SSD where it matters (boot time,
launching common desktop applications and utilities), but you conserve
the available space and lifespan.

I do exactly this on my main workstation; feel free to [read up on the
technical bits](../unix/ssd) about that at your leisure.

The same principle applies to big installs. If you're managing a
massively multi-user system, one of the things you inevitably need is
the ability to easily and predictably determine the install
destinations of various kinds of data. Do your docs need to live on
the same high-priority drive as your production application drives?
should your user data exist on the same drives? can you even afford to
keep your user data in the same data center or does your backup scheme
require a data center run by robots (I'm actually not kidding about
that).

## Too Many Files?

The perceived problem with the way Linux manages its applications is
that the user doesn't, then, know where all the little bits and pieces
of one application end up; you have binaries in one place, vital
libraries in another, documentation someplace else, and icons and
other assets in still another. How can anyone ever hope to re-assemble
an installed application in order to take it to another computer, or
uninstall it?

Well, how do you keep track of your weekly grocery requirements? Do
you write yourself notes about what ingredients you've run out of, and
then hide the notes, each in different places around the house? If so,
you'll be pleased to know that there's a better way: make a
centralised list. Keep the list in one place, at all times, and jot
down what you need on that list. When you go shopping, take that list
and use it as your master reference.

This, of course, translates to installing applications on Linux,
except that you don't have to do it manually. Computers exist, and so
they can (and should) do it for you.

On [Slackware](http://slackware.com), a master list of all packages exists in
`/var/log/packages`; each file there represents one package, and it
contains a full list of every file installed in that package.

On a [Red Hat](http://redhat.com) system, all installed packages are
listed with `rpm -qa`, and the command `rpm -qpil foo.rpm`
provides a full list of every file contained in the package.

Under normal circumstances, you'd just use RPM on DNF or pkgtools or
whatever to remove a package, or to install a package. But if you are
trying to "extract" an application from one computer to take it to
another, as long as you have a list of the files, the actual work is a
fairly simple `find` command away.

But wouldn't it be so much easier if we just kept all the files together?

Sure! But there's a downside. I found a random Mac sitting around the
office and ran `du -h` on its `/Applications` directory. The result? About 10GB.

That's 10GB for the userland apps.

On my decked-out, install-every-app-I-can-find Slackware machine, the
sum total of my `/usr/bin` directory is not even 1GB. But my binaries
aren't self-sufficient, so let's add in /usr/lib64. Much heftier;
about 6GB.

Both of those, I have installed on my 16GB SSD drive. That means that
my SSD drive is not yet half full, and I've installed everything I
will ever need, and then some. The random Mac I found around the
office would have eaten most of my SSD drive, and that's just from
having a few office-type applications installed. I guarantee if you
made me use it as my own machine, the application directory would
double from the kind of apps I install (well, no it wouldn't, because I'd install Linux, but I digress).

The point I'm making here isn't that Linux applications are smaller,
because they aren't. The point is that they're better distributed. I
didn't even bother with `du -h` against the directories containing all
the assets (icons, widget images, splash screen graphics, and so on)
or the documentation, because on Linux I don't have to keep them on my
SSD drive (and so I don't).

Can you hack around it on a Mac? of course you can. Does Windows
manage apps modularly? No idea. I'm just saying that modularity is
powerful, and the fact that Linux embraces it is, as long as you know
how to manage it, a **Good Thing** because you can leverage it when
you need to.


## Keep it Modular

Yes, a modular system layout is a big deal. It's an important part of
well-designed computing, and understanding it is important if you're
going to speak up and call for revisions. And I say that unless you've
sat down and custom-compiled your applications, struggling with
`cmake` flags and a fair number of sed hacks, to get your doc stack
and icons and fonts to go to a lesser drive, then you don't understand
why the system layout of Unix is complex, and so you're probably in no
place to call for its overhaul.

That's why the unix system is laid out the way it's laid out. Now that
you know, you can perhaps appreciate it a little more, and whether or
not you "get it" or not, you certainly can see how many people find it
very useful and very important.

[EOF]

Made on Free Software.

