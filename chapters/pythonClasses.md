# Classes and Functions




If you came to Python from a scripting perspective, as some sys admins
and most beginning programmers do, then the concept of functions and
classes can be difficult to understand and leverage. Some teachers
describe a class as an "object", which in some cases does reflect how
classes behave. Others call a class a self-contained (but not
necessarily *self-sufficient*) data set, which also describes classes
pretty well. Mostly, the best way to understand classes and functions is
to use them, so let's learn by doing.


## No Functions, No Class

First, let's see what a Python application looks like without
classes or functions. This is how most new programmers start, or sys
admins who are used to BASH:

    #!/usr/bin/env python

    FirstVar = 1 
    SecondVar = 2 

    print(FirstVar + SecondVar)


(This would of course print **3**.)

The variables created here are global variables; they just hang out in
common areas and are available to anyone that needs them. It's simple
and convenient. However, it can get messy and confusing if you are
dealing with lots of variables, and in larger applications it can also
get unnecessarily memory-intensive.


## Function

Functions are most often used as formalised methods of repetition. 

If there is a task that you know needs to be done again
and again in your program, group the code together into a function and
call the function as often as you need it. This way, you only had to
write the code once, but you can use it as much as you like.

The other reason you might use a function is to keep variables
insulated from the rest of the program. There is no need to keep
holding temporary variables in system memory or in namespace when you only
needed them to hold a bit of info while you did some math. Isolate
them in a function, use them as the function is running, and then let
them fade away when the function ends.

Here is an example of a function, with an emphasis on how it deals with
variables.

    #!/usr/bin/env python

    worldVar = 12

    def Boxxy():
        boxVar = 1328
        print(boxVar + worldVar)

    if __name__ == "__main__":
        print(worldVar)
        Boxxy()

    print("Failing...")
        print(boxVar)

Run the script:

    $ python ./boxxy.py
    12
    1340
    Failing...
    Traceback (most recent call last):
    File "./boxxy.py", line 16, in <module>
       print(boxVar)
    NameError: name 'boxVar' is not defined


Notice how everything but the final request works perfectly. The
**worldVar** is a global variable so it can be accessed from anywhere. No
big deal there. The **boxVar**, however, is confined inside of its **Boxxy**
function, so the final attempt to **print(boxVar)** fails, since there is
no such variable outside of the function in which it was created. It
is considered a *local variable*.


## First Class

Sometimes functions are not enough, because you want your local
variables to be shared among several functions, but not with every
part of your program, or you need to design a component of your
application and then call it into being several times.

You can isolate variables by creating them in **classes** and **methods**. By
doing this, they only get used when needed.

    #!/usr/bin/env python

    class FirstClass(): 
        def __init__(self):
            FirstVar = 1 

    print(FirstVar)

This will fail:

    $ python ./classy.py
    Traceback (most recent call last):
    File "./classy.py", line 7, in <module>
       print(FirstVar)
    NameError: name 'FirstVar' is not defined
    

This is telling you that the variable FirstVar, essentially, does not
exist. It does not appear to exist because it was defined within a
class, and so it is closed off from the rest of the world. This is
great when you need to create a bunch of variables just as temporary
holding places while you work out some self-contained problem. Or,
more importantly, when certain variables *must* only apply to that
class: for instance, in a video game you might create a variable to
represent gravity. This variable constantly pulls an object toward the
[game's] Earth, just as it does in real life. Unlike real life,
however, video game gravity if applied globally would also cause the
earth itself to fall, so you certainly would not want that gravity
variable to affect objects outside of your player avatar or else
everything would fall right off the screen. So you *must* insulate that
variable.

## Getting Things Into a Class

There are two ways to get a variable into a **class**. The first way, you
have just done: create a **class** and inside of that **class**, create a
variable. Unless you deliberately send it out, that variable will stay
forever locked within that **class**.

The second way to get some information into a **class** is to send it there.

Now, remember, **classes** and **functions** are barriers. By design, variables
cannot just walk out of a **class** or a **function** at will, nor can they walk
right on in uninvited or with the right credentials. To get a variable into a **class**, you need to pass
the variable in as an argument. I am not a programmer by training, so
that's a fairly obtuse concept for me, but if it helps, you can sort of
think of portals. The portals you will use are parentheses: ()

For example:

    #!/usr/bin/env python

    class First(): 
        def Blue(self,portal):
            print('running Blue method')
            print(portal)

    if __name__ == "__main__":
        Player = First()
        Player.Blue(13)
        print(Player.Blue(14))


In that example, we create a class called **First** and a method within that
class called **Blue**. At the very bottom of the application, we call upon
the **First** class to run, first by assigning it to the variable **Player**,
and then again by printing it with the Python **print** function.

But most importantly, notice the parentheses in the method declaration:
*this* is the portal through which we can send information from the
outside world. First, we submit **self** to the method.

The second thing we define for our portal is *some placeholder* for an
incoming variable. It doesn't have to be any special word; I call it
**portal** here just to be demonstrative but I could have called it **penguin**
or **myVar** or **whatever** (or whatever). The important thing is that the method now knows
that in can expect (and indeed, *demand*) a variable to be handed to it
whenever it is called.

In the part where we actually use our class, we submit an argument;
first, we provide the integer **13** and next **14**. As you can see from the
results, the function takes in the argument as **portal** and prints it
out for us, doing this regardless of whether we invoke the **class** on its
own or use it from within some other function like **print**.


## Making Functions in a Class Communicate

Notice the problem we get when we do something like this:

    #!/usr/bin/env python

    class First(): 
        def Blue(self,portal):
            cake = portal
            print('running Blue method')
            print(cake)

        def Orange(portal):
            print('running Orange method')
            print(cake)

    if __name__ == "__main__":
        Player = First()
        Player.Blue(42)
        Player.Orange()


That code renders an error because the **cake** variable is not known to the
Orange function. To let **Blue** and **Orange** share data, you need to use
**self**; it is one of **self**'s jobs to facilitate inner-class communication.
So to fix this problem, you need to do two things:

1.  bring **self** into both functions that need to share data, in this
    case: **def Orange(self):**
2.  make the data needing to be shared an attribute of **self**, for
    instance:

        self.cake = portal

    and

        print(self.cake)


After making those adjustments, the program looks like this:

    #!/usr/bin/env python

    class First(): 
        def Blue(self,portal):
            self.cake = portal
            print('running Blue method')
            print(self.cake)

        def Orange(self):
            print('running Orange method')
            print(self.cake)

    if __name__ == "__main__":
        Player = First()
        Player.Blue(42)
        Player.Orange()


And so it outputs **42** as expected. 

And as you can see from the code, you
only fed Python the number **42** once, and it retained in even when you
called upon the class later to run the **Orange** function.

![graphic showing the flow of data](./images/portal.png)

### Getting Data Back Out

Sometimes you need to get data back out of a **class**. Maybe you need to
get the results of an equation, or maybe you don't need to see the data
itself but you do need a **Boolean**, like **0** or **1** just to see if something
worked.

The **__init__** function of Python returns **None**, and is always expected
to return **None**. Like Unix itself, Python assumes that silence means
success, so if you have **__init__** return anything but **None**, Python is
forced to assume that **__init__** (the very function that spawns a
class object when a class is called) failed. 

You don't want that, so do not try to get feedback from an **__init__** function.

All other functions are fair game.

If you are running a **class** in order to get information out of it, then
what you really want to do is find *the result of* the class. When ever
you hear or think the phrase "the result of" in programming (not just
in Python) you can bet there will be a new variable involved. Why? because
you give the computer data via variables, and the computer gives *you*
data via variables. Variables is how you and the computer communicate.
Get used to it.

We have this program:

    #!/usr/bin/env python

    class First(): 
        def Blue(self,portal):
            print('running Blue method')
            print(portal)

    if __name__ == "__main__":
        Player = First()
        cake = Player.Blue(42)
        print(cake)


Run that and you will see that the final print statement renders "None".
This is because the **First** function, as which all functions, returns
nothing by default. It is safe to assume that getting silence from a
process means that it successfully ran, because when programs fail,
they spit out error messages.

But in this case, we want the program to return a value for **cake**. So
make it do just that by adding the line **return portal** to the end
of the function:

    #!/usr/bin/env python

    class First(): 
        def Blue(self,portal):
            print('running Blue method')
            print(portal)
            return portal

    if __name__ == "__main__":
        Player = First()
        cake = Player.Blue(42)
        print(cake)


Trace the flow of data and you see that now the input (**42**) gets fed into
the portal argument of the **Blue** function, it is printed, and then it is
returned as **portal**.

Where is it returned *to* exactly? well, it is returned as *the result
of* the class, so it is dumped into the **cake** variable.

This is why, when we print **cake**, the number **42** appears.

You can, of course, return anything you like. You do not have to return
the same thing that you fed into the **class** in the first place. For
instance, try **return 100** or **return time.localtime()** (well, you would
have to import the **time** module for that) and see what happens when you
print **cake**.

## Now You Know

That's everything you need to know in order to comfortably move data
into, within, and out of a Python **class** or **function**. The more you practise, the more
natural it will feel.

Have fun!

[EOF]

Made with Free Software.

