# Analogue Random Number Generation

Some time ago, I realised that sometimes I needed random numbers to
play a quick RPG or dungeon crawl adventure. This would often happen
when playing a solo, scripted game, especially something like [Lone
Wolf](https://www.projectaon.org/en/Main/LegendsOfLoneWolf) or a solo
adventure for [Tunnels &
Trolls](http://www.drivethrurpg.com/browse/pub/3094/Lone-Delver-Games),
so I wouldn't have a die on me (or else, no place convenient to give
the die a proper roll).

I needed some method of producing random numbers, within a specific
range (usually 1 to 6, but sometimes 1 to 10), without dice, and
without relying on my own subconscious bias.

While I was at it, I also thought that a no-electronics solution might
be nice, too. Sure, you could grab a mobile phone, find a dice rolling
app, and you'd be done, but I'm not fond of mobiles and part of the
appeal of "tabletop" gaming, for me, is that it doesn't rely on
electronics (or relies on extremely-low powered electronics, such as
an e-ink ereader, which gets about two weeks of life on one
charge). So I wanted something independent of heavy programming and
power consumption.


## How Random is Random?

Ask any Linux geek and they'll tell you: random numbers are harder
than you think. When you get started in Linux, it seems like one of
the things you get told pretty early on is that randomness in
computers is basically impossible to generate, because a computer
really is a closed system. Depending on what you define as a
"computer", it may be a *big* closed system, but if you're asking a
computer to provide you with a random number, you'll find that the
results are, by some measure, predictable.

If you introduce humans into the equation, you can do a little better;
tell the computer to, for instance, check for cursor coordinates in 3
seconds, and then tell the human to draw a picture or to just start
wiggling the mouse around the screen.

Sounds random, right?  Well, it's strange, but the more you do
something, the less random it gets. You start to notice patterns, like
maybe most humans start their mouse position in the upper left corner
of the screen, or maybe they tend to ignore prompts to move the mouse,
or whatever.

What most people find (outside of encryption, ideally) is that "random
enough" is good enough. If I ask a computer for a number and get back
some number that I could not predict that I would get, then I call it
"random".

But that's not actually random; it's "unexpected".

And for most fun and game applications, that's good enough.


## Unexpected Number Generation

Let's say I roll a d6 (six-sided die) three times and get back 1, 1,
and 1.

That's kind of disappointing, isn't it?

Totally random die roll, but the results aren't as satisfying as when
I roll, for instance, a 1, 6, and 4.

In fact, a 1-6-4 roll looks totally random compared to 1-1-1, which
just feels lazy.

So even if I really did randomly roll a 1-1-1, if I told you that I'd
rolled 1-6-4, the latter feels more random to most of us than the
former.

Likewise, if I asked you to pick a card from a deck, and each and
every time you picked a "random" card, I was able to tell you exactly
what you'd picked, the feeling of randomness fades quickly. You'll
either call me a magician, or you'll accuse me of playing with a
marked deck (or both); the idea being that predictability equals
non-random. But if I was only right about 50% of the time, then
suddenly it's random again. This is, obviously, how and why hustling
works.

So, what I was looking for is a way to hustle my own brain when
selecting numbers.


## Big Numbers, Divided

The first idea I had was to mask my number selection by just drumming
up very large numbers in my head, which I would then divide by 6 until
I got the number down to a value from 1 to 6 or 1 to 10.

Example: I choose 762. I have no idea what that reduces down to,
because I'm bad at math, so I start the calculations: I know that
6*100 is 600, and my number was bigger than that, so maybe 120? 6 goes
into 120 60 times, and it goes into that 10 times. So now I have 10,
but I never resolved that trailing 2, so let's call it 12 divided by 6
equals 2. There, I just rolled a 2.

That seemed pretty practical until I realised that poorly-done math
could be manipulated to give me any number I needed, and math done
well was just too much work. I felt that a simple die roll shouldn't
take me out of a game just so I could practise long division.

## I Spy...

My next idea I had was to just look around the room, choose something
to count or some attribute to categorise and then convert to an
integer value.

For example, I might arbitrarily propose that I'll look
for all sources of artificial light in a room. And then I'll count
ceiling tiles across the room. And then the tiles spanning the length
of the room. And so on. Any number higher than 6, I might divide by 6,
or 3 until I'm within range.

Or I might look at some object, identify its colour, and then take the
first letter of the colour name and convert it to a number
(a=1,b=2,c=3, and so on). That seemed practical, but again it was
subject to a subliminal bias, or possibly just an obvious pattern,
where I would fixate on certain colours, or suspect myself of
subconsciously choosing colours that ranked higher than others.

## Modulo

The problem with the choice-obfuscation methods were that there was no
baseline for what is or is not "random". I can come up with numbers in
my head, but how can I tell if it was predictable or not? I might not
be able to rattle off the result of long division, but how can I be
sure that my brain isn't playing tricks on me, making me win each
combat roll?

Even so, math seemed like a promising path. Within just one or two
internet searches, I found a classic old trick from early encryption
that could produce unexpected numbers. It's based around the
**modulo**, which is similar to the "remainder" principle in division
you were taught in school.

This method requires two seed numbers to get things started, but after
your first number it self-generates.

To start, choose two seeds. Let's go with 4 and 6.

Take your two seeds and add them together, dividing the sum by 6 (the
number of sides on your imaginary die). If 0, replace with the sum.

(4+6=10) mod 6 = 4 (6 goes into 10 once, with 4 left over)

Now take your result (4, in this example), and add to the *latest*
seed (6, in this example) and repeat the process.

(6+4=10) mod 6 = 4

So you've rolled a 4.

Next roll, do the same: previous seed plus your result:

(4+4=8) mod 6 = 2 (6 goes into 8 once with 2 remaining)

Again:

(2+4=6) mod 6 = 1

(1+4=5) mod 6 = 1

(1+1=2) mod 6 = 2

And so on. It worked, but its results were a little
annoying; depending on your seed numbers, the results could feel very
random (try 2 and 3) or they could seem like you were trapped in an
infinite loop (try 3 and 6).

I liked that it was, itself, eternally progressive; it self-generated
new seeds after the initial ones, and it was just complex enough to
discourage the brain from predicting, but just easy enough to do
without stepping too far out of the game.

This method is a real contender, and definitely a gem to know in a
pinch. If you have nothing else, you have progressive modulo.

## Shifting Tables

From these ideas, I came up with the idea of a shifting table that might make predicting the result difficult. My idea was to generate a little table of numbers like this: 

| 1 | 2 | 3 | d |
|---|---|---|---|
| 4 | 5 | 6 |   |

To use it, you pick a number and then step through the table in a
north-to-south, left-to-right progression. The first "roll" is the
seed.

For instance, if I pick 3, then I count three steps from 1 to 4 to 2. Keep that in mind, and pick a number for your roll. Let's arbitrarily choose 3 again.

First, shift the table by 2 (this produces the "unexpected" part of the equation):

| 2 | 3 | d | 4 |
|---|---|---|---|
| 5 | 6 | 1 |   |

Then "roll" 3 spaces: 2 to 5 to 3.

The result is 3.

For the next roll, let's choose the number 5.

First, shift the table by 3 (our previous roll):

| 5 | 6 | 1 | 2 |
|---|---|---|---|
| 3 | d | 4 |   |

And move 5 (our chosen roll): 5,3,6,d,1.

So we rolled a 1.

And so on.

The value of **d** is "slide up or down". So if you land on **d**,
then slide one space up, which in this case would have rendered the
result of 6.

This method works well enough; it's low on math, easy to use, and it's
pretty unpredictable since you are constantly shifting the position of
the numbers within the table. The trick really is that you declare
your roll *before* you shift the numbers, so you're tricking your
brain and forcing unexpected results.

Its weakness, I felt, was how linear it was. I was afraid that after a
few rounds, I'd get used to how the table shifted, even if only
approximately. There's no real way to re-seed, because no matter what,
the shift is in one predictable direction, without variation.


## Pocket Dice Roller

I liked the ideas of *unexpected* numbers over random numbers and
progressive seed generation, but I still wanted something as simple as
a dice roll (simpler than maths). I kept these notions in mind, and
then two things happened: I attended a zine-making workshop, and I
downloaded [Dungeon
Delvers](http://www.drivethrurpg.com/product/118558/Dungeon-Delvers).

At the zine workshop, I learned about a foldable, single-sheet
booklet, which online seems to be called the "pocketmod" but I don't
know who invented it. And then when I downloaded **Dungeon Delvers**,
I found that its rules came as a "pocketmod". It all came together: I
could use this funky zine trick to create a gaming utility!

From there, the concept was obvious to me: a small booklet with a
table of numbers on each page. Seed the process with any number; turn
to whatever page number you have in mind, and see what number is
there. This is both your "roll" result and the seed for your next
roll.

![](images/pocketdiceroller.jpg)

That concept quickly developed into a system using letters and numbers
(using numbers for both page number and the roll result gets
confusing), multiple choices per page (your letter choice resolves to
your die roll, the die roll becomes the destination page for the next
letter you choose).

To make things even less predictable, I made the booklet flippable and
floppable, so at any point you can "mix up" your results (that is,
confuse your expectations) by turning the book over, or flipping the
book upside-down. This provides 4 entry points, each of which with a
different result.

Since your seed is independent of your roll choice, you can change the
direction of your "roll" at any time, effectively pulling the rug out
from under yourself if you feel that your brain is remembering
pages.

The booklet is, like a card shark's deck (cutting a deck of cards does
not shuffle, it just offsets the "starting" point), an infinite
loop. There are no page numbers, so you can start counting from any
page that you arbitrarily call "page 1", and get a completely
different result than if you performed the same roll from a
*different* "page 1".

To make it more versatile, I also added d10 grids to each page,
meaning that the book serves as a d6, d12, d10, and d20 roller.

The booklet prints on one sheet of paper, so it can be made easily at
home, and it fits nicely into your wallet.

Since many of my RPG materials are independent or from small
publishers, a lot of it comes as PDF or (thankfully) EPUB. I figured
there was probably also a market for a dice roller that is
non-calculative (since many ebook readers do not run even simple
scripts without a considerable amount of hacking). For that audience,
I adapted my Pocket Dice Roller to `.epub` (it's written in
[Docbook](http://docbook.org), so really it can generate any half-way
sensible ebook format).

In a way, it's simpler than the paper edition, since there's never any
sense of where in the book you are (unlike the paper one). As long as
you just keep pressing letters and numbers, the seed is unexpectedly
shifted, and get an unpredictable roll result.

The solution is a simple example of analogue programming. As long as the user is invested in not knowing the starting point, the user can "randomise" the seed and produce unexpected and unpredictable results every time.

Hopefully, it's a useful system. It's published as a [Creative Commons](http://creativecommons.org) project, so improvements and variations are welcome.

The source and printable renders are available from [gitlab.com/notklaatu/pocketdiceroller](http://gitlab.com/notklaatu/pocketdiceroller).

[EOF]

Made on Free Software.

