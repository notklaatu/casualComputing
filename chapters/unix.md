# What's So Great about UNIX?

I'm a fan of Unix.

I use Slackware Linux at home and as the foundation of my consulting
"business" (the thing that makes me money; I put it in quotes because
I don't have employees or anything, it's just how I sell my skills)
because out of all the Linuxes, it strives to be the most
Unix-like. I'm a fan of that.

But why do I like Unix?

## Terminology

First, let's address terminology. I'm saying "unix" here very
deliberately. I consider **Linux** to be a sub-set of a bigger and
generic thing called **unix**. Technically that's incorrect; Linux is
actually a *clone* of a product called **UNIX**, written by two AT&T
employees Dennis Ritchie and Ken Thompson (I got to meet the latter at
an Ohio Linux Fest, in fact).

Really I should be saying **POSIX**, because that's the
intentionally-generic, broad specification of anything that aspires to
be very unixy without actually having been written, necessarily, by
AT&T. The "problem" with the term POSIX is that it's not quite as
recognisable, and to many who do recognise it, there's a connotation
that we are referring to a set of technical specifications rather
than, I guess, the tech specs, the tendencies, and the culture.


## Why I like POSIX

I'm a fan of POSIX.

And when I say POSIX, I mean all of what I was just talking about: I
mean the technical specifications that define what UNIX is, I mean the
history of AT&T UNIX and Berkeley UNIX and that Finnish-borne indie
clone, Linux, and I mean the computer hacker culture that has been in
development since at least the 60s.

The thing is, if we imagined for a moment that all software was free
and open source, I would still prefer to use Unix over anything
else. You might think I'm only saying that because there *is* nothing
else, but that's not true; [Haiku OS](https://www.haiku-os.org) (a
free implementation of BeOS) is open source and I don't use it. So,
removing the question of independence from the equation, why do I like
Unix?

Well, at this point, there's a degree of familiarity; I have very
intentionally taught myself Unix, so I know its command syntax, I
understand its basic concepts, I understand its file system layout,
and I have a pretty clear list of troubleshooting steps to follow when
I see errors. For that reason, even if, say, Windows were suddenly
open sourced, I wouldn't be likely to switch to it.

But that has been very intentional; I had to come around to the POSIX
way of thinking in order to achieve that degree of comfortable
familiarity.

So a better question might be: why do I choose to like Unix?


## Designed for Power

Since POSIX was designed to drive super computers, there's really never
the sense that we users are "herding cats" when we ask a room full of
computers to act in tandem. We are not *wrangling* 10 or 20 or 100 or
1000 computers together to work toward a specific task because that's
what they've been designed to do. And when we use a POSIX box as a
personal computer, we're really just borrowing a node from the network
and using it on a smaller scale.

And it *is* a matter of scale. There's no "compatibility" layer
sitting between the personal computer OS and the super computer OS;
it's the same OS, seeing the big picture, all the time. That's not
just a matter of esoteric pride, that actually means something: you
can learn POSIX at home, you can set up a little two or three person
network with whatever you have on hand, utilising native technologies
like X11 and NFS, and build yourself a nice little unified computers
system. And then you can take those same skills and walk into a big company
and actually put them to use on 1000 computers, ideally for a
paycheck. That's a big deal, and the very definition of scalability.


## Irresistible Flexible

Everyone always told me that POSIX was flexible, and as a new user I
found that to be mostly true; I was elated to find that I could choose
pretty much *everything* about how I interacted with my computer. Not
only could I choose what my desktop looked like and how it operated, I
could choose whether or not I even wanted a desktop at all. It was
amazing.

But after a while, I started getting a little frustrated, because it
seemed like all the really powerful and meaningful "flexibility" that
everyone talked about was out of reach to me. Sure I could choose
whether my desktop had a task-bar and start menu or just a blank screen
with a right-click pop-up menu, and I could route audio in and out of
applications, or run a modern OS on an "EOL" computer, but I wasn't a
programmer, so how could I, a lowly user, take advantage of this
"flexible" environment?

I got my answer once I started developing personal preferences with
regards to what tools I used on a day to day basis. I didn't even
notice, at first, because I'd become so accustomed to how POSIX
deferred to my will, but it was sublimely easy to get my computers to
use the applications and settings that I wanted it to use. It's so
easy that you forget you configured them that way; it wasn't until I
sat down at a mate's [Linux] computer at work that I fully realised
just how heavily my user environment had really become. I'm not
exaggerating when I say that I'd created my own personal OS; I mean,
not literally, but if the pre-POSIX me were to see my post-POSIX
computers, he would be utterly amazed. He would want that kind of
power himself. And I'd have to tell him to wait a few years, but that
it **would** sneak up on him.

It does sneak up on you; you don't really realise how easy it is to
change how something works, on a whim, until you sit down at a
non-POSIX (or functionally non-POSIX, if you're pedantic) and try to get
the thing to comply with what you want. Sure, there may be some
surface-level customisation, but mostly it's an uphill battle. Every
time you want to change something. *You* try to get your non-POSIX OS to
agree to use a third party application as if it was native, and I
don't meant use it *sometimes*, I mean use it *all the time, for
everything*. Even if you succeed, I'm taking bets on what'll happen
after a routine update. And anything vaguely lower-level than the
obvious GUI apps are even worse. There's just no underlying scheme for
it, because it's not the intended use.

And here's the thing about me: I have a low level of tolerance for
work-arounds. If you don't want me to do something with your
technology, then about 7 times out of 10, I would rather just make my
own (or find a pre-existing alternative) than work around your bad design.

POSIX design trends agree with me on this.


## Defining POSIX

I use BSD from time to time, either on a spare laptop or on a server,
depending on what's available to me, because it's a solid 
and beautifully designed system, and I like to experience various
"flavours" of POSIX (I'd lumped Open Solaris into the mix at one point,
although since Sun's demise, the open version of Solaris has suffered a few
setbacks, but it's coming along).

One thing about POSIX that becomes apparent only after you have tried
more than just one variety is the, well, POSIXness. Sure, you can try
Slackware Linux and then give Red Hat a try, and you'll see
differences in how some things work and you'll marvel at how there's
still a familiar strain of "POSIXness" to them both. But try BSD or
Solaris after you've used Linux for a while, and you're almost
intimidated initially. The commands are different: some don't exist
and others have the same name but react differently than they did on
Linux. How can this be a good thing, and why am I saying there's
consistency to POSIX?

Well, commands are commands. and the more comfortable you get with
them, the less they feel like magical incantations and the more they
feel like what they are: applications. They're the same as anything
else you run on a computer, even though they live inside a little
green-on-black text-only window. So the more you get comfortable with
POSIX, the less frustrating commands become when you encounter a new
one. It's just a matter of learning how it works, and then putting it
to good use.

That's one side of consistency in POSIX that I'm talking about, but the
other is how POSIX "works". Once you understand it, you understand it
across the spectrum.


## Stability

The stability in POSIX is something I value highly. I don't really
understand the excitement a lot of people seem to express when new
software versions are released. I theorise that it's a learned
response from marketing campaigns; we're "supposed to be" excited, so
we are, even though in a week's time we'll predictably be in tears
because our computers are borked and we have deadlines to meet.

I prefer the stable computing experience, at least as my default. And
POSIX is well-known for that. This extends, I have found, to the
culture of POSIX; the ideal state of POSIX admin is one where everything
is working as designed, and updates *fix* problems. You don't find
that in all sys admin culture; I can't prove that, because it's just
been my experience, but I feel comfortable in saying that you'd be
hard pressed to find a serious POSIX admin who didn't hold stability as
one of the measures of the success of his network.


## User Control

Granularity is a bit of a black hole; you can start talking about how much you
love to have the final say in what happens on your computer, but you
might go pale when 300 updates queue for your review. That aside, POSIX
has a tendency to expose this granularity, and generally you're the
one who decides both what you need to review, and what you're willing
to hand off to the magic of upstream sources.

This goes for package management as well as for pretty much everything
right up to your daily work itself. You want to blindly accept
whatever your distributor gives you? you can do that. You prefer to
manage a few important aspects, or customise your desktop, or use a
special driver? you can do that. Or would you rather take control of
every little decision needing to be made? well, you can also do that.

It's POSIX. You're the boss.

[EOF]

Made on Free Software.

