# Making the Simple Complex, and Charging for It




I recently found a very subtle, basically innocuous example of how a
simple system can be made unnecessarily more complex. In itself, this
isn't that big a deal. It's something I would probably get annoyed at
initially, and then forget about once the lesson has been learned. But
looking at it from a different angle, I realised that in a way it's a
metaphor for something larger and more problematic.

So, the example is this: environment variable in Linux are sublimely
easy to set. If you're a BASH or Bourne-like shell user:

    $ export FOO=bar

Or if you lean toward a C-style shell:

    $ set FOO=bar

Either way, you have just created and set a variable called `$FOO` to
the value `bar`.

Not a big deal, except it is; environment variables define the very
reality of a Linux shell and are hugely powerful. The fact that they
are so powerful but so simple to set and use, and so flexible (do you
want the variable to last for just one command, or for the duration of
your current shell and its sub-shells, or now and every time you ever
log in?), makes controlling Linux fluid and dynamic. A well-versed
shell user can define and re-define environment variables between
commands; and some do. I do, myself, because I package applications
for others to use, so I frequently set variables before building, or I
set variables prior to running a test, or I set variables at work to
define what version of a multi-version install of an important
application.


## How to Break Simplicity

This system works really well. It's available to users for personal
work, it's available to admins to define presets for all users, it's
dynamic, it's scriptable, and most importantly it's really simple. I
mean, by definition, it's simple. Key/value pair stored in memory
during a session. Can't really get simpler than that.

So how can we make this beautifully simple system and needlessly
complicate it?

I don't know why we would want to complicate something so effective,
but as a mental exercise, the first thing we could do is to take
direct access to environment variables from our users. No more
shell-based interface for them. Instead, we'll introduce a GUI, in
which values can be viewed.

But just storing those values in a text file sounds pretty simple;
that is, after all, what files like `.profile` and `.bashrc` do, so
storing the key/values in a text file is too easy. Instead, let's
introduce a syntax to define what the key/value pairs are, and then
we'll define the variables, and store it in a special hidden
directory, and we'll do all of this in XML, because nothing says
"over-engineered" like using a verbose format like XML for a simple
key/value pair.

So what we have done now is, essentially, made a Windows Registry out
of an otherwise simple system. Would there be hacks around this? yes,
of course; we haven't made it impenetrable, we've just over-engineered
something that has successfully been in use for decades. We have
applied the "if it ain't broke, fix it" principle to something that
definitely was not broken.


## Simple is Pretty

Don't get me wrong: I'm aware that there's "simple" and then there's
*simple*. We see it every day, and it usually has everything to do
with familiarity.

For instance, if someone asks me to install Wordpress, because it's
too complicated for them, I can honestly and humbly say "Sure, that's
simple" because I know the alternative. It could be hard to install;
it could be a project that requires me to install lots of patched PHP
libraries by hand, and then copy over files, and re-create a directory
structure that devs forgot to include in the tarball, and then edit a
bunch of config files, manually create a database from scratch, alter
the way my web server runs, and then hope against all hope that it
keeps running. But for this user who doesn't do server maintenance
every day, it's not "simple".

Wordpress has a "famous five minute install" or something like that;
it's seriously simple. In fact, it can be *even simpler*, because if
you pay for a web host, nearly all hosting providers have a one-click
install system that spins up a Wordpress instance (database included)
for you. In one click. And yet even this is too complex for some
people, because they don't even understand the concept of servers and
web hosting and installing software on The Internet.

The problem? Either nobody is teaching them or they aren't willing to
learn.

Either way, that's fine. If something simple to me seems complex to
you, that's OK.

However, there is a difference between something *simple* and
something that rides on familiarity to appear simple. Introducing a
GUI application to parse and view an XML representation of clearly
delimited key/value pairs, and removing direct access to these
definitions, is not making something simple, it's just bringing
something into an existing paradigm, even if it doesn't belong there.


## And Passing the Charges onto You

Who in their right mind would do such a thing? Well, to be fair it
happens more than one might realise. The world is full of
"user-friendly" front-ends to interfaces that are, to people who are
familiar with them, simple. We could spend all day arguing over
whether any one of them actually has helped or not.

That said, the interface that I've described doesn't seek to improve
interaction with our formerly simple system; in fact, it doesn't even
provide an interface for creating new variables,. That, users will
have to do on their own, still, but they will not be able to do it
dynamically as in the BASH or TCSH shells, they must use a text editor
to generate XML and then store that XML in a specified location.

So why would anyone do this? it seems kind of crazy. For the record,
what I have described is, in fact, exactly how Apple implements
environment variables for any ENV settings that need to be passed on to
its GUI environment. You can read all about it in their Developer
Manual, because why would normal users ever need to have access to
that information?

The short answer is, I have no idea why anyone would do something like
this. Surely Apple could have just integrated UNIX environment
variables into Cocoa. But the question is larger than just why Apple
took the long way round to complexity in this one case; the question
is whether or not people pay for simplicity instead of knowledge.

And the answer is, I guess, that yes, people pay for an appearance of
simplicity. And they pay for it even when what they are getting is not
actually simple; it's removed out of reach. It's something they don't
have to learn, but still its reap the benefits.

And that's the disturbing thing about technology, I guess. As it gets
fancier and more advanced, people take it more and more for granted in
the same way we take clean water for advantage. Should we all have to
learn about these low[-ish] level systems that can pretty much get
taken care of for us, if we let it? Obviously not. Would it be better
to learn about them? yes, certainly; knowledge is good, and
long-lasting, and powerful, and empowering. Is this knowledge readily
available? probably, although to be fair I don't personally know where
to begin to learn about clean water aside from condensation harvesting
and boiling.

So what am I complaining about?

My concern is that the needless complexity is being *sold* as an
improvement, as a feature, when it is clearly in no way a
feature. These "features" are often companies doing their work poorly,
and it gets sold as a benefit to users. Think about the old
"WinModems", those non-standard network cards that worked brilliantly
with Microsoft machines because they went against every standard on
record, but were so thoroughly tied into Microsoft's code base that
the OS could fill in the missing bits and pieces. Think of modern
webcams and scanners and printers; there are great existing
*universal* driver specs for these sorts of devices, and yet still, to
this day in the year of our Lord 2016, devices are being sold that
will fail to work on certain operating systems.

But this is a feature to most people, because the devices are cheap,
maybe, or they're guaranteed to work *for now*.

And since people aren't being taught about computers, or they aren't
choosing to learn, they don't see how tech companies are charging
money for jobs that are incomplete. The people who do notice this are
the outliers, using the systems that are not compensating for the poor
design choices, and when they take note of the inadequacy, their voice
are either lost in the crowd of "it works for me" or they're assumed
to be technological extremists (which we may well be, but that hardly
changes the crime we're pointing out to everyone).


## Keep Hold

The crux of the problem is, more than anything, the very real danger
that society is starting to operate in ways that its populace do not
understand. The complexity is getting so overwhelming that people
can't be bothered to learn how it all works, and they instead grow up
ignorant to the truth behind what has become, in many ways, magic. I'm
not talking just about technology, here; the political system, the
economic system, the banking system are all equally at fault. And
somewhere along the line, people aren't getting educated on what they
need to know in order to function freely. I'm not saying everyone
needs to know everything, but I am saying that people should know
where and how to find real information on the things that matter to
them. It should be available, it should be open, it should be as full
of liberty as most of us claim we aspire for our societies to be.



[EOF]

Made on Free Software.

