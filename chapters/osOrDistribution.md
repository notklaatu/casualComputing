# OS or Distribution




Why are there so many Linux distributions?

This is a question asked by every new user. But it goes deeper; what
*is* a distribution? what do the people creating a distribution *do*,
exactly? why are there so many? what's different about them? when is a
distribution a distribution, and when is it just a "remix" or
"re-spin"?

In fact, the answer is surprisingly simple: distributions don't exist
the way you imagine they exist.

Here's what you think (I know, because I used to think this, too):

You think a distribution is an OS, and you define an OS as a disc you
get from a company, which you put into your computer, install, and
use. There are two operating systems: Mac and Windows. Oh, and a
third: Linux. When you boot a computer, the OS appears, and you do
your work; you install applications, you use them, you print, you
email, you browse the web. When you're done, you shut down.

OK. So, that's basically what an OS is but it's not really what the
word distribution encompasses. Imagine a *distribution* like this: you
buy an OS, and then you hand it off to someone else. They disassemble
the OS, make a bunch of customisations to it, burn it back to disc,
and give it back to you. Then you install it, and start using it as
above.

As you can see, the process is very similar, but in the distribution's
case, there's that extra part where somebody somewhere configures some
stuff for you. And that, in a sense, is the beauty of a distribution;
instead of you having to figure certain things out, it may already be
done for you, depending on what distribution you choose, or a distro
may at least get you a third of the way there. And "there" is defined
as *whatever it is you are looking to do*.

It's multi-tiered, actually:

* Looking to install Linux? well, a distribution that boots to a usable system gets you 100% of the way there.
* Looking to install Linux with a full desktop with a wallpaper of a grassy field and blue sky? any distribution with KDE gets you 98% of the way there, because you will have to go find the wallpaper you want, download it, and change your wallpaper to that.
* Looking to install Linux with a full desktop, grassy field wallpaper, a menu bar located at the top of the screen and keyboard mapped for quick application access? most likely a distribution with the KDE desktop will get you 80% toward that goal. You'll have to find the wallpaper, set it, and then modify the keyboard shortcuts.

And so on. Generally speaking, the more specific your requirements,
the less any distribution meets them. For instance, it's super easy
to find a distribution that will result in a computer that boots to a
desktop. If you rummage around a little, it's easy to find a
distribution that will make an ancient old junk PC boot. But if you
start spouting off orders like "it must have a purple desktop, and
rounded icons, and a needlework application pre-installed" then it
does get difficult to find exactly what you are looking for.

That doesn't mean there's not a distribution out there that isn't
going to try to meet your demands, though. Since Linux is both
independently produced and costs $0 to re-distribute, it doesn't take
much for someone to spin up a "distribution" containing all of their
demands and expectations, and to post it online as a "distribution" of
Linux. They aren't wrong; they are distributing Linux.

But that doesn't mean it's going to be what you want, and at some point in your search, you have to stop and ask what's costing you more: searching the entire internet for your OS soul mate, or picking one and customising it to be what you want.

## Your OS

Linux and open source are not fast food restaurants. They're salad
bars. You can customise it to fit any expectation and any requirement
you arbitrarily declare vital. If that sounds like work to you,
consider that you probably don't exactly run the out-of-the-box OS
that comes with your computer. You know that OS, and so the changes
you make to it feel natural. You don't know Linux, so everything feels
like work. Heck, finding your "start menu" feels like work.

So don't think that you have to customise everything right away. Work
on it. Let it develop. You get to keep all of your changes, so let
them accumulate. Let Linux grow with you as you learn it.

I'm not ashamed to say that my early Linux desktops all stayed as
close to my former OS as I could possibly get them. And back then, it
was never enough. I spent way too much time trying to get little
nuances to be exactly like my old OS.

And then one day a funny thing happened. I realised that I'd done it:
I'd reached the point that my Linux environment was a perfect clone of
my old OS. I was so proud of myself. But wait, that's not all. That
same day, I sat down at a computer at the school where I worked, and
fired up the OS I used to use, because I wanted to marvel at how
totally *the same* they were. Much to my surprise, everything I
remembered about that OS, I'd made up. I had invented all kinds of
things, over time, that were definitely the way the other OS worked,
so I made my Linux work the same way, and I'd been so proud, because I
was "fixing" Linux.

And in fact, I was fixing Linux. I was fixing it for myself, based on
ideas that were being invented in my own head as the way a computer
*should* operate. Little things, like having a dedicated system hotkey
(separate from ctrl and alt, which are owned by whatever application
is in focus), or like having conventions for file naming schemes, and
the way applications get installed and how their sources get stored
for later. I had come up with a very specific and natural way for me
to use my computer, and I'd made Linux conform to that, and for some
silly reason I was convinced I was basing it all on my old OS when in
fact I was basing it on all the *fixes* to my old OS that I had so
badly wanted to make to it that I jumped ship and switched to Linux!

And that's what Linux should be for you: it should be your old OS,
fixed.


[EOF]

Made on Free Software.


