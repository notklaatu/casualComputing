# Non Geeks and Linux




An interesting phenomenon happens after you've been a Linux user for a
long while: people start referring to you as a "geek" or a "computer
nerd". When it started happening to me, I was a little surprised. Was
I a geek? I had never thought of myself as a geek. But I must be,
since people were telling me I was. And once I started using Linux, I
admittedly had started thinking a lot more about computers and how
they work. And yet, it seemed a little trite.

On the one hand, it seems that being called a geek or a nerd is a
compliment. After all, nerds are smart, yes? People are looking at
you, sat at your computer, and they recognise that you have some
skill, and they acknowledge you for your talent. That can't be a bad
thing. So why doesn't it feel like a compliment? Is it because maybe
they're actually looking at you doing *some thing* they don't
understand, so they call you a geek just for the lack of a better
term? That's not so much a compliment as it is a dismissal, or at
least it might feel that way sometimes.

Maybe it's the same mentality as any other label, like telling a
friend about that great new movie you saw, or a really interesting
book you're reading. They're not sure they want to invest too much in
what you're saying, so they look it up online and they say "oh, it's a
sci fi flick" or "oh it's a fantasy book" and they react
accordingly. You can try to tell them that they don't get it; it's so
much more than "just" another sci fi movie, or just another Lord of
the Rings rip-off, but they won't listen, because they looked it up,
they saw the label, and now they know. They get it, or at least they
think they do.

Or maybe it's also a little like being really excited about a new
recipe, and someone saying that while they aren't interested in
hearing about it or trying it if you cook it, they certainly see that
you spend time in a kitchen.

Or hey, maybe it is what it is: a diverse term that can mean a lot of
different things depending on context and who's using it. Maybe you
find it offensive, maybe you find it complimentary, maybe you find
that it brings people of the same interests closer to you and drives
those without those interests away. So we get called "geeks" and
"nerds", and we own the term, and "geek culture" pervades, and
changes, and adapts. Pretty cool, huh?

The real danger (such as it is) is that "geek" can become a word for
"magician" or "wizard". People look at geeks and assume that we
(geeks) were just sorta born this way. I realise that some people do
seem to have an aptitude for, say, mathematics or for complex logical
thought, but not all geeks are innately "geeky". By that, I mean a
geek doesn't just sit down at a new OS and automatically know how to
re-program it. Seriously, watch the most mystical Linux geek you've
ever met try Plan 9 for the first time. It's not impressive.


## West Across the Ocean Sea

When I first learnt of open source technology, I was an art student. I
didn't own a laptop. I'd never taken any kind of computer class. I
knew what I knew of computers from growing up with them, same as
everyone else, and from the times that I broke them and had to make
them work again because getting them to work again was more important
to me than walking away and finding something else to do.

Being a visual arts student, I was dealing with a lot of video and
large graphics. Since I was picking up odd jobs in my attempt to make
a living from visual art, I tended to encounter *innumerable* video
file formats. It seemed like that would be common, but the OS I was
using at the time had been (and still is) designed to only work with a
small subset of video formats, with options to pay some extra money to
"unlock" more.

That literally wasn't an option for me (because I was scraping by,
financially), and besides it just felt wrong. Surely, this wasn't what
technology had promised; we can generate digital videos, so surely we
must also be able to play them on a computer. We shouldn't have to pay
extra for that ability. And that's what led me to a project called
[FFmpeg](http://ffmpeg.org). The first implementation of it that I
used was a horrifically bad GUI frontend which, even with its flaws,
managed to re-encode anything I ever needed it to translate for me. It
was my secret weapon and secured me several high-paying jobs as a
freelance "video guru" (for lack of a better term).

I can't overstate the impact that FFmpeg had on my life. I'm not
talking emotionally, I'm talking about how it delivered what
technology had always promised to be.

As a result of finding this strangely powerful and yet totally free
FFmpeg, I discovered that Unix existed and was, in fact, now the basis
for the OS sold by the computer vendor I'd been using my entire
life. I remembered that my father had mentioned Unix as sort of the
original computer OS, and that it was something that big companies and
the government used.

It was all coming together. I started poking around at the Unix
underbelly of my OS.

I read a book about Unix. It was an early edition of the [Visual
Quickstart Guide to
UNIX](http://www.peachpit.com/store/unix-and-linux-visual-quickstart-guide-9780321636782),
for the record. I learned what Unix was, how to use it, lots of
commands, and found out about a public Unix server with $0
membership. I joined [SDF](http://sdf.org) and have been a member ever since (although I
did change usernames about a year in).

As a result of this, people did start telling me that I was a geek. At
first, I declined the label, because back in school it had been a
badge of shame. Cool kids were not nerds. So I didn't want to be a
nerd. But you can only hook up someone's printer so many times before
you realise that however trivial it may be, you really do know more
about computers than the average bloke.

I eventually faced facts: I had a modest skill that I could market. I
could do simple, everyday tasks on people's computers and get money in
return. It's like getting paid for eating lunch, or for taking out the
trash on Friday, or whatever. Really simple things; an afternoon of
work, in exchange for cash. Yes, this was good.

I printed business cards and posted them on the billboard at school,
on subway walls, on community boards, and took jobs fixing people's
computers. I eventually managed to obtain two independently broken
laptops, which I combined into one working laptop. With this, I
"modded" my OS so that it booted straight into X11 and the
Enlightenment (e16) desktop. I learnt how to find source code, compile
it into an application, and install it for everyday use. I started
using cool new (very, very old) applications that ran only in a
terminal window.

I still didn't exactly know that Linux existed yet. This was all just
me messing around in terminals on a proprietary OS that happened to
have Unix at its core. At some point, though, I discovered a funny
command that would let me play Tetris in the terminal. It was actually
in [Emacs](http://gnu.org/software/emacs), but I didn't understand
what that meant, at the time, and the OS didn't ship with the Emacs
GUI. This, in turn, revealed a license agreement buried deep within
the computer. It was the [GPL](https://www.gnu.org/licenses/gpl). I
sat down and read it like a book; I was entranced. It spoke about an
uncanny philosophy based on sharing information and knowledge,
ensuring that users owned the *whole stack* that created the data they
generate. It was less a life-changing event as it was a realisation of
the feelings I'd been having anyway, and about a lot more than just
software.

I remember reading more than just the GPL; I think it must have been
some kind of annotated thing, or maybe it was just a README, but
through these "hidden" documents that shipped with Emacs by
requirement, I learnt that there was an entire operating system that
was itself open source. I switched over to it as fast as I
could. Sure, I did probably half a year of research (it felt more like
a week, at the time):

* I read books about Linux and how it can be used as a desktop replacing the OS that shipped with one's computer
* I read Linux "fan" magazines
* I read Linux and Sys Admin (whatever that was) technical journals
* I listened to Linux tutorial podcasts

I was getting poorer by the day at this point, being mostly an artist
barely getting work, so I was reading all of these things for free in
the bookstore. Eventually, it got to be too much for me to bear, and I
finally borrowed someone's laptop, got a book with a Mandriva DVD in
the back cover, and tried booting into this magical thing called
Linux. I wasn't really expecting anything; I think I almost expected
it to not work at all. But to my surprise, it booted. It booted to a
desktop; I remember a blue wallpaper with a star logo on it. I
remember moving the mouse around, seeing that it worked, and it was
like a whole new world had suddenly been open to me.


## Melodramatic Aside

As a side note, I do wish, in some way, that everyone could experience
the same kind of technological love story in their own lives. I look
back at those memories, and it's entirely in soft focus with amber
gels, warm sunlight, the whole nine yards. It was perfect. Today, it seems
people get all the way to the end of my tale, sometimes, in one
afternoon (or at least, they think they do). The internet says
"Linux!" and you click the link, you download the ISO, you look at it
in a virtual machine, and you're done. There's no sense of discovery,
and when there is, it's mired by annoying comments online about which
Linux is best or why Linux is horrible or whatever the issue is that
week.

Anyway, that's a pretty good ending to the story; I investigated, I studied, I
booted. Bright white light, all is revealed, roll credits.

But, you see, the story doesn't end there. That's actually the *start*
of my story.


## 12 Bar Blues

The story itself isn't as pretty as the backstory. Nobody wants to
hear the real story, the part where after I have learnt that Linux
exists, and once I start actually using it, I realise that while I had
built up several skills fixing computers and learning hardcore Unix
commands, actually getting used to a whole different style of
computing was an entirely different matter.

You know how when you live in America, you think it'd be really nice
to visit, say, New Zealand? So one day, you take a holiday (I mean
"vacation") in New Zealand and it's great. It has little oddities, but
they're quaint and fun. Then you go back to America and resume
life. Easy.

Then one day, you get a job in New Zealand. You move there to
live. And that's when it hits you: it's different there. All the
things you hated about life in American towns are now the things you
wistfully, however involuntarily, dream about. Those little quirks you
noticed on vacation are everyday annoyances now. It's *hard* to adapt.

That's what switching your OS is like. It was easy at first, and
fun. But then it becomes real life. Everyday life. And sometimes
that's not fun. Anything different is bad, and anything similar is
never similar enough.


## Geek With No Name

So you see, dear reader, the fact that I get called "geek" now is not
because I sat in a bookstore reading about the joys of the Linux
desktop and the nuances of Unix shell commands.

The fact that I get called "geek" is because I re-learnt the very
basics of computers. And I do mean the basics.

For instance, for my *entire* computing life, the way to eject a disk
was to drag it to the trash can icon. Did this make any logical sense?
no, but that's how it was done and I'd never thought to even question
it, much less think that there might be another way for it to
happen. Turns out, there are several other ways for this to happen,
and it was on Linux that I got to learn that. Could I have learnt it
elsewhere? sure, but on Linux I didn't just learn about physical
buttons on optical drives and right-click menus, I learnt about the
underlying code that made it happen.

For my entire computing life, the menu bar was at the top of the
screen, and it (get this!) *changed depending on context*. Bad
UI/UX/Whatever design?  "Horrific" is a better word, but I was
convinced that it was not only the best but also the *correct*
implementation of the concept. It took me years to adapt to per-window
menu bars, and years upon those years to accept that I had been
wrong. Only recently did I have the opportunity to try my old OS
again, and believe me when I tell you that I was the most surprised
person in the room at seeing myself struggle with the
inefficiency. (For the record, if a menu bar is non-contextual, it
only requires one-click to activate it, whereas a contextual menu bar
always requires two unless it's already in focus. To say nothing of
the constant roundtrips back up to the top of the screen of the
left-most monitor *every time you need a menu*.)

For my entire computing life, installing an application was done by
dragging an icon into a special folder, or by running a little install
assistant. It didn't take me long to adapt to Linux's method, because
it was just so darned easy, but I did miss that "special" applications
folder where "all the best" stuff was kept (except when it wasn't, but
you learn to live with quirks like that when it's all you know).

Believe me, there were times when the learning curve seemed like it
would be too great. There were times when I hated Linux, and software
developers, and open source. Heck, there are still times I hate
software and computers; who doesn't? We all find a missing feature or
a feature that's sloppy, and we bang our fist on the desk and think
"what are those developers even doing all day, if not giving me that
ONE feature that I suddenly want?". It's human nature, and it happens
with everything we ever touch, whether we feel like we've been
suckered into paying for something that isn't good enough, or like we
would pay anything to make it better.

But I got through that, because I wanted to learn new stuff, and I
didn't want to rely on magical computer devices and mysterious
benevolent programmers who sometimes, if I prayed hard enough on a
shooting star, blessed me with a new feature or a vital bug fix. I
wanted to understand what I was dealing with, and at the very least to
be able to identify what needed fixing, if not how to fix or work
around it.

And that's what a "geek" is. That's what a nerd does. They, er, "we",
*learn*. We look at a puzzle, and we insist that it gets solved.



## Knowledge is Power, Blah blah blah

Pretty much anybody can learn new stuff. Not everyone has the time for
it, or the interest in, learning all there is to know about Linux
(and, therefore, computers) and that's OK. But the word "geek" or
"nerd" is not the right way to express that. If you really want to
learn new stuff, then learn it. Don't be intimidated by someone in a
cool t-shirt with thick-rimmed glasses talking about things so fancy
you can't tell the nouns from the verbs. Don't go thinking that he or
she got to be that way through birthright.

"Well, it's easy for you; you're a geek" doesn't mean anything. That's
not how it works.

It was earned, and it wasn't easy, but it's attainable. You might not
become a super star programmer who gets interviewed on the evening
news about violence in video games or the latest new security flaw,
but you **can** get this stuff.

You can be a geek. You have only to learn.

[EOF]

Made on Free Software.

