# Expectation and Intention




I saw this on a forum post the other day:

    There is no fix. No linux distro will work right with
    my monitors. Only windows does it right. Ubuntu, Debian,
    Kubuntu, Mint, puppy, and several others I've tried.

The "problem" the user was talking about is that his computer is
seeing the "wrong" monitor as the primary screen, such that the
desktop menu bar and other desktop *things* are being placed on one
monitor instead of the other. The user asked about it some, declined
several suggested fixes, and then went on to rant about how no Linux
distribution he has tried is doing it "right".

The problem (the one on the surface) doesn't concern me; I happen to
have quite a bit of experience with graphic cards and multi-screen
setups, so I know for a fact that there is definitely a way to
configure a machine to use one monitor or another as the primary
screen. The problem obviously and clearly has a solution.

No, what interested me in this post was what it reveals about how
people perceive computers.


## Definition of "Right"

We all do this at first: what a computer "should" do is defined
entirely by what the computer we used yesterday did. This runs so
deep, it's hard to fully grasp how pervasive it is, but I think I,
personally, have a unique perspective on it. I grew up on a computing
platform that did not have the majority of the market, so my critiques
of Linux were always different from everyone else's critiques of
Linux. Inevitably, whenever anyone I knew had an idea about how Linux
could be "made better", the moment they voiced their idea, I'd shake
my head, "No, no, you have it all wrong, you're trying to make it more
like Windows. Here's what it SHOULD do..."

Of course, my brilliant idea would 90% of the time make Linux more
like the platform I'd migrated from.

This is a tip-off: pre-conceptions of how a computer is supposed to
achieve a specific task is on *us*, and it's not universal. Not by any
means.

It really can't be; for every one user, the definition of the "right" way of doing something is different, even when those users are on the same OS. This holds true for the simplest concepts, like how you launch an application, how you open a file, how you remove a file, where you keep your files, and on and on. We all compute differently, and there's no OS on this planet that can suit everyone's individual styles. There are two paths toward a solution:

1. an OS stays flexible and customisable so everyone can use it the
way they prefer

2. users conform to the one or two ways an OS provides for performing
a task

You can probably guess my preference (if not: it's the first option. I
prefer the first option, where the user stays in control).

I can hear you saying "yes, but there's such a thing as a de facto
standard", and you're right. If 30 years of GUI computing has trained
the world to expect one thing, then it seems counter-productive to
throw it out. But I think you'll find that not everything has a 30 year
tradition behind it. Most of us get used to something over the course
of a year, and then when it changes the next year, we pitch our
computer off a balcony. That's over-reacting.

Computers hold lots of programs. Each program has the option of
doing any number of tasks in any number of ways. We as users need to
remember a few things: there is no "right" or "wrong", and humans are
great at adapting.


## Hardware Knowledge

The post also fascinated me because is shows a severe deficiency in
understanding software and hardware, paired with a resolute conviction
that these same things are not working "right".

This isn't new to me. When I was working as a computer salesman to pay
for school, I encountered this daily and generally had to simply
endure it. Now I see it and usually walk away from it silently,
although I generally make the offer to educate just in case someone
wants to learn. Sadly, when faced with a situation that they don't
understand, most users seem to prefer to blame the programmer, or bad
mojo, or themselves, or whatever arbitrary thing they can use as an
excuse to run away from the problem.

I find it difficult to identify with this mentality, because it's just
not how I react to puzzles. If my car won't start in the morning, I
don't go to a garage and tell them that my car will not start and that
there's no possible solution, and that model of car is basically
flawed anyway; I'd go to the garage, describe the problem, and look
for a solution. Since I don't know a whole lot about cars, I'd let the
mechanic explain to me the potential problems and how they might be
diagnosed and repaired.

But for same reason, when people talk tech, there's a conviction that
they know the issue, and often that the issue cannot be fixed.

In the post above, the "problem" is that the software is working
correctly; it is identifying the primary display as assigned by the
GPU. Since this particular user wants the opposite of what the
hardware provides by default, the user is supposed to intervene and
declare a preference. For whatever reason, this user is declining to
do that.

For whatever reason, Windows chooses to do something different. I
don't know what it's doing, but maybe it's hard-coded to prefer DVI
over HDMI, or maybe it detects and holds the first display plugged in
as primary; who knows? It's probably a perfectly valid choice, and for
some people, it works well. The problem is, some people obviously
believe that it's the only and best way of detecting displays,
forgetting that there are millions of other users out there, many with
different expectations. All of this somehow gets translated to an
assumption that there is a problem with the way the software and
hardware are communicating. To anyone with half a programmer's brain,
it's so obvious that there's not a problem, just a *difference*. Heck,
to a programmer well versed in drivers, there's possibly even a
low-lying "right" answer based on how the GPU declares itself to the OS.

It's no mystery as to where a user gets their absolute "knowledge"
about these kinds of things: it's from whatever their previous
experience prepared them for.

This cuts both ways. Just because Linux might auto-detect the "right"
monitor for someone doesn't necessarily mean it's doing anything
better than Windows. It just means that you happen to like the way it
is defining the primary display.

The solution is to understand how the selection is being made, and to
do whatever you need to do to get it to align with your
expectation. Either that, or stop complaining and accept the fact that
there are details that exist over your head, and that you do not have
the time or interest in understanding. That's not a bad thing, but
complaining loudly whilst acting like you do understand, isn't
helpful to anyone.


## Custom Orders

No OS vendor takes custom orders from every single user. Configuration
is going to happen somewhere along the line. You can pick and choose
the things you firmly believe should just be a certain way, but that
doesn't mean anyone agrees with you.

A good OS has ways for you to set
your preferences and then *keep them* across upgrades and re-installs;
a bad OS sets those preferences for you, and forces you to settle for
what they give you.


[EOF]


Made on Free Software.


