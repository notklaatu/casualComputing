# Python Pip




It seems like everybody's got a package manager these days, and
Python's no exception. In a nutshell: `pip` provides an easy,
Python-native, platform independent method to install Python modules.

On one hand, that seems like a collision of domain; after all, your
Linux distribution probably has a package manager, so if you also have
a Python package manager, then how do you mitigate one with the other?
What happens if you `pip install` pyFoo on Monday and then your
package manager stubbornly tries to pull in its own copy of pyFoo on
Tuesday as part of some other package?

Well, I run Slackware, so my OS's package manager never collides with
anything unless I personally do it myself. But on, for instance,
Mageia or CentOS, it could be a problem; in that case, it would be up
to me to either defer to `urpm` or `dnf` (since they're more automated
than `pip`), or use `pip` but manage my own `sys.path` carefully and
just ignore the fact that I actually may have two versions of the same
module installed.

You know your own system better than I do, so you decide what you want to do. 

However you manage it, `pip` offers an easy way to download and
install Python modules, and since it's the community portal for
Python, you're likely to get the most updated versions promptly (even
before your distribution), and it's all neat and tidy within Python so
you never have to get out of the Python mindset.

And better yet, `pip` can run at the user level, so if you haven't got admin privileges for the computer that you use, you can still use `pip` to download and install Python modules to your home directory only.


## Install Pip

It should come as no surprise, but the official [pip
docs](https://pip.pypa.io/en/stable/installing) provide the install
procedure for `pip`. At this point, it's most likely that `pip` is
already on your system, so probably a simple update will do:

    $ su -c 'pip install -U pip'

or if you're an unprivileged user:

    $ pip install -u --user pip

If `pip` is not installed at all, then you should download the
[install
script](https://pip.pypa.io/en/stable/installing/#upgrading-pip) and
run it (use the `--user` flag for a local install).

    $ python get-pip.py [--user]


## Use Pip

I tend to think of `pip` as the `yum` of Python. I say it's like `yum`
rather than some other package manager because it's got the same kind
of indifference about where a package is located when you ask it to
install something. It's happy to download a package from the internet
and then install it, or it can take a package that you feed it from
your home directory and install that. It doesn't care, so you have
some flexibility yourself in how you acquire the things you want to
install.

The centralised online repository of Python packages is
[PyPI](https://pypi.python.org/pypi), the "Python Package Index". You
can go to the [pypi.python.org](https://pypi.python.org/pypi), browse
what's available, download packages, and install at your leisure.

Similar to packages in a Linux repository, packages from PyPI may come
in two different formats: a source code archive or as a pre-compiled
bundle called a "wheel" (in a `.whl` container).

To install something from PyPI, either reference a package by name:

    $ pip install --user foo

Or point it at a package on your system:

    $ pip install --user ~/Downloads/foo-0.6-py2-none-any.whl


## Pip Locations

Pip can run as a system tool or as a local user-based tool. If you run
it with root privileges, it gets installed and installs packages to
standard system locations, such as `/usr/bin` or `/usr/local/bin` or
however your distribution provides it. If you run it with the `--user`
flag, then `pip` gets installed to `$HOME/.local/bin` and your Python
modules go to `$HOME/.local/lib`.

There are versions of `pip` for both a Python 2.x and 3.x, so if you
want to install a Python3 modules, you probably need to run
`pip3`. Python 2.x uses `pip`. This is true unless you have installed everything manually and have changed things around; in that case, obviously, you know your system best.

That's it. Simple, effective, and easy.

[EOF]

Made on Free Software.


