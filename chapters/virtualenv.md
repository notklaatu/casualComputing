# Virtualenv




If you do lots of work in Python, you'll eventually hear about
`virtualenv`, the Python system to create and maintain virtual
environments so that your projects stay superficially separated from
one another. Why bother? well, there are several reasons. Two good
ones are:

* it helps you avoid blissfully developing an application on top of a stack and then forgetting to tell users what's in the stack because "the stack" is just *whatever I had on my computer at the time...*

* you want to use pyfoo-1.0 in one project and pyfoo-2.0 in another

Is it worth the trouble? Frankly, if you're not sitting around
thinking "oh my gosh, I need to learn virtualenv!" then you may not
need to learn virtualenv. It's a handy tool, but it's definitely an
intermediate-to-advanced level tool, so don't sweat it if you have to
put off learning it until later.

There are several tutorials on how to set up and use `virtualenv`
online, but most of them are confusing or introduce all kinds of
frontends and wrappers. I prefer to learn and teach the baseline tools
*first*, and then explore layers of abstraction later. So here are the
basics of getting `virtualenv` installed, followed by a quickstart on
how to use it.

## Install Pip

The `pip` system is sort of the `apt` or `ports` of Python. If you are
on unix (and in this article, I assume you are, so if you are not,
you'll have to translate a little), then Python probably comes
pre-installed and is managed by the OS. Pip can probably be installed
by your package manager, too, so a command like `apt install
python-pip`, or a visit to http://slackbuilds.org if you're on
Slackware, or whatever, will get you `pip`.

If you're on a unix system being managed by someone else (as I am, at
work), then you may not have access to system-level tools. The good
news is that you can install `pip` locally, to you user directory,
without admin privileges:

    $ wget https://bootstrap.pypa.io/get-pip.py
    $ python get-pip.py --user

Pretty easy.


## Install virtualenv

Now that you have `pip` installed, use it to install
`virtualenv`. Once again, you can do this as root or you can do it
just for your local home directory:

    $ echo "Systemwide:"
    Systemwide:
    $ sudo pip install virtualenv
    $ echo "Local install:"
    Local install:
    $ pip install --user virtualenv

Now `virtualenv` is installed.


## Using Virtualenv

There's nothing mysterious or magickal about `virtualenv`. You can
think of it as a kind of superficial `chroot`; if you don't know what
that means, then you just think of it as *you* lying to your
computer. When you launch Python normally, your computer asks
(silently, but it still asks) where it should go to find Python
modules; you tell it where modules are located. I can prove this to
you:

    $ python
    >>> import sys
    >>> sys.path
    ['', '/usr/lib/python3.4', '/usr/lib/python3.4/plat-x86_64-linux-gnu',
    '/usr/lib/python3.4/lib-dynload',
    '/usr/local/lib/python3.4/dist-packages',
    '/usr/lib/python3/dist-packages']
    
You see how Python just sorta inherently knows where to look for
modules.

Now let's use `virtualenv` to lie to Python about what our system
looks like.


First, create a virtual environment:

    $ virtualenv my_fake_env

If `virtualenv` cannot be found, then you either have not installed it
or you have installed it locally and have not added the install
location to your path. In that case, you can execute the command
directly:

    $ ~/.local/bin/virtualenv

But you should probably, for the future, add `$HOME/.local/bin` to your PATH.

Wait patiently for `pip` to install a reasonable base environment.


Once you get a prompt back, take a look and you'll see that your mini environment exists:

    $ ls | grep env
    my_fake_env
    $ ls -1 my_fake_env
    bin
    lib
    pip-selfcheck.json


Next, activate the environment. There are a few ways to do this, depending on what shell you're working in. For me, at work, I use `tcsh`, but at home I use `bash`. Your choices live in your newly-created environment:

    $ ls -1 my_fake_env/bin/
    activate
    activate.csh
    activate.fish
    (and so on...)

So if I'm running in BASH, it's simply:

    $ echo $SHELL
    /bin/bash
    $ source my_fake_env/bin/activate
    
If I'm at work on TCSH:

    $ echo $SHELL
    /bin/tcsh
    $ source my_fake_env/bin/activate.csh

And so on. Not rocket science.


One your environment has been activated, your shell prompt changes:

    [my_fake_env]$

Now you can work on your Python project as usual. Remember how I said `virtualenv` is basically just lying to your computer? Well, let's see the results of that:

    [my_fake_env]$ python
    >>> import sys
    >>> sys.path
    ['', '/home/klaatu/my_fake_env/lib/python3.4',
    '/home/klaatu/my_fake_env/lib/python3.4/plat-x86_64-linux-gnu',
    (and so on...)

You see that all paths within this virtual environment are based in the my_fake_env directory, almost as if the rest of the computer doesn't exist. This means that you can install pyfoo-1.0 in my_fake_env without affecting my_other_env. Pretty neat!

To get out of your environment, just use `deactivate`:

    [my_fake_env]$ deactivate
    $


## Fancy Setups

Are there more elaborate ways to use `virtualenv`? Yes, there are. There are ways you can tie it into larger development practises, you can use a wrapper, you can do all kinds of things with it.

That's not what this article is about.

You know how to get `virtualenv` and how to use it. So start using it, get used to it, fall in love with it, and then go out and investigate cool add-on ideas.

Happy hacking.

[EOF]

Made on Free Software.

