# Unix is not OS X




If there's one thing I can't stand, it's the trope that "Mac OS X is
just a fancy UNIX!"

It's annoyed me for a very long time. Initially, it just annoyed me as
a user, because, while strictly *true*, it's a misleading
statement. It suggests that if you are using OS X (Unix), then if you
want to later use Unix proper, you'll have a head start. After all,
you had been using Unix all that time, right?

As I have often said: no. I mean, yes, you've been running Unix. But
what you've been *using* is Cocoa.

Because that's what OS X really is to most people: it's Cocoa. Cocoa
doesn't need Unix; it could be ported to Windows, if Apple
Incorporated ever wanted to bother. It certainly isn't particularly
Unix "aware". Sure, if you move a file in the Unix shell, the file
also moves in your graphical desktop view, but that's not because
there's any connection between that shell and your graphical
environment, it's because both of those two things are both looking at
the same file system on the same hard drive.

Don't get me wrong. I understand that there is, inarguably, UNIX
happening in the standard-issue (what am I saying? the *only*-issue)
OS X machine. And we unix-geeks should take pride in that; the design
schema we "champion" (in whatever way we champion it) is working! it's
working well enough to drive a billion dollar company.

But imagine looking at a zebra and calling it a human because it's got
bones and DNA and stuff. OS X is so thoroughly determined to NOT act
like Unix that calling it Unix is almost antagonistic.

But to this day, people cite stuff like fink, macports, and
homebrew. Homebrew even calls itself "os x's missing package
manager". First of all, why is it missing? Second of all, OS X doesn't
let it (them) work like a package manager on any other unix
distribution does. I installed a Python library the other day through
homebrew and spent an hour trying to import the thing before stumbling
across the widely accepted "answer": install Python from homebrew and
use *it* instead.

No.

No no no.

That's not how it's supposed to work. I don't mean package managers,
now, I'm talking about *computers*.

Yes yes, `sys.path` and all that. I don't take issue with how
programs find libraries, I take issue with the persistent,
against-all-evidence insistence that OS X *is* Unix. OS X *uses* Unix
tools for low-level access to some components of the system, it uses
Unix to manage its file system (I don't mean HFS+ specifically, I mean
the file structure of its system), but OS X as a packaged product is
not Unix. OS X is Cocoa, and to develop for it you *will* use its
API set.

## So What's the Point?

My point is that using the Unix part of OS X is a little like creating
a chroot.

No, sorry. Actually it's a *lot* like creating a chroot.

If I wanted my Unix environment broken out into its own separate
system, I'd just dig out a computer from the rubbish bin and run a
proper unix on it. I don't see the point in having "it's unix!" on OS
X if it's functionally the same as Cygwin on Windows.

"It's Unix!"

Big deal.  By these standards, so is my toaster.

[EOF]

Made on free software.

