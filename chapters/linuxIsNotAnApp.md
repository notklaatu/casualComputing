# Linux is not an App




Linux is a kernel, and I don't say that lightly, because it
matters. But in the vulgate it's an "operating system", too, so that's
the term I'll use here.

For a very long time, Linux has been either discounted or ignored by
vendors of other operating systems; you can attribute that to
confidence or fear, as you please. Lately, there have been some shifts
in how competitors view "Linux" (both the OS and the esoteric "force"
that it represents), and one tactic that I perceive is the peculiar
and subtle suggestion that Linux is, essentially, *just an app*.

Usually I would interject the aside "not literally", but in this case
I do feel that it's the **literal** goal of some companies to create
the impression upon people that Linux really is Just An App. It's as
if to say, "It may seem strange that we're raving about Linux in our
marketing materials now in spite of all those mean things we said
about it in the early 2000s, but the reason is this: Linux isn't a
reliable OS and should not be used instead of *our* product. Linux is
just an app, and it's OK to use it here and there, as long as you're
running it from within our product."


## Linux as a Software Appliance

Linux was already pretty "unavoidable" in the server space;
realistically, you weren't going to work around large server
deployments and not encounter UNIX (often in the form of Linux,
specifically). Microsoft and Apple both knew that, and they each
carved a niche for themselves in home and small business server
spaces.

When Microsoft decided to change its tactics from all-out war against
Linux to sudden-colleague, they positioned it as a kind auto-correct
mistake on all past statements. They didn't go so far as to
acknowledge the monopolistic business tactics and outright corporate
sabotage, but they very suddenly took a new stance: they love (not
"like", but *love*) open source. Microsoft decided, overnight, that 
they could magically transform from competitor to colleague.

How did this change in heart and stature manifest itself? by making
Microsoft more compatible with Linux than ever before. You can now run
Linux on a your Microsoft "cloud", and you can run "Linux" applications
via a compatibility stack on Windows. Can you do the same with
"Windows" applications? well, yes, technically that's been possible
for years upon years, but only because independent hackers have
sufficiently reverse-engineered how they work. Is Microsoft now going
to help with that effort? no, of course not.

To make sure it's straight, this is the state of play:

* Microsoft has enabled you to run open source Linux-centric software on Windows
* Microsoft has enabled you to run open source Linux instances on Azure
* Microsoft has not provided any support to help you run Windows-centric software on Linux
* Microsoft has provided support to help you emulate the Windows OS on Linux

If this seems like a one-way street, that's because it is. Microsoft
is supporting any effort that results in the sale of a licensed copy
of Windows or Azure, whether it's running on bare metal or in a
virtual environment; they don't care. If you're only in it for one or
two applications, they aren't interested in giving you
anything. Microsoft gains most of what Linux has (on a superficial,
but functional, level) without having to give up anything that they
didn't have before. It's a no-brainer, and a little surprising that
they didn't think of it earlier.

And in fact, what I'm really driving at is that Linux loses a little
something. Instead of being identified as an operating system, Linux
becomes, in this model, an application that you download and install
and run on top of Windows. Linux isn't an alternative operating
system; to a born and bred Windows user, this new exciting Linux thing
is just a quirky application, much like Visual Studio but with a built
in web server.

But who cares, right? Whether we call Linux an "operating system" or
whether we just treat it as an appliance you run as a `.exe`, the
result is the same. In fact, in many ways, the functional result is
*better* than what you'd have if you're stuck on Windows: you can
still use Linux even if you're not allowed by your sys admin to
install Linux on your hardware. You get all the benefits of Linux
without any of the "burden". Microsoft, one of the world's largest
monopolies, granting its blessing upon Linux should, in theory, help
"spread" Linux in some very real and practical ways.

Let's come back to this quandary later. First, let's look at another
way Linux has lately been relegated to application-level duties.


## Embedded Linux

Linux has long been a key tool in a hacker's, or hobbyist's, toolkit;
it's open from the ground up, allowing a user to learn everything
involved in making a computer run. It's no exaggeration to say that
just booting into Linux a few times can, if you think about what you
are doing and how it is happening, can improve your understanding of
computers. In fact, I don't even think it's exaggerating to say that
just *hearing* about Linux can improve someone's understanding of
computers; before I knew Linux existed, it literally hadn't even
crossed my mind that a computer could be purchased without an OS, and
that an OS from a third party could be used (I didn't understand, at
the time, that Microsoft itself was, at least legally, a third party).

Because it is so suited as a hobbyist's OS, when the "maker" fad hit
its full swing with the rise of ultra-cheap SOC boards, Linux was (and
is) the *de facto* OS in use. This came as no small surprise, clearly;
the Raspberry Pi (although the Pi foundation has made it clear that
they are no ideological friend to Linux) sells out consistently upon
each new board's release, copycat boards proliferate, and Microsoft
scrambled to mitigate the craze. They eventually did: they released a
Windows 10 "core" installer that turns the Pi into a sort of .Net
Arduino, a slave device to be programmed through a Windows master.

Before Microsoft got their code onto the Pi, though, there were a
number of tutorials from some large vendors (not Microsoft, that I
ever saw, but big places, like Autodesk in the guise of Make™) out
there that instructed users on how to use the Pi exclusively as a
slave device. It all but ignored that Linux was an operating system
that you could just use, that the Pi doesn't *need* a host because it
is its own host. The Pi and SOC boards like it are not
broken-by-design cell phones and mobile tablets, where you need to
connect them to a host computer to do anything useful with them,
including program for them. It's a flat-out dangerous concept: a
computer that cannot itself be programmed. That's fine for the
industries creating new chips and boards but for the end user, it's
the very idea of closed source, blackbox, applianceware, and it flies
in the face of what a "maker" (I'm using the nearly trademarked term
somewhat sarcastically here) "movement" (also somewhat sarcastic,
given the huge push from corporate monopolies drives this market)
should be all about. If you can't sit down at your "maker" workbench
and hack on the thing you're using, then just what kind of maker are
you, exactly?

In a sense, it's kind of nice that Windows "core" got released for the
Pi and the Minnowboard; at least in that model, you're acknowledging
that the platform is closed and dumb, rather than blissfully ignoring
the open source foundation that you are building upon.


## Linux and Independence

There are several issues here, and they all revolve around one
another, sometimes wandering off into pragmatism, other times flirting
with ideology, but all of them firmly rooted in tech.

It's no secret that I'm a Unix and Linux enthusiast, to the point that
I use nothing else. I'm open to something else, as long as it's open
source and meets my requirements. But at the time of this writing, the
only thing that consistently meets my computing requirements is Linux
and, to a lesser degree (in terms of multimedia flexibility), BSD. As
such, I have an interest in promoting unix and open source above all
else, so on one hand I have a "bias", although on the other hand, my
"bias" is sincere; I am interested in the large scale success of open
source because it works best for me, it enabled me to get into
technology even though I had no financial business engaging with such
an industry, and it likewise enables others to be successful.

And that's actually my first problem with this situation: technology
is meant to improve quality of life. If that's not what technology
does, then it's no longer useful to us and, in fact, potentially
harmful. As such, technology should be accessible to us all,
regardless of anything like class or status. I'm not saying everyone
needs to know how it all works, but the ability to learn ought to be
there, with no barriers. Same as anything else, like the ability to
grow food for one's family, or to build a shelter, purify water,
breath clean air. If we're saying that something improves quality of
life, it must then be as "natural" as everything else that sustains
life. Otherwise, it's not improving life, it's yet another achievement
in life that you must work toward: a luxury.

Microsoft and Apple could be called luxury items. I don't think they
are very "luxurious" in the sense that I think an extended weekend in
the Alps is luxurious, but we might call them *bourgeois*. They're
restricted. And because they're restricted, they discourage learning
and understanding. You can't ever really understand or know Windows or
OS X, because there are major components that are kept out of reach
from you. And if de-compile and reverse engineer, then there are legal
consequences to remind you that too much knowledge is just not
allowed.

I guess more than the literal closed-off source code, it's the very
superficial *legal* limits that speaks volumes. There's no reason you
can't understand computers, it's purely a man-made blockade between you
and knowledge that others, employed by Microsoft or Apple, obviously
do have access to. I mean, we're not debating why I can't "see" a
molecule; I can't see a molecule because I don't own a microscope with
sufficient power to show me that level of detail. Code is different;
it inherently has no barrier, at least not to see. I'm not proposing
that Microsoft or Apple distributes a free CPU along with the code
they write, but I am saying that if we're seeking to further the
collective knowledge of humanity, then we should share what can be
shared freely. And if we don't, then can we really claim that we're in
technology to further our quality of life, or are we actually just
luxury items?

Don't get me wrong; if you really want to be nothing but a luxury item
for the bourgeois, that's fine; it's a free world, you can be as
selfish as you want. But I prefer honesty. I'd rather Microsoft and
Apple advertise their true intentions, rather than claim in marketing
material that they're here to empower and elevate and educate.

What this boils down to is that Linux is an alternative to frivolous
technology. It's a technology that can be both (you can, after all, run
closed source software on Linux) but in the context of empowering
users, Linux and open source has the market cornered. But, as has been
said, the medium is the message, and to present Linux as an add-on to
Microsoft threatens to obliterate one of the main points of Linux:
independence.

With Linux, all you need *is* Linux. It's an independent,
free-standing, complete solution. You don't need a license, you don't
need permission to use it, you don't even need to purchase anything
(if you're good at dumpster diving) aside from electricity. You don't
need to opt into a special Microsoft club to use Linux.


## Linux is Efficient

From a purely technological point of view, treating Linux as an
application is grossly inefficient. There's a pretty simple, and very
apropos, analogy to be made: if you need a micro-controller to trigger
motors and read sensors, then the [open source] Arduino is a great
choice, but if you need a micro-controller and a web server, then
probably a Linux-based SOC is more sensible.

Same holds true for Windows users. If you've just bought a Raspberry
Pi, it makes no sense to interface with that Pi through another
OS. Because the Pi has an OS on it already. If you meant to buy a
micro-controller slave, then trade the Pi in for an Arduino, or (this
is less than ideal in terms of staying open, of course, but if you
don't care...) just install Windows "core" on the Pi and let it be a
slave. Don't add an entire OS to a stack that already has an OS.

Furthermore, a Pi *should* have an OS (so don't install Windows "core"
after all). It's a complete computer that can do some of the jobs of a
micro-controller, but it's not a micro-controller. It has an OS, it's
designed to be driven by an OS, and by the sheer grace of God, no
entity has stupidly intervened to break the Pi's OS such that it no
longer functions as a self-standing solution. If you buy a Pi, you
*have purchased a computer*.

Now, understand, if you've bought a cell phone, you have *also*
purchased a computer, but unfortunately the vendor has broken your
computer such that it can only be used as a slave device; it's sort of
a moderately intelligent thumbdrive or flatbed scanner without the
flatbed. You have to install "dev kits" to program for a
mobile. You're obligated to plug it into some other device in order to
modify how it operates (to the extent that you're even permitted to do
so). It's an abomination of technology.

Don't turn your SOC into that.

I'm not speaking as a hobbyist, here, but in the interest of consumer
rights. If you accept that a device that is fully capable of doing 100
things can be sold with 90 of those things arbitrarily cordoned off
from you, then you're helping create a false economy driven by nothing
but the checkbooks of the corporations producing the products. You
don't want to be a consumer where value is determined by the people
selling the product.

Linux is true to itself both technologically and economically. Value
is derived from user requirements. There's room for artificial economy
and bias just as there's room for closed source on top of the open
foundation that Linux lays down, but one should come before the other.


## Linux is Open

You can run closed source applications on Linux, and now you can run
Linux on closed source, but don't be deceived: Linux is not an
application. It's not just an open source application for a closed
source environment. Linux is an open environment from the ground up.

The difference seems subtle, especially if you're just a code (or
office, or data entry, or whatever) monkey stuck on Windows at
work. In that case, Linux on top of closed source is indeed a
blessing, because you can work in Linux even within the walls of OS
restrictions. Seems great, as long as you ignore the fact that you
have had to acquire "permission" to do something as simple as use open
source, but it's a day job and it pays the bills. I get it.

I get it, at least to the point of necessary evil. But in real life,
we can't relegate Linux to an `exe`. Linux is open, and if you hit the
ground and find that you can't dig any deeper, then you're not running
on open source, and that's not something we should settle for in an
advanced, knowledge-driven society. The obvious problem is that we're
not really an advanced knowledge-driven society, but if we agree for a
moment that we're aiming for that, then I think we can agree that open
source is the solution that enables education with no artificial
barrier. Why do we want to place an artificial barrier around this?

Well, we don't. If you can use Linux as a free-standing open source
system, that's the ideal, and it's the message we want to send to
people curious about technology. It's independent, and it's completely
open.

Keep it that way by using it that way.


[EOF]

Made on Free Software.

