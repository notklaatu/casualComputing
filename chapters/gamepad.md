# Gamepad on Linux

![](images/gamepad.png)

Before [Humble Bundle](http://humblebundle.com) and
[Valve](http://steampowered.com) brought "serious"
[gaming](http://gog.com) to Linux, my gaming experience was limited to
basically *nothing* for the bulk of my life, followed by two years of
finally buckling and getting a Sony Playstation because my girlfriend
at the time wanted me to introduce me to video games. Consequently, my
knowledge of gamepads was pretty narrow. I knew that the Playstation
and the Xbox had different kinds of controllers but I figured they
were just like mice or keyboards; USB input devices that were probably
basically universal.

I was incorrect.

I still have limited experience with gamepads, so this document is
only my experience in getting three gamepads working on my Linux
machine.


## Playstation vs. Xbox

If your frame of reference for game controllers are the two big console
vendors, then in your world (like mine) there are two "kinds" of
controllers: those distributed with Playstation or the third party
controllers sold for the Playstation, and those that ship with or
mimic the Xbox controllers. They're basically the same, except in the
ways that they are not, and those are usually the things that annoy
you. More on that part in a moment.

The *actual* difference between the two are the drivers. You see, game
controllers are *not* just mice or keyboards, they're finely-tuned
hardware that allows the gamer to interface with their
gameworld. They're also pretty vendor-specific, so neither Playstation
nor Xbox really "want" to follow any kind of standard but their
own. What that means for you is that you are, as is so often the case
in closed-source, screwed. You can't just plug a Playstation
controller into a computer and have it work, or even an Xbox one; in
both cases, you need specific drivers for them to "just work".

On Linux, the easy no-brainer solution is to use an Xbox controller,
because between these two choices, it just so happens that the Xbox
controller, for whatever reason, just happens to have better
support. It has its own driver, in fact.

Bottom line: you may love your Playstation controller, but on a Linux
PC, just go with an Xbox-like controller. You'll get used to it, I
promise. Also, the market for third party controllers is actually
really good, so you'll be able to find a controller that uses the Xbox
driver but looks and feels more like a Playstation controller if you
look hard enough.

In fact, you don't even have to look that hard. The Logitech F310 is
a dead ringer for a PS3 controller, but talks Xbox drivers on the
backend. If you prefer the Xbox feel, there are plenty of those out
there, too.


## Xpad and Xboxdrv

So now that you've settled on an Xbox-compliant gamepad, let's talk
drivers.

I was especially impressed when I bought my first generic controller
on TradeMe, plugged it into my Slackware machine, and found that it
worked without further configuration.

I was again incorrect.

I mean, I was correct: it did work, but it's working via the `xpad`
driver. There's nothing "wrong" with the xpad driver, but most big
*computer* games were written for mouse and keyboard first, and Xbox
controller second. This means that if you're using the Xbox driver,
you can plug in your controller and the game will recognise the input
as Xbox signals, and respond accordingly.

Bottom line: you want to forego the xpad driver and use the
[xboxdrv](http://pingus.seul.org/~grumbel/xboxdrv) Xbox driver.


## Configuration

Honestly, most Linux distributions at this point will get your
Xbox-compliant gamepad working *for you*. It's a no-brainer. Install
the xbox driver, plug in your controller, and start playing your
games. If you're running SteamOS (more on that later), you don't even
have to install the driver.

I use Slackware, which doesn't ship with `xboxdrv`, so I had to install
and configure that more or less manually, but it's really really easy.

1. First, install the driver from its website.

1. Along with the driver, you should have downloaded (if not
installed) a bunch of example configuration files. Copy the default
configuration to your `/etc` directory as `xboxdrv.conf`. If anything
listed in my code sample is commented in the example config file,
uncomment those:

        # cat /etc/xboxdrv.conf
        [xboxdrv]
        mimic-xpad = true
        [xboxdrv-daemon]
        pid-file = /var/run/xboxdrv.pid
        [xboxdrv]
        ui-clear = true
        [ui-axismap]
        X1 = ABS_X
        Y1 = ABS_Y
        X2 = ABS_RX
        Y2 = ABS_RY
        LT = ABS_BRAKE
        RT = ABS_GAS
        DPAD_X = ABS_HAT0X
        DPAD_Y = ABS_HAT0Y
        [ui-buttonmap]
        start  = BTN_START
        guide  = BTN_MODE
        back   = BTN_SELECT
        A = BTN_A
        B = BTN_B
        X = BTN_X
        Y = BTN_Y
        LB = BTN_TL
        RB = BTN_TR
        TL = BTN_THUMBL
        TR = BTN_THUMBR
        # EOF #

1. Create a startup file so that `xboxdrv` runs automatically as a
 daemon in the background. This makes it so that when you plug your
 Xbox controller into your computer, it gets recognised and can
 actually be used by applications. Something simple:

        #!/bin/sh
        xboxdrv_start() {
        if [ -x /usr/bin/xboxdrv ]; then
            echo "Starting Xboxdrv in daemon mode." >> /tmp/xbox.log
             xboxdrv --daemon --config /etc/xboxdrv.conf \
            --mimic-xpad --detach-kernel-driver --silent &
        fi }

        xboxdrv_stop() {
        killall xboxdrv }

        xboxdrv_restart() {
        xboxdrv_stop
        sleep 4
        xboxdrv_start }

        case "$1" in
            'start')
        xboxdrv_start ;;
            'stop')
        xboxdrv_stop ;;
            'restart')
        xboxdrv_restart ;;
        *) # Default is "start"
        xboxdrv_start
        esac

    If you're using systemd, it's just a matter of launching the daemon at boot; systemd handles the rest. Of course, if you're using systemd then you're probably using a distro that has already configured all of this for you. Still, in the interest of completeness, something like this should do:

        [Unit]
        Description=Xbox Driver

        [Service]
        Type=idle
        ExecStart=xboxdrv --daemon --config /etc/xboxdrv.conf --mimic-xpad

        [Install]
        WantedBy=multi-user.target


1. Make the `rc` startup script executable so that it gets started upon boot.

        $ su -c 'chmod +x /etc/rc.d/rc.xboxdrv'

1. Remove the `xpad` driver:

        $ su -c 'modprobe -r xpad'

1. Optionally blacklist it so it never loads:

        $ su -c 'echo "blacklist xpad" > \
        /etc/modprobe.d/BLACKLIST-xpad.conf'

1. You could manually start the `xboxdrv` driver with `su -c
'/etc/rc.d/rc.xboxdrv start'` but you should probably just reboot to
make sure that it gets started automatically for you. After a reboot,
verify it with:

        $ lsmod | grep xbox

1. Plug in your controller and start playing games.


## Intercepting Signals

The weird thing about being a PC gamer using a console controller is
that computer games are generally not written for controllers. Real PC
gamers (I'm not a real PC gamer, so I'm excluding myself from this)
use the keyboard and mouse. Some games do ship with a controller
scheme, and it's getting more and more common as SteamOS permeates the
PC market. To find out for sure, just go into the **Options** menu of
your game and have a look at what controls are available.

In the event that a game does not provide controller support, you can
brute force controller support with some middleware that listens for
Xbox control signals and translate them into keyboard and mouse
events, which get forwarded on to the game.

There have been many applications of this sort throughout history, so
the names and availability of the app might change, but they all
basically do the same thing. A really nice current one is called
AntiMicro and it's available from
[github.com/AntiMicro](https://github.com/AntiMicro/antimicro/releases). It's
got a nice GUI, presets from other users, and it works quite well. For
something simpler, you might also try
[rejoystick](http://rejoystick.sourceforge.net) but I use and can
vouch for AntiMicro.


## Steam Controller

Xbox-compliant controllers are a breeze, and in terms of PC gaming,
it's what I started off with. About two years after Steam came to
Linux, Valve released a wireless Steam controller, and I highly
recommend having a go. It's a nice controller and is mostly
plug-and-play on Linux. I say "mostly" because obviously if you're a
hacker and modder the way I am, there's no telling what state your
computer's in, but realistically speaking, if you've got a machine (or
a partition) that's dedicated to gaming on Steam (presumably you'd be
running Debian or SteamOS proper in that case) then the controller
definitely is plug-and-play, and has several nice features (cloud
syncing control schemes, for instance) in addition to a really slick
hardware design.




[EOF]

Made on Free Software.

