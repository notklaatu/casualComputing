# Advice Against Using Windows




It's so popular that it's cliché to hate Microsoft. You either settle
for it, or you make money supporting it. But nobody likes it.

At least, that's how it seems.

Myself, I don't hate Microsoft any more or less than I hate any other
mega Corporation. I find it unfair that they are the de facto computer
system for certainly the bulk of personal computers in world, and they
are used by governments but impose restrictions on the people paying
for the software through taxes. It'll all scummy, murky water that
obviously is not right, and yet Microsoft is still around, so I guess
everyone is fine with it. Like I said: you settle for it.

I'm one of those unique few who grew up with no exposure to
Windows. My family never used Windows, I don't recall it being
something I used in school, I managed to avoid it at work (my early
jobs were in computer retail, but I was always "the Mac guy", and then
I worked in film, and then in Linux). Windows has simply never been
something I've had to deal with, for better or for worse, for my
entire life.

So aside from my disdain for its business practises (which, again, I
don't really see as being any different from any other major US
company), I actually don't have *that much* to say about Microsoft
Windows as an OS, because I don't have the deep personal wounds I have
from Apple.

With that being said, I do have issues with Windows beyond Microsoft
as a company. Some of them are the standard complaints about
sanctimonious closed source, but there's a gem or two that I have
myself discovered in my brief interactions with it whilst trying to
make sure an application is cross platform.

To be clear: this is a list of reasons *not* to use Windows. It's not
a list pretending to weigh benefits to liabilities. It's just the
liabilities. I realise that for every complaint about any OS or
technology, there's always a hack. You can work around stuff. We all
do it every day. The problem is, some things just aren't worth hacking
out a way to circumvent a block in the road.

Sometimes it's just better, for so many reasons, to just not take that
road in the first place.

Here are some reasons:


## Developer

*If you're developing on or for Windows, the good news is that a lot
of stuff supports Windows in a very native-feeling way. The bad news
is that Windows suffers from several questionable design decisions.*

### Program Files and Architecture

This is a little bit technical, but it says a lot, so if you're keen,
read on. Otherwise, maybe skip to the next one!

Let's very broadly say there are two types of run-able files on
computers: there are applications and there are libraries. That's not
accurate, but I'm keeping it simple.

Libraries, by design, are not launchable; you don't click a library
icon and get a nice pretty application window. Libraries are pieces of
a puzzle; they're useless on their own, but they fill in the gaps of
an application when needed. When is it needed? whenever we launch an
application that needs a specific library (and most need at least
one), then a library gets plugged into a library-shaped hole, the
application is made complete, and it runs.

When the library is not available, then the application fails to
launch, or it crashes at some point, usually with some kind of library
error (an `.so`-related error on Linux, a `dll`-related error on
Windows).

One caveat:

A 32bit (x32) library only fits into a 32bit application, and a 64bit
(x64) library only fits into a 64bit application.

Applications are simpler.

* An x32 app runs on an x32 *or* x64 system

* An x64 app runs on an x64 only

As long as its integers can fit into the available memory space of the
host OS and all the libraries it needs are present, an application is
healthy. It doesn't actually ever need to know if it's x32 or x64; it
just tries to launch when clicked.

And how does a computer know where to find the application's code when
we ask the computer to launch something?

Well, the location is stored in an environment variable called PATH
(or `%PATH%`, on Windows).

It makes sense to separate x32 libraries from x64 libraries. After
all, you might have a x64 version of the standard C library installed
on your computer, and have the need to also install the x32 version,
because you may want to run one application that requires `libc.x64`
and another that requires `libc.x32`. You don't want those two things
to conflict, so you might do something like make a separate directory
for all your x32 libraries, or maybe give all the x64 libs a unique
name, or whatever.

You would want to do this consistently, preferably in such a way that
it does not break backward compatibility (existing x32 apps should
still be able to find x32 libs without the users or developers doing
anything differently).

It would make no sense to differentiate between x32 and x64
applications. As I said, applications don't need to know what they
are; all they need to do is grab their libraries and launch. Each
application launched gets its own process ID, each one claims some
memory, and computing continues as usual.

And yet for some reason, Win64 has been designed to differentiate
between x32 and x64 *applications*.

Why would you need to do this? theories abound online, and most of
them are obviously misinformed and malformed; the ones that are
correct boil down to this: we guess Microsoft decided it would be nice
to be able to see how many x32 apps you have on your x64 system. And
it might well be nice, for statistical purposes...the problem is, it
breaks forward compatibility in order to preserve backward
compatibility....if that even makes sense.

On Win32, applications are installed to `Program Files`. Fair
enough. Backwards compatibility tells me that in the future, I'll look
for apps in `C:\Program Files`.

On Win64, applications are installed to `Program Files`. Seems logical
and consistent; as above, I know to look for apps in `C:\Program
Files`. Oh, *unless* the application is x32...in which case we'll just
default to `C:\Program Files (x86)`.

The result? An x32 app I install on Win32 goes into one execution path
(`Program Files`) but *the same* x32 app I install on Win64 goes to a
*different* execution path (`Program Files (x86)`).

Same action from the developer, same action from the user, totally
different results.

Doesn't seem like a huge deal until you look at it from a programming
perspective.

On Linux, if I install an application to some default location (and by
default, I mean *let the computer decide*), and then add that default
location blindly to my PATH (which the computer does for me, but let's
assume not), then when I call that app, I get an answer.

Doesn't matter what architecture I'm on; could be x32, could be x64,
could be a PowerPC, a Raspberry Pi, whatever. I can blindly throw my
installer at Linux and say "install this where ever you keep your
binaries, and then launch it". Works every time.

Would you know how to do that if you've never used Linux? No, of
course not, but you would spend an hour reading up on it, try it, and
it would work as expected, on each architecture you (or, more importantly, your users) try.

On Windows, though, it breaks.

Watch:

    Exec "C:\WinFF-1.5.4-Setup-3.exe"
    DetailPrint "Installing WinFF and ffmpeg..."
    ${EnvVarUpdate} $0 "PATH" "A" "HKLM" "%PROGRAMFILES%\WinFF\"

That works on Win32 because `%PROGRAMFILES%` resolves to `Program Files`.

But that SAME CODE *breaks* if it's performed on Win64 because the
`.exe` gets its path switched to `Program Files (x86)` but
`%PROGRAMFILES%` STILL resolves to `Program Files`. In other words,
Win64 changes one variable (the implied `INSTDIR` variable) without
changing the other. The end result is that on Win64, we install a
binary to a place and then add a *different place* to the PATH, even
though on Win32 the same action installs to a place and adds that
place to the PATH.

And for what? 

There's no reason to differentiate, at least on the filesystem level,
between x32 and x64 applications. Applications don't care if they're
32bit or 64bit. Applications care if *libraries* are one or the other,
but an application doesn't care if it or any other application is. I
can call an x32 app from the code of an x64 app, or an x64 app from an
x64 app, or an x32 app from an x32 app; there is NO combination of
application calls that is affected by bit-edness.

If Windows really really want to let the user differentiate between
the two, then surely a different icon emblem or a meta directory of
all x32 apps would be better. (Although, for the record, I still fail
to see the significance, personally.)

But don't break your own system environment variables and internal logic.

## User

*Windows isn't safe for users. People should stop using it.*


### Accomplishments

At the end of an evening of playing around with a computer, the last
thing I want is to look back with a sense of satisfaction over having
"beat" Windows at something. By this, I mean I don't want someone to
ask "how was your day?" and the best answer I can give is "well, I
managed to get Windows to stop doing foo so I could do bar".

Why? because I want my personal list of accomplishments to be about
*me*, and the progress on something new and exciting that I've done. I
want to be excited about something I invent, or dream up, or
program. I don't want to be proud that I outsmarted some random
programmer in Redmond whose boss told him "put a cap on how often our
users can do foo". I want to be the guy who comes up with stuff no one
else is doing, I don't want to become the minor day-player villain to
a nameless code monkey. I want to be the main character of my own
story.

That's my problem, in a nutshell, with much of closed source. They
lock me out of my own achievements. Yes, I can lay claim to some
pretty cool things, but only as a user. I have two choices: play their
game, or don't play their game. Trouble is, I was always the kid who
opted, in life, to just kinda do my own thing.

And that's not going to change in computing.


### The Missing OS

People keep telling me that "most users just want a web browser", and
while I do sometimes find that to be true of some group of users,
there's an odd mix of activities that people do on computers, and a
lot of it is outside the browser.

Which means, you have to install some applications.

Now, this is by no means unusual. I used to do it on Mac, and I do it
now on Linux; it's just the kind of "power user" I am. I have a list
of applications I want on a computer, and I'm pretty inflexible about
that. But by gum, when you install an OS and all the default
applications were written for 5 year-olds, and the amount of clicks it
takes to download and install good ones nearly breaks my mouse button,
then it's time to say enough is enough.


### Here Let Me Do That For You

Back when I was a dumb Mac user, all my Windows friends would make fun
of the OS for not letting me do anything. They meant that Apple had
very strict expectations of how a user might want to interact with a
computer, so in a world of infinite possibilities, Mac OS gave its
users...3, at the most. 

And mostly that's true. It's definitely one of the top reasons I
abandoned Mac: too inflexible.

While I'll admit that it does appear to me, at first glance, that
Windows is a little more liberal with options, the one thing that
stands out most about its interface is the constant, persistent,
eternal, non-stop, rapid-fire pop-up *things*. They appear in every
corner, with everything that you launch or plug in. Heck, if someone
walks past a Windows box, a pop-up alerts the user that someone is
within RADAR range. I don't know how anyone accomplishes anything
through the mire of helpful pop-up tips and alerts.

I'm sure pro users get into the control panel or registry and turn all
that stuff off, but for that to be the default betrays a lot about
Microsoft; you're not the pilot, just a co-pilot.


### Stupid Boot

I'm no computer genius. I've never written a kernel, or bootstrapped a
machine from raw assembly code. But I have *compiled* more than a few
kernels, and I've made an initrd (initial ram disk, or something like
that) or two, so I like to think I know a little something about the
boot process.

One thing I know about the boot process is that it scans hardware
available and constructs a system to talk to that hardware; this is
why, for instance, your OS knows how to read from and write to the
hard drive it is loaded on. It's why your OS knows how much RAM it has
available to it, and what CPU it has access to.

On a pre-historic OS, you might hardcode the boot process to skip
hardware probing and just boot on faith. That could be a good thing
for a speedy boot, but a bad thing should any hardware ever change.

And yet that's rather how Windows acts when it boots and hardware has
changed. I once repaired someone's Windows machine and had to swap out
the CPU. The resulting boot process was treacherous: Windows alerted
me that recovery was required, then it went through some kind of
"recovery" process (it didn't actually tell me what it was doing, but
it promised me that personal data would be restored), then it rebooted
mostly as normal, but once the user logged in, Windows prompted her to
reboot again so that the new settings could be applied. And all
through the process, the user was panicking, convinced that instead of
repairing the machine, I'd lost all of here data.

Maybe this is normal to pro Windows users because they've never
experienced anything different, but on a sensible OS, a normal boot
sequence when hardware has been changed would be no different that a
normal boot sequence. And that's exactly how it is, on Linux, BSD, and
even (at least, at the time of this writing, but I'm not taking bets
on whether it'll change, given the company's history) Mac.

Another time, I tried swapping drives on two machines. Windows
detected the change and refused to boot, telling me it could not
authenticate to that hardware, and so I was breaching its license
terms.

I know this will upset Microsoft, but the OS does not own the entire
stack. Hardware changes should be detected by an OS, but the OS's job
is to adapt to it, not fight it. A change in a CPU or a graphics card
shouldn't require Microsoft's prior written consent, and it shouldn't
claim that "recovery" is now required, as if there's been severe
system damage.


### Bare Metal

Not unrelated to Microsoft's apparent conviction that they should own
the entire computing stack are the latest developments in UEFI. As of
Windows 8 or so, Microsoft has made serious inroads into the firmware
of computer hardware, insisting that a computer be able to boot
"securely". The term "secure", as defined by Microsoft, is that they
hold the keys, and in this case it's a software key that identifies
the OS to the firmware; a little like an SSL cert between a server and
a client.

First problem here is that Microsoft does not and should not own the
computer that a customer has purchased. This is a huge issue, and it's
easiest summarised by stating unequivocally that it should be by
default that a new computer has *no OS* on it. I realise this means
customers would have to buy the OS and install it. I don't see that as
a bad thing, and in fact it's a good thing, because those who want to
buy the OS can, and those who do not, will not. Those that don't want
to do the install can hire someone to do it for them, and those that
can install it themselves will do just that.

There's no reason that Microsoft Windows should come pre-installed,
especially in this day and age of high speed hard drive imaging.

Secondly, an SSL cert (or a "secure boot" key) isn't actually *secure*
the way people think it is (assuming that most people, as they do,
think that "security" means "protected by a magical
force field"). These key checks can certainly verify that some entity
had access to a key and has flipped some specific bits and blown some
fuses, but it can't really verify anything about what those chips are
up to, and they certainly can't guarantee that the OS it's about to
launch is untainted.

Heck, nobody but Microsoft can verify that, and it's very much in
their best interest to claim that it's pure.

Microsoft is a software company (licensed hardware products
notwithstanding). They shouldn't be making firmware decisions for the
bare metal, and I'm frankly shocked that any company external to
Microsoft would allow it (so much for the free market, amiright?).


[EOF]

Made on Free Software.

