# Environment Variables




Setting environment variables on Unix isn't exactly something most
users do on a daily basis, at least not consciously. Applications they
run might set `env` variables for them, or they may set one or two in
a startup script like `.bashrc` and then forget about it
forever. Advanced users and developers might use them more frequently,
because they're surprisingly powerful. You can sort of think of it as
changing the basis of reality for the computer's operating
environment. Sort of like Virtual Reality goggles for the OS.

For instance, if you set an environment variable one way, then logging
into your Linux box drops you into a `bash` shell, and setting
an environment variable another way drops you into `tcsh`. Seems
simple, but multiply that by 2 or 5 or 20 and you can basically tell
your computer to use a completely different set of libraries and even
application versions (there goes the myth of not being able to have
multiple versions of an application installed).

At my old job, we used environment variables to create little
environments on-the-fly; we would script the creation of `env vars`
(that's what the cool kids call environment variables) around a
specific task or application set, and then use whatever set of
variables depending on what we needed to do. This was especially
useful since students and members might each want or need to use a
different version of some application, depending on what class they
were taking or what their project was using.

To sum up, it's surprisingly powerful.


## Setting Environment Variables

You might imagine that such a powerful technology is probably very
complex to use. I mean, if something has the power to change the very
fabric of a computer's runtime, then it must be something that only
advanced users should dare even trying to wield, right?

It's true, but nevertheless I'm going to give you the secret of this
power, right now. Brace yourself.

On Unix, to set an environment variable (let's say you want to set FOO
to "bar", as an example) for your current BASH or Bourne-style shell:

    $ export FOO='bar'

Or if you prefer a C-style shell like TCSH:

    $ setenv FOO bar

That is all.

Now no matter what you do *from within that shell*, `FOO` will always
equal 'bar'. What does that mean to a user? well, let's say that you
are launching an application that can either use a really boring 2d
graphics library to draw stuff on screen, or a really fancy library
with 3d effects and graphic card acceleration. How might it decide
which one to use? well, maybe it does some checks and figures it out,
and then sets `FOO` to whatever it's using. So if the user wants to
prompt the application to use one or the other, then setting `FOO` makes
that decision for the application.

That's all there is to it. Export a variable; if anything uses that
variable, and the variable's contents are sane, then it will be used.

Now, unfortunately, you can't just invent random environment variables
and expect applications to use them. It would be nice if you can `set
SPEED=reallyFast` and have all you applications work 10x faster than
normal, but that's not how it works. You *can* set anything to
anything (including made-up nonsense variables) but for an environment
variable to be useful, you would need to know what applications *look* for.

This is not always identified clearly to you. Sometimes it is, in a
project's documentation. Other times, it's reserved for developer
documentation, or left undocumented because the programming language
takes care of it by default. Python, for instance, has a bunch of
environment variables that you can tap into, and even manipulate from
within Python:

    $ python3
    >>> import sys
    >>> sys.path
    ['','/usr/lib/python3.4','/usr/lib/python3.4/plat-arm-linux-gnueabihf',
    '/usr/lib/python3.4/lib-dynload','/usr/local/lib/python3.4/dist-packages',
    '/usr/lib/python3/dist-packages']
    >>> import os
    >>> sys.path.append(os.path.join(expanduser("~"), "py"))
    >>> sys.path
    ['','/usr/lib/python3.4','/usr/lib/python3.4/plat-arm-linux-gnueabihf',
    '/usr/lib/python3.4/lib-dynload','/usr/local/lib/python3.4/dist-packages',
    '/usr/lib/python3/dist-packages','/home/klaatu/py']

And so on. Notice how similar it is to this:

    $ export PYTHONPATH="$PYTHONPATH:/home/klaatu/py"
    $ python3
    >>> import sys
    >>> sys.path
    ['','/home/klaatu/py','/home/klaatu','/usr/lib/python3.4',
    '/usr/lib/python3.4/plat-arm-linux-gnueabihf',
    '/usr/lib/python3.4/lib-dynload','/usr/local/lib/python3.4/dist-packages',
    '/usr/lib/python3/dist-packages']

It's essentially the same thing, although Python's IDLE interface
behaves differently depending on how it's launched, so there are minor
differences in the exact path, and in the first instance we appended
the PATH and the second we implicitly pre-pended (because no PYTHONPATH
variable existed until python was launched). The important thing to
understand, though, is that if an environment variable exists **and**
an application probes for it, then it gets used.


## Spontaneous Environment Variable

In BASH, you also have the option of setting and using a variable at
the same time that you launch an application or command that you want
to use the variable:

    $ TWOLAME=yes ./audacity.SlackBuild

I use this trick frequently with [SlackBuilds](http://slackbuilds.org)
on [Slackware](http://slackware.com) because SlackBuilds are just
shell scripts, meaning they can directly utilise environment variables
as pre-defined variables. Python and C++ and most anything else can do
the same thing, as long as the author bothers to check.

The logic is pretty simple; look at existing settings in the OS
(actually, specifically the shell that launched the application),
check to see if a value exists for a given variable, use that value if
it does exist, and otherwise give it some default value or ask the
user for a value by way of a prompt or whatever.

That's all an environment variable does, really; it provides a value
for variables, which it makes available to any application asking for
it.

Here's a really simple example that you can run right now in BASH:

    #!/bin/bash
    SAY=${SAY:-'hello world'}
    echo $SAY


Run it in BASH to see it in action:

    $ bash ./say.sh
    hello world
    $ SAY='i like lettuce' bash ./say.sh
    i like lettuce

or in a C shell:

    $ sh ./say.sh
    hello world
    $ setenv SAY whut
    $ sh ./say.sh
    whut

As you can probably surmise, the script checks to see if a variable
`SAY` exists yet, and if so, what its value is. The script does this
with some BASH shorthand, specifically the `${:-}` bit in the second
line. You could do it more generically with some test statements, but
if you're OK with using some shorthand, it certainly makes things
quicker to write (and most modern unix systems either have BASH on
them, or have access to BASH as needed).

## Uses 'Em if you Got 'Em

Any application can check for environment variables and use them, so
if you know what variables an application looks for (they usually tell
you what they use, and there are some that are so universal that you
can pretty much assume applications use them), you can define or
change variables and have them affect how an application runs.

In theory, you could define lots of things prior to running an
application, even where it looks to get libraries (as with
PYTHONPATH). For compiled applications, you can change the
`LD_LIBRARY_PATH` variable to pull the rug out from under a binary and
tell it to go elsewhere for the libraries it has been told to use:

    $ LD_LIBRARY_PATH=/opt/lib64/ ~/bin/blender

Now, you could take this to an extreme level and manually define where
every application looks to get its resources, or you can use it for
exceptional cases (that's the most common use for this); it's all up
to you and what your specific needs are. The point is, you should know that environment variables exist, why they exist, and how to use them.

And now you do.

[EOF]
Made on Free Software.

