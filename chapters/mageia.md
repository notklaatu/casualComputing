# A Look at Mageia 5

Mandrake Linux was a fairly early distribution (it was around since
2000, at least), and it was a real heavy-hitter for quite a while. I
wasn't using Linux at the time, but from what I've heard, it was one
of the first distributions to come up with the idea of a subscription
model for updates and extra packages, which I think still has some
merit (and apparently, so does everyone else, since many software
vendors are now implementing that same idea).

## Née Mandrake

Late in 2016, I embarked on an academic exercise over the weekends to
install one Linux release from each year since 1993. I didn't come to
discover Linux until 2006 or thereabouts, so this exercise gave me an
emulated sense of just how far Linux has come, and how quickly or
slowly. Making it easy to configure X, for example, I felt happened
too slowly. I'm not saying I could have done better, but it wouldn't
have hurt to have some more helpful tools to get `XF86Config` sorted,
and it amazes me still that nobody ever bothered making `XF86Config`
or `xorg.conf` any less confusing; as config files go, the X config
surely is the worst I've ever experienced to this day.

Everything else, though, moved quickly and gracefully, and generally
it was smooth sailing since 1998 or so. Even so, when I got round to
installing Mandrake 8.0, I was *flabbergasted*. And I don't think I
often use that term, so I really really mean it.

Mandrake 8.0 was a thing of beauty. It installed easily, it was
friendly and helpful without getting in the way, it made partitioning
easy and, most importantly, it let me configure and then test my
screen and mouse. And then it was installed, and I found myself
looking at a beautiful KDE desktop with helpful shortcuts to
documentation and support channels.

All of this is pretty standard now, but from what I'd seen up to that
point, it was the most professional and genteel presentation I'd
seen. If I'd been shopping for Linux back in 2001 (*o, would that I
could have been so progressive*), I feel pretty sure I'd have become a
Mandrake user for life (I wouldn't have been savvy enough, then, to
understand Slackware; I admit it).

![Mandrake 8.0 from 2001](images/mandrake8.png)

At some point before I had even heard of Linux, it merged with another
distribution called Conectiva, forming Mandriva Linux.

I don't have a whole lot of history with this bloodline, but in fact
it was the first Linux I ever booted. I had gotten the boot DVD from
the back of an introduction-to-Linux book. I was eager to try Linux,
so I tried booting off of it mostly to prove to myself that such a
thing really, truly existed. I'd been reading up on Linux, researching
it, trying to wrap my head around this idea that yes, there *really
are* operating systems that come free in the back of books or on the
covers of magazines (and online!), and they could be the way one uses
a computer. Every day. Forever!

For the record, Mandriva did boot the old second-hand computer I was
borrowing at the time. I remember stars and very noble-looking
penguins, and that's about it. Well, that and

*My God, it's full of stars!*

Later, I tried the [Metisse](http://insitu.lri.fr/metisse) windowing
system that they were working on integrating into their desktop. It
was really neat, but by then I was settled on Slackware (I wasn't any
*good* with it, but I was using it and I wasn't going to quit) so I
was only a tourist in Mandriva and didn't stay for long (I think I had
to install it on a temporary partition to see the effects, but that
was the extent of it).

## eee PC

The real game-changer, not just for me but for the entire computer
industry, was the humble eeePC: arguably the world's first "netbook"
and the "low end" power-hitter that spawned an entire sub-genre of
laptops. It was small (7 inch screen originally, later to be expanded
to 9 or 10), low-powered, lightweight, ultra-portable, and pre-loaded
with something called Xandros Linux.

I bought in around the second or third iteration, mere months from the
time that Mandriva had released the first third party (non-Xandros, I
mean) eeePC-optimised Linux OS; it was Mandriva specially tuned and,
most importantly, correctly sized, for the eeePC. It was (at least, as
I remember it) the only distribution that went to the trouble to do
that, at the time, so I decided it would be the one to use.

I installed it, and loved it. It was polished, it looked great,
everything worked, it was RPM-based so it "felt" like Fedora (that's a
meaningless statement; it just means that when I went to install
packages, they ended in `rpm`, which superficially comforted me), it
had all the packages I could want (I think I used an "extras"
repository for the really good stuff, but that was no more outrageous
than using rpmfusion).

I didn't move away from Mandriva on that laptop until years later when
it became my *only* computer, at which point I basically had to switch
it over to Slackware. Not Mandriva's fault.


## Mageia

Since then, Mandriva has dissolved as a business but has spawned the
Mageia and OpenMandriva distributions. I've checked in on Mageia once
a year or so since it started, mostly just to see what they've been up
to, but I guess it escaped my attention that I might actually run it
on something. But recently I got a computer that I wanted to get up
and running quickly for every day use; I didn't need anything special,
just a Linux desktop with the usual tools, which I'd mostly use to SSH
into my servers or my workstation at the other end of the apartment.

Fact is, Mageia is as good now as it ever has been. All this time, I'd
kinda been wondering where there was a nice and stable, long-term
supported Linux distribution for the "normal" desktop users in an RPM
world. Not that there's anything wrong with, for instance, Fedora, but
one of its self-stated goals is to be cutting edge, and Red Hat
doesn't tend to offer much in the way of even run-of-the-mill
multimedia packaging without source RPM re-builds.

(SUSE is another very notable and important distribution to consider,
when looking for a stable and long-term desktop, but some times you
just have to flip a coin and make a choice.)

The thing about running Mageia (or SUSE or Slackware, for that
matter) is that some of us get nervous about wandering out too far
into the fringe. Some Linux users worry that they're already far
enough into the unknown by using Linux in the first place that they
feel they ought to use a distribution that gets a lot of press and, in
theory, a better chance of support from the rest of the computing
world.

Here are some things I've come to notice about Mageia, and about using
"obscure" (quotation marks are *very* deliberate there) Linux
distributions.


## Packaging Concerns

I admit, I did have some fear that somehow I would come across some
application that just wasn't packaged for Mageia. It kept me from
using Mageia on computers at the community center where I volunteer (I
maintain Mint there, because I was just sure, at the time, that
everything and everyone supported Ubuntu).

Believe me, I have no technical reason to fear that, since I'm happy
to build my own RPM or do a manual compile and install as needed. But
to my mind, if I'm going to "cheat" (by Slacker standards) and use a
pre-packaged Linux system on a machine, then I may as well expect
everything to be pre-packaged. Otherwise, what's the point?

When it comes to applications, there are two scenarios any Linux user
fears:

1. You look in your software repository and discover that no one has
gotten round to packaging up the one application you use every day,
all day (or they have, but it's such an old version that it may as
well not be there at all).

2. You go to a random website and find a cool new
application that advertises Linux support and find that they only
offer Ubuntu-branded `.deb` packages (and a Fedora-branded `.rpm` from
four years ago).

These are basically the same issue; they only feel different because
they occur in different scenarios. One is a repo let-down (you curse
your distribution, Linux, and yourself for not knowing how to package
stuff, and so on) and the other is an Internet let-down (you
curse the world for being anti-Linux, the software vendor for not
bothering to make ONE entry in their Makefile, and so on).

The fact is, I've not used even one Linux distribution where I've not
had to do a little package work-around. I don't *like* that, but since
each distribution insists on being its own central source for all
one-click installable packages, that's what happens. This does *not*
happen on Slackware because it packages *nothing*; it lets you do that
on your own, with `makepkg`. It also would not happen [as often] if we
all could agree on one packaging format, or else we agreed that
[AppImage](http://appimage.org) was actually a pretty brilliant idea
after all.

But we don't, so in "pre-packaged" Linux, you're guaranteed to
encounter "third party" software that didn't get the memo about how
Linux packages stuff, or hasn't been packaged at all, or has a new
release that hasn't gotten added to the repo yet. I know this to be
true, because I offer several applications myself that are not
packaged for any Linux distributions, so if nothing else I can speak
as a non-prepackaged software distributor.

Non-packaged software is not only inevitable, but it's also
healthy. The whole point of open source is to enable anyone to write
software for any OS, so it's just not possible, and it shouldn't be
possible, to package everything ever written.

If it was, then open source would be failing.

So don't be afraid of finding software you want to run that has no
one-click installer for your distribution. Here are a few sure-fire
ways to deal with packages your distribution does not have:

* Find a package in a related or similar distribution, or on the project's own site, explode it, and re-purpose the resulting bits and pieces.

* Find a source RPM and re-build it for yourself (a lot easier than you think).

* Download the source code, compile it, and use [checkinstall](http://asic-linux.com.mx/~izto/checkinstall) to create a personal RPM.

* Find a "generic Linux" tarball, if available, and put it in `opt` or `~/bin` or similar and run it from there.

So far, I've encountered one application that was not apparently
packaged for Mageia, but I found it in a Fedora repository and was
able to adapt it with a little bit of effort. There are a few others
that I can think of that probably aren't packaged, or else are not
packaged at their latest versions, but the same holds true for them;
adapt what you find, and do it so you never have to do it again.

Problem solved.


## urpm

Of course, a lot of software is already packaged by the Mageia
distribution, and ready for you to install in a few clicks from the
Mageia Control Center.

I tend to avoid the GUI installers and go straight to a shell.

Mageia, like Mandriva and Mandrake before it, uses the `urpm` command
to search for, install, and manage software. Now, my path along the
Linux shores was such that I encountered [Yellow Dog](http://www.fixstars.com/en/technologies/linux/) and Fedora well
before I even knew that `rpm` was a command and not just a packaging
format, so I cut my packaging teeth on `yum`.  As a result, `urpmi`
was very unique to me when I first encountered it on the eeePC. It
felt more direct, closer related to `rpm` than even `yum`.

I quickly came to enjoy it, even though there are a few quirks in
design (the fact that `urpmi.addmedia` exists, rather than `urpm
--add-media` or something more intuitive, for instance) that other RPM
packaging systems (like `yum` or SUSE's `zypper`), have smoothed out.

On the whole, `urpmi` is a nifty little interface to common `rpm`
commands plus as sane dependency resolution policies as you can hope
for.

Some of its nicer features:

* `urpmq --fuzzy` performs an rpm fuzzy query.
* `urpmq --whatprovides` tells you what package provides a given library or executable.
* `urpmi` installs a package, whether it's a local file or something that it finds in a repository.
* `urpmi --no-install` downloads packages to `/var/cache/urpmi/rpms` but does not install them.
* `urpme` **e**rases a package (like `rpm -e`)
* `urpmf` **finds** a file contained in a package (for example, `urpmf libfoo.so` finds all occurrences of libfoo.so in RPM packages; like `find /var/log/packages/ -type f | grep foo` in Slackware)

Pretty simple, and for a change it's quite nice to have the commands
split into several different invocations. It seems odd even to me to
praise this, because one of the things I never got my head around in
the old apt-get system was the split between `apt-cache` and `apt-get`
and so on, much less the options consigned to each (you have to admit
that `apt-get remove` is a stupidly confusing concept; how do I *get*
a *remove*?). The new `apt` command fixes most of the confusion for
me, but `urpm` uses sane options and intuitive invocations.


## No Sharp Edges

One of the things I love about Fedora is its fearless development, and
that's also what keeps me away from it when I want a stable
system. Not that you can't run Fedora as a stable distro; I've done it
before, myself, and it works. The problem for me is the temptation to
push the boundary, to keep getting closer to that cutting edge, and
then regretting it. So I acknowledge that I'm the problem, not Fedora.

And anyway, you've got to admit that Fedora's repositories are filled
to the brim with all the fun applications a busy computerist could
ever want.

RHEL (or CentOS, or SL, and so on) is stable but lacks the
packages.

Mageia, in short, has both. It's a reasonably long-term release cycle,
so you won't find yourself needing to upgrade every 6 months, and yet
its repository is reasonably well-stocked.


## Magickal Mageia 

I regard Mageia as a sort of safer version of Fedora. It doesn't have
Red Hat's or SUSE's length of support, but it has a notably better
repository than RHEL or even EPEL.

And in terms of staying-power, you have to admit that a distro that traces
its development back to 2000 or so is pretty good.

So if you're looking for something between the breakneck pace of a
cutting-edge distro and the long-term support of Enterprise-class
desktops, Mageia is a nice, solid option.

[EOF]

Made on Free Software.


