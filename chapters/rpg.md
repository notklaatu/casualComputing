# Role Playing Games


The ("tabletop", or "pen and paper") RPG game is one of those fine
traditions that nevertheless has had quite a lot of unfair
pre-conceptions and clichés applied to it for pretty much its entire
lifespan. As a child, I was forbidden from playing them, because a
popular notion at the time was that they were a thin veil over a
literal gateway into the occult. Later, it became popular to claim
that the only people who played RPG were male social misfits, usually
with the subtext that they were latent predators of one sort or
another. And then everyone fell back on it being boring, not flashy
enough, not high tech; why would anyone play a tabletop game when
there are MMORPG's out there, waiting to accept your micro transaction
payments plus subscription fees?

So, really, what is tabletop RPG? why is it something that people do
and enjoy?

Well, I think it was one of the early D&D rulebooks that described
role playing games as "cops and robbers, with rules". I find that to
be true in spirit if not in implementation, since children playing
cops and robbers are usually running around the backyard chaotically
while kids and adults playing an RPG are usually sitting round a
table. Still, the basic idea remains that a bunch of people are
participating in telling a common story, but instead of arguing over
whether that imaginary laser beam hit the imaginary bad guy or not,
there's dice and character stats that makes the decision for everyone.

While that's accurate, there's just so much more to the RPG
experience. Let's look at it from all of its many angles.

## Object Oriented Gaming

I've written about how tabletop gaming is a form of analogue
programming, and that's one of its most appealing aspects, for me
(well, one of many). With card games and board games, it's hard to
miss the programming aspect of it, as long as you look for it. But
with an RPG, I think there's the danger of confusing what's going on
with a simple interactive Choose-Your-Own-Adventure book, sort of like
one of those BASIC turtle-logo scripts (or Scratch, depending on your
age) you wrote in school, as opposed to, say, a Python or C++
application.

I've always enjoyed the RPG experience, but I never truly understood
the complexity of it until I came to understand that it isn't just
scripting. All the players agree on a set of pre-defined rules, random
number generators in the form of dice are used to introduce entropy
into the progression of the game, and a GM acts as a database API to
provide access to NPCs, maps, and story (and general management,
including seemingly out-of-scope decisions, random number
tie-breakers, and so on).

And you can really break that down to some fascinating
conceptualisations, if you think about it long enough. For instance,
if we just lazily think about an RPG as four people sat around a table
stepping through a standard campaign, then it sort of looks like this:

* Bob, an orc knight with 12 HP and a war hammer
* Alice, a elven knight with 12 HP and a long sword
* Ted, a wizard with 10 HP and a book of spells
* Carol, the DM

And the code they step through would sort of look like this:

    00 PLAYER=(knight,orc)
    05 LOCATION=dungeon
    10 MONSTER=1
    15 FIGHT? GOTO 30
    20 RUN?   GOTO 25
    25 PRINT("You are hit as you run, but survive.")
    30 PRINT("You attack, but the monster kills you.")
    35 END

Pretty easy to follow. Maybe a little too easy; after all, any
"choice" they have has to have been accounted for and scripted in
advance.

But if we think of these players in terms of how the *game* sees them,
it would be a little bit more like this, in Python-like pseudo code:

    class Player():
          def __init__(self,CL,RC):
              self.CLASS=CL
              self.RACE=RC
          def defense(self,HP,THAC0,WEAPON,DEX):
              if self.RACE == "elf" and WEAPON == "bow":
                 RACE_BONUS=1
              elif self.RACE == "orc" and WEAPON == "hammer":
                 RACE_BONUS=1
              else:
                 RACE_BONUS=0
              HP=HP
              AC=THAC0+DEX
              DAMAGE=DEX+RACE_BONUS

And so on.

Now you don't need a script. Any obstacle in a narrative can be
dropped in front of the players, and it's a function of mathematics
and player choice that decides the outcome.

In our first pseudo-code example, our players had two choices when
encountering a monster; they could run or fight. You could add in four
other options (play dead, beg for mercy, look to see if the monster
has a thorn in its paw making it grumpy, tickle the monster) and it's
still a multiple choice test.

With our RPG pseudo code (actually not pseudo; it will run, even
though it doesn't do much), you get levels of choices:

**GM** A monster appears. What do you want to do?

**Player 1** Wield my war hammer and attack.

**Player 2** Climb a tree and wield my bow.

**Player 3** Run.

**GM** The monster attacks you with [rolls dice] 12 damage.

**Player 1** My armour plus natural strength is 14, no effect.

**Player 2 & 3** Out of range.

**Player 1** I attack with [rolls dice] 15 damage + 1 race bonus.

And so on. (Just wait until the group finds out that the monster has
rock-like skin making him basically immune to war hammer blows, and
they have to call the wizard back to soften it up.)

Heck, the obstacle doesn't even have to be a monster. It doesn't even
have to be an obstacle. The GM could drop a completely innocent person
in front of the players, and they could choose to converse or attack
or barter or *anything*.

And to be clear, the **GM** doesn't actually have to be a human. It
could be a deck of cards or a book; once you start introducing
concepts that depend on some random result (like a roll of the die),
or math problems (calculating level differentials, armor effects, and
so on), there's a degree of non-linear storytelling that emerges from
the events themselves. Sure, it may not be very nuanced, and it become
predictable without any real live intervention, but it's dynamic, it's
flexible, it's non-linear. The system is self-contained, and will
continue to function for as long as you decide to use it.


## Maths 

Love it or hate it, there's an inherent gamefication to a collection
of numbers. Let's face it, if you see this:

    (strength + weapon) - dexterity/encumbered + roll = hit

You *want* to see what happens if you plug in random numbers (as long
as you don't personally have to do figuring, unless you're into doing
calculation for fun). 

If the terms in the equation mean something to you, then you want it
even more, and the complexity of solving the puzzle increases: how can
you maximise your hit? Sure, a lot of it's going to come from
strength, but if you choose an especially powerful weapon then you can
give your attack quite a boost. Unless, that is, the weapon is too big
and encumbers your movement.

So you start looking over your inventory sheet, trying to do the math
in your head, except that you never can complete the equation because
there's that darned dice roll you have yet to do. But you ignore the
unknown factor and come up with a strategy, wielding the weapon you
think best, and then you roll. Maybe you're happy with your roll, or
maybe you're cursing under your breath; either way, there's the attack
and you look to the GM anxiously to find out how much damage you've
actually done.


## Design

Part of the fun of reading books and playing games, at least for me,
is finding out what kind of world I've been dropped into. I'm not
talking about the story-line, I'm talking about the environment. What's
the map like? what's the terrain? what's the landscape look like at
sunset? what can I see when I look up at the sky? what are the
cultures of the people in this world? what do they think about? what
do they fight about? what do they want to achieve in their lives?

In several of my favourite books, the *world* is as much a character
as the people being written about, and in RPG's it's almost always, at
least unofficially, a sub-plot that you must learn about your
environment as you go. Every RPG is exploratory, in part because you
may not know the setting at all, but even once you become familiar
with a world, there are no visuals. You *have* to dig deep into your
surroundings, deliberately, because you can't just glance around and
absent-mindedly take it all in.

There's a lot of artistry that goes into building a world, so part of
the RPG experience is the act of walking into an imaginary art
gallery, and taking it all in. Some of the art is the stuff that the
game designers put there, provided by the core rulebook and add on
modules, but the other bits and pieces belong to you, the
players. You're building this world as you go, using whatever the GM
has planned, what the players prompt the GM to invent on the
spot, and, of course, what the players themselves force into existence.


## Planning

The RPG designers aren't the only ones plotting out maps and filling
in statistic charts. For many people, myself included, part of the fun
of RPG is the prep work. It's the part where you sit down, or take a
stroll outside, and ponder a character build. It's like meeting
someone briefly at a café and hearing their life story; you get all
the background information, you hear about all of the interests and
passions and fears and hopes, and then, in an RPG, you get also see
their future.

And that's fun; all the planning, the rolling for stats to determine
how strong they are, or how dexterous, or how charismatic, and just
how good their skills are with different weapons or equipment.

Then there's the studying and the research; opening up the core
rulebook, reading about all the nuances of how interactions are
calculated, how combat is implemented, and all the extra stuff about
the history of the world and the cities you'll be visiting, the races
there, the cultures.

I say it's "studying" but really it's like a sort of inverted book
that you read from the outside in.

And for a GM, the process is even more of the same; the research, the
planning, *plus* the writing of a campaign.

It's all the part of the game that isn't in the game, and it can keep
you entertained for weeks. I literally read rulebooks for games that
I'll probably never play, the same way you might browse a travel
brochure for a country you'll likely never visit.


## Puzzles and Storytelling

RPG storytelling is more than just non-linear narrative, and it's more
than just keeping the GM awake with requests for on-the-fly
descriptions of tapestries and the contents of the desk junk
drawer. The stories in an RPG evolve out of necessity; the GM or the
campaign book might have a specific story proscribed, but if the
players don't ever rub the lamp to reveal the vengeful djinn or if
they don't go into get into the boat to sail to the forgotten island,
then the story, at the very least, has to be adapted.

But it's more than even that. There are other stories happening
during an RPG: the story of the players themselves. Any RPG player
knows that deep down, the *most important* tale being told during the
game in the story of their own character's development as a
person. Those are the stories we humans truly care about. Does the
level 1 wizard's apprentice ever get a wand of her own? does the level
5 knight ever master the use of an orc's war hammer? Does the level 2
orc grow up to be the leader of the horde? Does the level 6 witch
lower her emotional defenses and finally find true love?

Those are the kinds of stories that no one can tell, but anyone can
experience.

The gateway to those experiences is RPG.

[EOF]

Made on Free Software.


