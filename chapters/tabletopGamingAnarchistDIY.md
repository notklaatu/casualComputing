# Tabletop Gaming and Anarchist DIY Ideology


I'm the first to admit that sometimes I'm slow to pick up on the
obvious, but I also humbly posit that when I finally do get the
message, I listen and I listen *good*. Such has been the case with
what we all seem to call "tabletop" gaming.

"Tabletop" gaming, of course, is the hip and trendy term for what used
to be called "boards games", or card games, or RPG, and basically
anything that was not done on a computer. A more accurate term, I
guess, is "analogue" gaming.

I grew up with the usual assortment of board games. Nothing wrong with
those, necessarily, but they were very much that predictable list of
family-friendly, standard-issue board games.

After hearing murmurings about an alternate, members-only,
super-secret world of cool board games, some mystical cross between
hardcore RPG and traditional coffee table pastimes, I started
investigating. I moved slowly; between childhood cautions against the
"gateway to the occult" that was D&D, and flat out unfamiliarity with
modern gaming, I hardly knew where to begin.

Eventually, I found an entry point in the form of Cards Against
Humanity. That's an easy, mad-lib style game, and by all accounts a
rip-off of a game called Apples-to-Apples (which I had vaguely heard
of but never played). But it's an easy game to fall into, because the
focus is on humour, and it doesn't much matter whether or not you're a
"gamer"; it only requires that you have a twisted sense of humour and
a high threshold for what offends you.

Most importantly, it's a Creative Commons game, so it encourages
people to *not* necessarily buy the game, but print it at home. Or
else, buy it and make your own cards. The idea was totally unique to
me. You mean you can add to the game yourself? Surely no other game
ever had that notion.

How little I understood tabletop gaming culture!



## User Mods

I'll blame technology for this, but for most of my life, until I
discovered open source, it very rarely occurred to me that I could take
something and use it in some other way than how its creator had
intended. It's a shame I have to admit that, because it really is
pretty obvious: when we're kids, we dig around in the kitchen cupboard
for plastic containers so we can build makeshift cities for our Lego
figures, or we collect all the pillows in the house so we can build
forts for ourselves, but at least for me, getting into software-based
tech was sort of a tunnel-vision inducing experience. You get some
software, you're explicitly forbidden from figuring out how it works,
you're told exactly how you may use it, and indeed if you attempt to
abuse it, it crashes or ends up being frustratingly inflexible.

I should mention that open source software has none of these
drawbacks, although the limitation of software and ROM chips are still
there. You can modify and add to your open source system, but you're
still bound within the parameters of it being a computer. It can only
do so much.

With tabletop gaming, there are no such restrictions.

You can add to a game, modify the rules, share your new ideas with
friends, invent an entirely new game. Unlike so much closed source
software, you're buying the assets when you buy a game. They are yours
to do with as you please. If you don't like the fact that there's a
Joker card in your deck, set it aside. If you want the Joker
to have special powers, you can grant it special powers. Nothing
stands in your way.

That's what entertainment should be about: creativity. Sure, you can
sit and enjoy someone else's creativity, and I very often do, but if I
*want* the opportunity to be creative, too, then a good entertainment
system ought to allow for that.

Tabletop gaming does, and the best tabletop games encourage and foster
it.


## Imagination

It surprised me and appealed to me that tabletop gaming, as it turns
out, has all the same thrill and, in many cases, immersion that a
complex video game has, and indeed sometimes it even does better at
these things than a fancy video game.

The obvious analogy is that tabletop gaming is to video games as books
are to movies; allowing for some overlapping, one shows you the
results of an artist's imagination and the other acts as a catalyst
for your own.

At least at the time of this writing, I'm happy to admit that I'm a
fairly superficial gamer. On a spectrum with **game theory** on one
side and **pure fantasy** on the other, it's the fantasy that appeals
most to me. I don't tend to play a game to analyse its competitive
mechanics, I play because for a few hours I get to become a rogue or a
necromancer or a dictator of a fictional country, and I visit new
lands, and there's intrigue and stories and bits of stories that only
get hinted at in the artwork of the cards. I enjoy most the games that
provide that immersive imaginative experience the way books and radio
plays do.


## Simplicity and Power Outages

A lot of what I do in real life depends on modern conveniences, like
electricity, and the assurance of the continued availability of
microchips. I'd find myself with a lot of free time if all of those
things suddenly vanished.

Or would I?

In fact, I'm perfectly happy with analog computing, and have been for
a very long time. I used to compose music in notebooks during science
class; sometimes I'd be off on a few notes but at worst I'd get the
idea and general progression down on paper, so I could correct things
when I got home to a synth. I wrote an entire novel by hand during
lunch breaks in college. I designed the game mechanics of a card game
in my head as I walked to and from work.

I don't really rely on computers as much as I like to think that I do,
and tabletop gaming is one of the things that opened my eyes to that.

Computers and software, video games especially, come and go. They get
released, people play them, and then the next big thing comes along
and eventually he game becomes too old to play on a modern OS, or else
it just loses its audience. Sure, some games escape this cycle, or
they get re-made by some diligent hacker, or emulated by a clever
programmer, but let's face it, sometimes the power goes out.

Sometimes you're sitting around at home in candlelight, wondering what
to do with your evening without the internet or without Steam. Or
you're out on an "unplugged" adventure somewhere, like, you know,
*outdoors*, without power. Or maybe you're just trying to shed an
addiction to the obligatory updates and upgrades that comes with
modern video games.

Tabletop games can have a similar life cycle, except for the
obsolescence part. Sure, people may lose interest, or the game might
go out of print, but the game itself still exists. The scene has been
set, the story written, the game mechanics proscribed. As long as
someone remembers it, it's still a game, and from there, you never
know what might happen or who might randomly discover it, revive it,
and enjoy the heck out of it. I'm really not speaking theoretically,
here; I did just that with an old game called [Dark
Cults](http://boardgamegeek.com/boardgame/1784/dark-cults) from 1983:
I heard people talking about it, I found an archive of its rules and
deck, I re-implemented it at home, and [released my
work](https://gitlab.com/notklaatu/darkoccult) in hopes of others
finding it and enjoying it.

And best of all, no electricity or microchips required. Sure, it helps
to make cool-looking cards on your computer and to print them out on
card stock, but in terms of what we'll all do after the Apocalypse,
analogue gaming ranks pretty high on my list.


## Analogue Programming

I have long been interested in analogue programming. It's something
that has always fascinated me, all the way back to the time as a kid
that I attempted to make a computer out of cardboard (it did not
work). I want to be able to "program" something without the
microprocessor, which is strange coming from a guy who never could
understand why his primary school teachers kept insisting that the
abacus was actually a calculator.

Even when I started analogue gaming in earnest, I didn't see games as
a program. They were games, with clever rules and cool art. I didn't
know what made them so fun, and I didn't really give it much thought.

Then one day I was playing [Cards Against
Humanity](http://cardsagainsthumanity.com) with my girlfriend; we'd
been looking for that ever-elusive 2-player game so we could spend an
evening a week playing tabletop games together. **Cards Against
Humanity** requires at least 3 players, but in the rulebook it notes
that as a workaround you can deal a hand to "Rando Calrissian", a fake
player that receives and submits random cards each turn. Amazingly,
the results are just as often hilarious as they are utterly
perplexing, but I have been in games with Rando where Rando has very
nearly won the game. Such is randomness.

As rule mods go, that works (and in the context of that game, it's
very effective), but it's not what you'd call "elegant". The mod boils
down to: "if you need another player, play two hands".

When I revived the 80s game [Dark
Cults](http://boardgamegeek.com/boardgame/1784/dark-cults) as [Dark
Occult](https://gitlab.com/notklaatu/darkoccult), my world was changed
when I discovered the 1985 expansion pack, which included modified
rules for a single-player game. As a player, the scheme is so elegant
that the game may as well have been meant for solo play. But from a
programming standpoint, it's ingenious and elegant.

I don't know how much source code you've read, but I've read a lot,
and it can best be described as a kind of surf: you wade through some
tides, picking out the important spots on the horizon, and then you
get to the part where it all comes together, and it's like you're
riding on a wave. All that preparation and necessary groundwork
finally pays off; it all makes sense, it's beautiful, you understand
what the code does. That's how the single player rulebook felt; you
see how the card decks are getting divided, you understand that there
are percentages involved, you see how challenges are being mitigated
and how randomness plays the part of your opponent, but then you deal
the cards and actually play it. Then it just downright humbles you.

**Dark Cults** in single-player mode *programs* the card deck to
provide a reliably entertaining game progression for the
player. Between mixing cards carefully into sub-decks and then
dictating (per card) what new card may be drawn by the player, the
single player version is literally an analogue program consisting of
decks of cards and the player. Together, they form an analogue
computer destined to play a game, with the human's imagination
constructing a story to go along with it (which is the point of the
game; it's a story-telling RPG-alike).

Tabletop gaming provides many of the same challenges, the same
mechanics, and the same rewards, as computer games. Not just for the
player, but for the designer, as well. You can "program" the
situation, set the stage for an imaginary environment, and forge the
same obstacles and challenges, just like in a computer game.


## Barrier to Entry

The concept of a *barrier to entry* is tricky to agree upon because,
strictly speaking, there's always some barrier to entry. I do remember
trying to create my own board game as a kid, for instance, and failing
miserably.  My attempt failed partly because I was unsatisfied with
how crude my board game looked when compared to a professionally
printed game, but more importantly because I had no concept of game
theory. All I knew was that board games involved a spinny thing, maybe
some dice, cards, and player pieces; I didn't know how to make those
things interesting or useful, and I certainly had no concept of how to
manipulatively pit players against one another or against a fake
common enemy.

So that's the barrier to entry for tabletop gaming, and it shouldn't
be discounted. Not everyone wants to mod a game or invent their own
game, or even learn the rules and play a game. And those who do must
learn a little something about social engineering and game theory.

Then again, those barriers are all within the person, not the medium.

Computer games have a larger barrier to entry, because to play them,
you need a computer, and very often you need a particularly powerful
computer. To create them, you have to learn the syntax of a
programming language, and potentially some art applications, and
whatever else you need for the game to come alive. The end result may
be better by some measure, but if your primary goal is to create a
game and not necessarily a video game, then none of that should be
considered a hard requirement.

It's also easier to dabble in the "programming" of tabletop
games. Truth is, most people who have played a tabletop game already
have dabbled. It's why there's such a thing as *house rules*; people
play a game and they discover that some mechanic isn't quite working
for them, so they change it to something that feels better, or they
flat out get bored reading the rulebook and settle for enough of the
rules that make the game go.

You can't quite do that as much with video games. There are mods for
the everyday user; any PC gamer with a neon-lit keyboard and "pro
gamer" 12-button mouse will tell you how easy it is to download and
install some arbitrary user mod from the net and, at least in some
small way, change the way they play the game. And more advanced users
might be able to hack their way around some exposed parts of a game's
lua scripts (or similar), but mostly video games don't encourage
modification, and in the worst cases they strictly forbid it.


## Free to Game

I'm not putting on rose-coloured glasses and saying that tabletop
gaming is a blissful place where sharing and intellectual freedom
reign supreme. Even if it is, I suspect that were tabletop gaming a
multi-billion dollar industry the way video games are now, then
efforts to enforce copyright and thought would also increase. However,
you can't really alter the fact that tabletop gaming happens
dynamically. The program is proscribed, but only the half written in
the rulebook. The other half are the players, and they are and will
remain free agents.

I'm also not saying that video games are bad, or not as fun. Video
games have a wealth of strengths that tabletop games do not have, at
least not built-in.

Tabletop gaming is a brave new world that isn't really new, and you
should try it, if you haven't yet. Check out sites like
[BoardGameGeek.com](http://boardgamegeek.com), read a few reviews, and
get started. Head on over to [Drive Thru
Cards](http://www.drivethrucards.com) and [try
some](http://www.drivethrucards.com/product/135134/Goblins-Breakfast)
[print-and-play](http://www.drivethrucards.com/product/149512/Hallow)
[games](http://www.drivethrucards.com/product/171115/Brass-Empire-Free-PNP). Discover
some [new](http://www.worldofmunchkin.com/cardgame)
[games](http://www.looneylabs.com/games/fluxx). It might take you to
places you never expected.

[EOF]

Made on Free Software.

