# Nixstaller




There are many ways to install things on the Linux OS, and one of the
methods I, as a sys admin, have been playing around with lately is
[Nixstaller](http://nixstaller.sourceforge.net), an easy-to-use and
easy-to-make "install wizard" for POSIX systems. My personal use case
is very specific, and nixstaller has excellent documentation, so this
post is less about how to use nixstaller and more about how I use
nixstaller.

You may have seen nixstaller before, whether you know it or not. It's
a GUI installer (although it also has a method of running within a
shell) and has been used as the "install wizard" for several
high-profile games; in fact, probably *most* high-profile games prior
to Steam-on-Linux. If you ever bought a game from the Humble Bundle
before they turned into Just Another Steam Key Reseller, then you
probably installed something with nixstaller.

Generally speaking, I personally use nixstaller in three different
cases:

*   Local bundled install: I have a collection of deb, rpm, or slack
    packages that need to be installed by instructors who have an admin
    password but do not have an internet connection.
*   Universal install: I have an application that I want to distribute
    but I do not know the technical capabilities of the audience.
*   No Other Choice: there is an application that distributes itself in
    a way designed for system administrators to re-package, not as an
    installable application by inexperienced end users.

It's important to note that I am in no way implying that I use
nixstaller for the mythical "grandparent" user who is "too dumb" to be
taught new tricks, because I do not believe that this mythical user
exists. However, I do believe that there are users out there (because
I deal with them daily) who have no experience with Linux, no internet
connection, and really cannot be expected to learn how to do manual
installs of a sequence of dependencies and applications during the two
hours a week that they are on a Linux machine. There are more
important things to teach them. And besides that, by rolling my own
controlled installation tool, I ensure success every time, while
leaving it up to a user risks a variety of failures.

With that in mind, here is how I use nixstaller.


## Download Nixstaller

The first step is to download nixstaller from its homepage.

Curiously, I do not tend to install nixstaller itself. I treat it more
like a [SlackBuild](http://slackbuilds.org) or
[RPMBUILD](http://www.rpm.org/max-rpm/) environment, because
realistically when one builds installers, one tends to work in a
centralised location.

So I usually just download the tar archive of nixstaller rather than
the nixstaller installer of nixstaller (yes, that's actually real).

Unarchive and then cd into the nixstaller directory.

## genproject

Use the local script `genproject.sh` to generate the default project
directory.

    $ ./genproject.sh pkg-foo
    $ cd pkg-foo

Now, here's the thing about nixstaller. I get the sense that it was
originally designed for an old style of installation, which is the
untar-and-distribute method. You see, before RPM came around, a
popular way of distributing code (aside from as pure source code, I
mean) was as a tarball (often with the extension `.tgz`). The cool
thing about tarballs is that they can contain an effective, yet
utterly minimal, mirror of a filesystem. So inside of one of these
installable tarballs were folder structures, such as `/usr/bin` and
`/usr/share` and so on. This meant that if you untarred the tarball and
pointed it at the root of your filesystem, it detected that the same
directories existed, and simply took the files out of the tarball and
placed them into the existing folders on your system; it basically
"over-laid" the tarball contents onto your in-use
filesystem. What had been "installed" in a tarball was now
installed on your system.

Make no mistake: this was ingeniously simple and effective. Slackware
still uses this method today, with the one important addition of
keeping log files for each tarball applied, detailing each and every
file that it had contained. That way, if you wanted to remove an
application, you can do so by pointing an uninstaller script at the
log file ("look in this index and un-install all the files listed in
it; do not remove parent directories if they are left non-empty").

Brilliant or not, this method is generally rare today. Nixstaller can
deal with this method, or it can deal with newer install methods, but
it's important to keep in mind that the most basic default behaviour
of nixstaller is to look at the payload files, extract them to the
install destination, and call it a day. If you are NOT distributing
software this way (and I often am not) then you do need to make
adjustments to circumvent this behaviour. Just keep that in mind as we
go forward, because many of the things I do with nixstaller are to
avoid and add onto the default untar-and-quit behaviour (not because I
dislike this behaviour, but because many of the installers I deal with
happen to not be tarballs).


## Place the Payload Files

The next logical step is to place your payload files into the
appropriate directory. For me, this is invariably `files_all/foo`. They
are placed in `files_all` (as opposed to, say, `files_freebsd_all` or
`files_x86_64`) because all of the installers I create lately are either
100% targeted for a specific set of known computers (with the same
architecture) or they are being built from source. They are nested into
`files_all/foo` because I generally want the files to be placed, nice and
tidy, into some temporary directory where I can work on them before they
are actually copied to the system. If you are distributing a magical
tarball that extracts itself to the filesystem as a perfectly-fitting
overlay, then you do not need a sub-directory. But that's not the way I
do it.

If you are putting together a more complex installer, you may need to
create new directories, one for each arch, or possibly one for each OS
(Linux and FreeBSD, for example). Again, nixstaller has great
documentation and examples, so you can refer to those to figure out
specific requirements.

## Place UI Design Files

Another neat thing about nixstaller is that you can customise the look
and feel of your install wizard. For me, this means that I can "brand"
the installer so that users see for themselves that this was made
locally, by real people (an important concept to reinforce at the
makerspace where I do this work). There are three types of elements that
you can change:

1.  An optional intro image, displayed on the initial screen of your
    install wizard. Most be a 300x200 pixel image, in the usual common
    formats (`png`, `jpg`, or `gif`)
2.  A logo, displayed along the top message area within the wizard's
    interface. There is no size limit specified in the nixstaller docs,
    but in practise you probably want your logo to be pretty small, like
    64x64 pixels or so. If it's too large, then the title area at the
    top gets really big and makes your installer interface unnecessarily
    huge.
3.  An icon, displayed in the window decoration and task bar. This must
    be an .xpm file. It's presumably best to keep this at a standard
    icon size (256, 128, or 64 pixels). It will be scaled as necessary.

Generate these in [Inkscape](http://inkscape.org) or
[GIMP](http://gimp.org) and then place them in the `files_extra`
directory, or just skip this step to use the defaults (which are quite
nice, themselves).

### Welcome Screen

You can put text into a file called welcome and its contents will be
displayed in the intro screen of the wizard. If you have defined an
intro image, then the message is displayed to the right of your intro
picture, which can get a little clunky, since the default word-wrap
seems to allow for a horizontal scroll bar. which for a simple message
like "Welcome to the Foo Installer! Click NEXT to continue." seems a
little over-significant. So I keep the welcome message short and simple;
I let the user know what they are about to install, and who made the
installer / who to contact if there are problems.

## Modify config.lua

The `config.lua` file adjusts the look and mode of the installer itself.
None of this gets used during the actual install process, it just sets
up how and what the install wizard is going to present to the user.

There is a wealth of options available, but the default options are all
I use for my work at a local "maker space".

- **cfg.mode = "attended"** the point of this installer, to me, is
    that it is a familiar install wizard with clear instructions on
    how to proceed, and confirmation of success. So I generally write
    attended installers. I am assuming, at least for my current
    audience, that if they know enough to execute the installer
    non-interactively, then they do not need an installer. This is
    only my personal use of nixstaller; in fact, nixstaller has great
    options for **unattended** installation, so feel free to
    incorporate that into your installers if you need it. Frankly,
    sometimes when I'm using my own nixstaller wizards on a computer
    on site, I do wish I made them hybrid, just because the attended
    mode is *so* interactive. So think about whether you want "power
    user" options or not, and choose wisely.
-   **cfg.appname** I have a script that sets this to the name of the
    application I am building the installer for (I do this by parsing
    the basename of the current directory, which I set as pkg-foo). The
    only place this string shows up are pop-up dialogue boxes telling
    the user that "new software" will be or has been installed.
-   **cfg.archivetype = "lzma"** specifies the type of archive you want
    nixstaller to create when it bundles everything up as a
    re-distributable, double-clickable install file. This does *not*
    refer to the type of compression your payloads use.
-   **cfg.targetos = {"linux" }** this is the result of the uname command on
    a system. For me, I am only doing installers for Linux so that is
    what I use. In the event that I am doing an installer for Linux,
    FreeBSD, OpenBSD, NetBSD, and sunos, I would add those strings into
    the list.
-   **cfg.targetarch = nil** I keep this at **nil** because I am only ever
    building for one architecture. For more complex examples, just look
    at the sample installers bundled with nixstaller.
-   **cfg.frontends = { "gtk", "fltk", "ncurses" }** I include all the
    possible frontends for the installer.
-   **cfg.defaultlang = "english"**
-   **cfg.languages = { "english", "dutch", "lithuanian", "bulgarian" }** I
    don't need them, but I include all available translations because it
    just doesn't add enough bloat to bother leaving them out.
-   **cfg.autolang = true** Let the installer choose the best language.
-   **cfg.intropic = "intropic_300x200.png"** sets the name of the intro
    pic to my custom file. Note that its full path is not necessary; its
    root is automatically set to **files_extra**
-   **cfg.logo = "tux.png"** the location of the logo file, from
    **files_extra**
-   **cfg.appicon = "tux.xpm"** the location of the icon file, from
    **files_extra**

There are heaps more options, so if you need more flexibility or more
features, see the [nixstaller
docs](http://nixstaller.sourceforge.net/viewpage.php?page_id=3).


## Modify run.lua

Everything up to this point has been sublimely simplistic. This file is
where all the action happens, though, and what needs to be done here
depends entirely upon what your payloads are made of. This is a really
really *really* flexible script, so there just isn't a generic
all-encompassing explanation of what needs to be in it.

I'll step through the defaults and pseudo-defaults, explaining what
each function does and how they may be used. But I am leaving a LOT
out. Nixstaller can be very cool, very amazing, so whatever your
payload may be, you should refer to the docs and find out how to best
handle your source files.

-   **function Init()** As the name suggests, this function is run once when
    the installer is launched.

    Common uses for it are:

    - **install.destdir** This is a little tricky, because the name is
      **misleading**. It reads like it sets the location to which
      files are going to be installed. But actually, it sets the
      root path to which files are going to be *extracted* (it
      should be called `install.start` or `tmp` or something, but it isn't).

      If your payload is a **.tgz** that contains a miniature filesystem
      structure that is meant to be extracted and sprinkled over your
      filesystem, this could be exactly what you are looking for
      (although actually you probably want to investigate nixstaller's
      ability to register the components with the system's package
      manager instead).

      More commonly, I think, this variable should be set to the
      location to which you want your source files to be placed so
      that you can then configure and install them programmatically.
      The only time you actually want **install.destdir** to be set to,
      for instance, `/` or `/usr` is if you really do intend to have your
      installer to do a `tar xf foo-x.x.tgz -C install.destdir`

    - **install.screenlist** is a list of the screens you want displayed
      in the wizard. The standard set is **WelcomeScreen**, **LicenseScreen**,
      **InstallScreen**, **FinishScreen** but I omit the license since
      everything I deal with is open source, and it's either me or an
      instructor doing the install and we both agree to all
      [OSI](http://opensource.org) and FSF -approved licenses.

      You can also create your own screens, and customise widgets and
      request further information but I don't do that in this use
      case.

   -  **function Install()** gets run when the **InstallScreen** appears.
      This is the heavy lifter; everything important happens here.

      The first thing you want to do is execute the **install.extractfiles()**
      method (to extract your source files from the nixstaller archive and
      place them into your filesystem at **install.destdir**) and then change
      to the directory where your source files are located with
      **os.chdir(install.destdir/foo)** (assuming you have placed, as I do,
      your payloads in a dedicated dir).

      Now you are "in" a directory containing all of your source packages.
      The question is, what do you need to do with them? Do you need to
      compile them from source? do you need to use the system's package
      manager to install them? do you need to extract them, patch them,
      and then distribute over the system? or something entirely
      different?

      It's all up to you. This is basically your opportunity to run either
      built-in helper methods, or completely arbitrary code. Helper code
      exists for compiling from source, and for creating a native package
      so that a user can easily uninstall the app with their normal system
      tools. So far I have not needed any of that, but I have used a
      method to detect whether or not we need to grab an admin password:

          os.chmod("doinst.sh", 700)

          install.setstatus("Installing files")

          if (os.writeperm("/usr") == false) then
             install.executeasroot("./doinst.sh")

          else 
             install.execute("./doinst.sh")

      In that example, the doinst.sh command is a custom shell script that
      I run to perform whatever install sequence I happen to be doing for
      a given application. In the same way, you can run arbitrary code;
      just place a shell script that performs whatever install action you
      require in the directory along with your payloads and tell
      nixstaller to execute it.

  -   **function Finish(err)** is the final default function. You can use it
      to do final clean-up tasks, or auto-launch the application that has
      just been installed, or whatever else you might imagine for the
      final step. Because this receives the value of err when being
      called, it only runs upon success of the installation. If there was
      an error, this step will not run.

My default run.lua file typically looks like this:

    function Init() 
       install.destdir = "/tmp" 
       install.screenlist = { WelcomeScreen, InstallScreen, FinishScreen } 
     end

    function Install()
      install.extractfiles()
      os.chdir(install.destdir/foo)

      os.chmod("doinst.sh", 700)

      install.setstatus("Installing files")
       if (os.writeperm("/usr") == false) then
         install.executeasroot("./doinst.sh")
       else
         install.execute("./doinst.sh")
    end

    end

    function Finish(err)

    end

But, again, that's just how I do it for the applications that I need
to have installers for. Yours Will Be Different!


## Run geninstall.sh

Assuming everything has been filled in, created, or populated as needed,
you can now wrap it all up in one magical installable bundle. Back out
in the main nixstaller directory, run the `geninstall.sh`.

    $ ./geninstall.sh foo foo.install

This takes the `pkg-foo` directory and bundles it up as a self-contained,
executable package containing everything you a user needs to run the
nixstaller UI, and to install the software you are distributing. How do
they run it? probably some really complex shell command with lots of
fancy switches and args, right?

Well, no. They double-click it.

No, seriously. Give it to a user; one double-click and the install
wizard launches, steps them through the install process (such as it is),
and the deed is done.

On some desktops, the user is presented with a choice: do they want to
run the script in a terminal, or just run the script? First of all, I
do not really understand the distinction; if I click on an executable
script, then execute the script. But still, the choice may exist on
the desktop you use, and the correct option, counter-intuitive though
it may be, is to run it in a terminal. This launches a shell that
uncompresses the nixstaller archive, which in turn launches the
nixstaller UI.

Enjoy.

[EOF]

Made on Free Software.

