# Motherly Advice Against Using Apple




There are too often only two types of opinions in this world: the
fanatically favourable and the fanatically farcical. The problem with
both of those is that they specialise in the extremes. Something is
either God's gift to the known universe, or it is the most evil thing
in the world and is responsible for war, earthquakes, and [traffic accidents] [1].

Well, make no mistake about it: I'm against Apple. This list is a list
of reasons *not* to use Apple. It's not a list pretending to weigh
benefits to liabilities. It's just the liabilities.

Then again, that's all it is. It's just a list of the reasons I find
Apple to be more trouble than it's worth. It's like when your mother
told you to watch out for them big-city women (or men). She didn't
tell you why, she didn't fill your head with horror stories, she just
sat down and warned you.

And what did you do? You ignored her. You threw out her advice and
dated all the wrong people, you had your fun, and then, sure as the
day is long, you got your heart broken.

Your mother told you so.

That's what this list is. Not literally, because I'm nobody's mother,
but it's the most motherly advice I could summon on why you, young
computerist, should avoid Apple.


## Developer

*If you're looking at programming as a hobby or career, these are particularly uninviting aspects to Apple's glossy developer's pearly (or brushed metal? whatever it is these days) gates.*


### The SDK

Computers are programmable. They just are; that's what makes them
computers.

You don't need a special Software Developer Kit to program a computer;
a computer is programmable at its very core. If a company offers you
an SDK, it might be a fancy way of offering you the development
libraries you could get anywhere else, or it may be a form of
gate-keeping; make sure none of the "normal" users are bothered by all
the technical mumbo-jumbo, but then charge extra for the privilege to
write code.

In Apple's case, it's mostly been a form of gate-keeping.
Historically, their SDK did actually cost extra money. If you wanted
to build stuff on the Apple platform, you had to pay Apple. Yes, you
had to pay to do work that would ultimately benefit at least yourself,
possibly other users, and ultimately, in a way, Apple (by way of
supporting its "ecosystem").

This started to give way to a free SDK (because Apple started basing
their own code on open source, and the very technical developers were
cobbling together tools to develop on Apple for $0 external of Apple's
offerings).

But the gatekeeper remains, and for super secret special developer
access, you have to buy special membership. This entitles you to
special pre-releases, beta code, and so on. It makes sense for Apple;
they don't want every comp sci student building early releases of the
OS and releasing it online and giving it a bad reputation. However,
the flip side of this is that unless you have a few thousand dollars
to maintain your club membership each year, then as a developer you
are playing catch-up instead of developing on the cutting edge. That
means that when the next version of the OS is released, your
application could possibly break for all your users until you get a
copy of it yourself, can investigate the changes, identify where the
breakage is happening, and code around it. Not easy, not fun, and it
reflects very poorly on your application's reliability.

**Verdict:** Not worth it. Instead of chasing the cutting edge, use
open source tools so you can *be* the cutting edge.


### An SDK the Size of an OS

The Apple SDK, an assortment of open source and closed source
development tools, is huge. It always has been, and I guess in a
sense, the more the better, right?

Well, maybe not.

After all, if you're a dumb high school kid and all you want to do is
compile a cool new 2MB-large GameBoy emulator so you can impress your
friends, then your compiler download shouldn't be 5120 *times* larger
than the thing you're trying to compile. It can be 10x as large, or
100 times as large. But not 5000x as large.

![This is too much.](images/macXcodeCopy.png)

OK, so modularity is nice.

But there's something more to it, beyond just inconvenience. It's the
impression that in order to program your environment, you need a whole
OS on top of your OS; a whole new set of tools, new libraries and new
packages to open and learn. You're not downloading a development kit,
you're downloading a way of thinking.

Consider this; you can start writing applications with nothing more
than [GCC](https://gcc.gnu.org/) and a [good text
editor](https://www.gnu.org/software/emacs/), all in less than 1 GB
(yes, that small even on Mac).

**Verdict:** Not worth it. Use open source dev tools that remain
autonomous and portable and don't require high speed internet to
acquire.


### What They Don't Tell You About Cocoa

(And what they don't tell you they don't tell you about Cocoa.)

People get really excited about the *look* of the Mac OS interface.

From a programming perspective, though, the Mac interface is a little
like one of those plastic bins you get to store Christmas decorations
in. You know the box I mean; you put all the stuff into it, then you
work it, Tetris-like, into the stack of storage in the hall closet,
and it becomes an autonomous object. It is The Christmas Box. You can
see into it, so you know you have a Christmas ornament, but you cannot
access that ornament without removing the bin from its stack, opening
it up, rummaging around, and so on.

The Mac interface, technically named `Cocoa`, is like that because the
Apple company builds it in private, and then they put it into the
stack, but they lay it out very cleanly; programmers can look at an
object in the container, and may even be able to communicate with
it in some way, but it's always inside that container, and there's no
direct access. That means that if I want to do something with an
object in the container, I might have to jump through several
programming hoops to do it, even if on some other platform it would
take a line of code. That's if you're lucky; in some cases, it might
be that an object you need to use is just buried too deep in the
container and has not been made available to you.

To be clear, my warning about Cocoa isn't *just* that it's closed
source, it's that it's a contained layer *and* closed source.

Linux's X server is by comparison not a container at all; you can
trigger events on your desktop with terminal commands as easily (or
easier) than with mouse clicks. It's insanely powerful, but you could
argue that it has some drawbacks (or not). It being open source is a
huge benefit. For something in between those two extremes, look at
Android; it's implemented a similar model to Mac OS, but instead of
using closed source, they use the open source Java toolkit. Suddenly
the "contained" graphical layer is a lot less threatening because you
can make calls to anything and everything within it, and it works just
as you'd expect.

**Verdict:** Not worth it; introduces a partial API where no API
should be necessary. I'll take an abstraction layer, but I want full
access to the code for when the abstraction isn't enough.


## User

*If you're looking at Mac OS as "just" a user, these are reasons to avoid it.*

### Consumer Culture

For a long time, Apple marketing positioned Mac OS as the alternative
computing platform, as opposed to Windows. They glossed over the
price tag, which is sort of fair (more on that later, though), since
computers do cost money, but they strongly suggest (or in some cases,
blatantly assert) that Apple users are charming young upstarts with
new, bold ideas, bucking oppressive conventions.

The problem is that the "rebelliousness" embodied by Apple is entirely
purchased. You buy your way into Apple usage; you can't be a "proper"
individualist by digging a computer out of the rubbish and installing
a free and open source OS on it. That's not individualism, that's not
"revolutionary". No, you have to pay for the Independent and Different
Thinker upgrade.

In fact, all accomplishments in the Apple universe are based on how
thoroughly you've bought into Apple. The Apple website used to (maybe
still does?) profile artists who do things *using Apple
products*. Again, fair enough, since that's the point of the website,
but it creates a sense that we're all taking part in Apple's world,
rather than celebrating what their marketing campaigns claimed: the
individual.

Beyond psychology, though, there's this problem Apple has related to
how it treats its developer community that actually makes it
considerably difficult for average computerists to program serious
applications for Mac. Apple, in its frenzy to keep everything secret,
doesn't do a great job in communicating with the cottage industries
cowering in its shadow, so when a new release happens, third party
applications that you may have come to rely upon sometimes break
badly. Sure, that can happen any where, but too often have Mac
developers closed up shop due to the cost of keeping up with Apple's
almost subversive rate of surprise changes.

And it's not just third party developers. Apple does it to its own
customers, directly. I grew up with Apple before I switched to Linux,
and I can't even count how many flagship Apple products have been
suddenly discontinued. And when they discontinue something, they
discontinue it. I have spent months of my life rummaging through
files, trying to make sure everything has been converted to the new
format before upgrading my computer. And I've spent years lamenting
some of the files that slipped past me, and as a result could not be
opened by me. Yes, Apple ate my data, not through one of those "Act of
God" quirks, like a failed hard drive, but by betraying my trust in
the technology they themselves sold me.

It's as if Apple is challenging the world to just try to use their
products, taunting everyone with applications and file formats that,
just as soon as you get used to them, will be deprecated and locked in
vaults.

To what end? why would Apple do that?

Because they can.

No matter how they abuse their customers, the bulk of them keep buying
whatever new Thing comes down the pipe. And if you know that, why
wouldn't you keep switching things around? Deprecate the old and force
people to buy new; it's the best license enforcement there is.

You can dismiss all of this by shrugging and accepting that Apple
Incorporated is, after all, a corporation. To survive, it has to grow
its paying customer base. I can't and don't really argue with
that. But playing on the self-worth of customers crosses the line
between advertising and snake oil.


**Verdict:** Not worth it. Lying to customers about how a product will
free their minds (and other hippy fairy tales) is disingenuous and
dishonest, and making computer usage a moving target to ensure
purchases is disrespectful at best.


### All Your Files Are Belong To Us

Apple loves to talk about how it uses open source. Debates about how
much they use it and how much they actually give back are pretty easy
to find online, but probably the best written article on the subject
is [Was Apple the First Major Open Source
Company?](http://www.zdnet.com/article/apple-was-the-first-major-open-source-company/)
by Steven J. Vaughan-Nichols.

Despite how you feel about Apple's attitude regarding open source or
their refusal to play well with open source contributors, to a user
all that really matters is whether or not their data is safe: if a
user puts their data on a thumbdrive and takes it to work or school,
can the data then be accessed by whatever OS they end up on?  If a
user gets a new computer and can't afford a Mac, can the stuff they've
worked so hard at for the past five years be converted to something
usable on the new machine? Should the application that created the
data become deprecated, will the user still be able to access the
data?

All too often, the answer to this is negative. Apple does use *some*
open formats, but since so many of its applications are closed source,
so are many of the file formats that do not have an immediate, obvious
need to inter-operate with other systems or devices. What I'm saying is
that Apple uses standards when it would be blatantly stupid not to,
and even then, they tend to favour formats that are $0 to use but
still maintain a list of restrictions.

I used to tell everyone to use the [Universal Disk
Format](http://www.osta.org/specs/), but of course Apple's done away
with support for even that (at least, for now; who can say? maybe
they'll restore that feature by the time you read this, or maybe they
won't).

I've been paid a lot of money to de-tangle various projects from Apple
file formats. It's grueling, manual labour that I only did for the
money, and it happened without any help from Apple or their supposed
dedication to openness.


**Verdict:** Make no mistake: Apple doesn't care about your
entitlement to access the data you yourself produce with their
tools. They just care that you keep using their products.


### Bloat OS

I can download a complete OS, with not just one but *two*
full-featured desktop environments (think the **Finder**, but
generally more robust, and more than one to choose from), a whole set
of applications (text editors, photo editor, web browser, email
client, image viewer, fonts, media players, and so on), **plus**
development tools, some silly games, more file sharing protocol
integration than you can shake a stick at, and a heck of a lot
more. It fits on a 4.7G DVD, and unpacks to about 8GB on drive.

By contrast, the latest (as of this writing) Mac OS disc image is 6GB
of download, with a bare minimum of 8GB drive space required. Not too
bad, right? Hang on, Mac OS hasn't got even a third of the actual
content; it has one text editor, one desktop, no serious photo editor,
one media player, and no options for development. Fact is, you're
likely to replace as many of the default tools with better
alternatives, and if you want dev tools then you need a separate 10G
download.

So basically, twice as much data with a third of the content.

**Verdict:** Not worth it. Apple will bowl you over with fancy looking
props that, when put to the test, flop.


### HFS

One of my very favourite topics: the HFS+ filesystem. I have a long
history with this filesystem, and have written at length about my
experiences with it.

Filesystems are difficult to test, because there are several variables
involved, so I can't comfortably say that HFS+ is a poor quality
filesystem. I will say that it is the filesystem that I trust the
least, but I can't back that up with raw data.

What I can say confidently is that at the very foundation of my
concern about how Apple regards the sanctity of its users' data is the
system that it uses to save that data. And if you can't even get the
source code to the filesystem that a company forces you to commit your
data to, what does that say about their attitude?

An analogy would be this: you are sent on an important trip but cannot
take any of your possessions with you. So you pack them up and take
them to someone for safe keeping. They promise you they'll keep it
safe, but flatly refuse to tell you where they are going to keep
it. But they swear that any time you want to come round and see
something, they'll fetch it for you. When you're ready to re-claim
everything, they'll give it back to you, but only to you (you may not
send an envoy).

Would that make you feel comfortable? what if this person dies or goes
missing while you're away? you may never be able to find your stuff
again. What if you have to stay abroad longer than anticipated, and
want to have your stuff sent to you? What are they *doing* with your
stuff in the meantime, any way??

That's the HFS filesystem. It stores your data, but you can't read
that data from any other computer (unless you go through Mac, for
example, via file sharing). If something happens to your Mac and you
don't have another Mac nearby to interpret, you won't be able to
rescue your data. If something happens to HFS itself, no one but Apple
has its source code, so quite possibly you'll never see that data
again.

Luckily, resourceful *open source developers* (of course) have been
reverse engineering HFS for years and can mostly get around its
blockades *in spite of Apple's refusal to provide data on how to do
that, or even permission to do it at all*, but the fact that this is
necessary is not just disconcerting, it's pretty disgusting. This is
how Apple feels about its users and their data. Your family photos,
your artwork, your schoolwork, your music: Apple promises to keep it
safe, just so long as you always ask Apple nicely for it back.

Not only does Apple keep HFS+ from being used by other platforms, but
they refuse to let you choose anything but HFS+. They have provided a
Microsoft closed filesystem for thumbdrives and similar, but that's
all. The system itself still must be HFS+, and that's Apple-only,
closed source and exclusive.


**Verdict:** Not worth it. My data belongs to me, and I should be able
to store it as I please, and to transport it seamlessly between
devices.


### Price Tag

I still remember the sinking feeling I got when I had to go buy my
very first Mac computer with my own money. I was getting the previous
year's model, with an employee discount, from the place where I
worked, and I still couldn't actually afford it. Admittedly, I needed
a fairly robust one due to being in film school at the time, but it
was shockingly expensive. The software that I needed on top of that
was actually *more* expensive than the computer itself.

It was bad enough for me to entertain the idea of buying a Microsoft
PC. That doesn't sound that crazy to most people, but for me,
especially at the time, it would have been a life-changing event. I
didn't do it, but the fact that I was considering it meant that I was
truly desperate.

And then there was the time when the mouse broke. I am not kidding
when I say that I did not know that I could use a non-Apple mouse, so
I went and paid for a new mouse and skipped several meals that week.

This, probably, is why I resent Apple so deeply, the way most people
resent Microsoft. It's not the pompous attitude, the slick marketing,
the hoards of devoted fans, the aggressive attitude toward its users,
the flagrant disregard for user data and application stability, the
deliberate inability to inter-operate with other systems; it's the fact
that Apple kicks you when you're down. There's just no quarter given;
according to Apple, it's Apple or nothing. That's not just by
implication, it's the way they design their computers and
protocols. You may not choose an alternative, because nothing you're
used to on Apple will work on the alternatives. You have to pay, even
if it means going into debt.

As it turns out, Apple overcharges for what they sell. It's not really
something you can "prove" because obviously when you buy an Apple
product, you are indeed paying for a metal case or a specific design
choice. But in terms of spec-for-spec, there's no arguing. I can
easily build a Linux system that outperforms (cycle-for-cycle) the
latest top-of-the-line Mac for a third or half the price (depending on
how we equate things).

And you know what? if Apple came out with a budget computer today, I
still wouldn't buy it or advise others to do so, even ignoring every
other reason to avoid them. Why not? because it's been too
long. They've over-charged people, driven people to buy into their
illusion on the pretense that they've got not choice because it's such
a quality product. This is a business I do not want to do business with.

**Verdict:** Nowhere near worth it. There's a reason studios and
production houses don't buy Apple hardware to run their Linux
workstations; it's just overpriced.



## Ideologist

Apple markets itself as a "green" (silly term) computer company,
meaning they are ecologically responsible. They go to great length to
make this known on their site, and it usually has that "responsible
consumer" boost to it; you feel better about paying all the money
you're spending because, while you may be over-paying, at least you're
paying *the right* company.

But there's more to this than meets the eye, I think. Apple is being a
little dishonest in their claims, even though they may be reporting
the actual production properly. You see, you cannot honestly *imply*
that you're the most ecologically friendly computer company out there
because you don't use mercury in your computers if your entire
business model hinges on forcing customers to purchase new devices
every year or two; yes you use no mercury, but you're campaigning for
people to throw out two phones and a laptop every three years.

What if Apple slowed production? what if they let their products last
for as long as they're made to last?

It can be done. Debian 8 Linux in 2016 supported iBooks from 2004
(possibly earlier, I haven't tried lately). That's 12 years ago; you
can have a modern OS on a 12 year old computer. Yes, it's slow, and if
you're producing HD images then you probably won't use a 2004 computer
that maxes out at 512 MB RAM (or whatever it is). But if you're
writing blog posts or just learning programming or networking, you may
well use a 2004 computer, quite happily and securely.

This isn't just Apple at fault, here; the computer and tech industry
at large is extremely destructive and wasteful. But Apple is the
loudest about how different they are from this trend. Apple is the one
claiming to be progressive.

So it's disappointing that it turns out to be a lie.

Or is it, really, disappointing?

**Verdict:** Not worth it. Apple encourages waste and disposal
possibly more so than any other major computer manufacturer, and
blatantly lies about how "eco friendly" they are.


## The Alternative

Apple is a company that has China make it some very nice computer
parts, including custom bodies. Some people find its OS attractive.

The truth is, though, that Apple is dangerous to you and your data. Avoid it.

I'm not going to say there's a "similar" experience elsewhere. There's
not; Apple is very distinct. And when you use it, it feels like you're
part of a special club, because you didn't grow up with it, or there
aren't that many other Apples in the café, or you've seen them at tech
conferences, or they just feel cool. But in the end, the cost is
greater than you realise.

Discover liberated computing with Linux. It'll get you using computers
like a pro.  It'll get you creating.

And most importantly, it'll get you thinking.


[1]: http://forrestcrow.proboards.com/thread/3379?page=1        "traffic accidents"


[EOF]

Made on Free Software.

