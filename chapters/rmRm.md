# rm 'rm'




OK, don't really go and `rm` the `rm` command; that's a
horrible idea, but the sentiment is sincere. You see, I take issue
with the `rm` command. I don't like it as a command; I've never ever
come across an intermediate-to-advanced user who claims to never have
accidentally lost data because of `rm`. In terms of a command line
interface, I sincerely believe that `rm` is the single-most greatest
enemy of the user (with the `find` option `-delete` being a less
common runner-up).

So yes, I hate the `rm` command. Why? Well...

...not just because it's so dangerous...

...not just because it's almost always included in those "my first 10
unix commands" children's books...

...and not even just because it erases data and offers no "undo"
function...

No, the reason I hate it is because it does a *poor job of doing even
what it claims to do*.

## &#34;When I Delete a File, I Want it Deleted&#34;

One argument I hear a lot when I complain about `rm` is that "I'm an
advanced user. When I tell my computer to delete a file, I mean DELETE
the file."

Lies, all lies&#33;

If you wanted a file well and truly deleted, then you'd use
`shred`. You'd delete a file *and all traces*. But you don't. Nobody
does. Because nobody wants to have to make the mistake of shredding
important data and having to face the reality that it&#39;s *gone*.

You see, in a bizarre twist of complaint-reversal, my issue with
`rm` can also be expressed as both:

* I hate `rm` because it erases data and offers no recourse.

* I hate `rm` because it does not erase data effectively. 

It looks contradictory, but actually they are two sides of the same
wonky coin. Let's say we use `rm`. Yay the file's gone. Oh wait,
no...I needed that file. Panic panic panic. OK, OK I found a tool
called <a href="http://www.cgsecurity.org/wiki/PhotoRec"
target="_blank">PhotoRec</a> and another one called <a
href="http://slackbuilds.org/repository/14.1/system/scalpel/"
target="_blank">Scalpel</a>, and these will scan the hard drive for
"erased" files, identify them by headers, and rescue the thing I
"erased".

Well, heck.

To me that sounds a lot like a program that was meant to *erase*
something but failed to properly do so.

Some people fall back on that shortcoming when they make a mistake,
but that doesn't make the tool a better tool when it gets used such
that no one minds that it fails, nor the user a better user when some
other command happens to be able to rescue data accidentally erased.

In fact, I should file a bug about that with `rm` because if the
goal is to erase data from a drive, then it's frequently failing.

In fewer words: I am calling shenanigans.

If you really want a command to erase data, then man or woman up and alias `rm` to something serious about erasing data. Otherwise, your safety net doth betrayeth you.


## &#34;I&#39;ve Never Had a Problem with It&#34;

This is another excuse I hear a lot. 

I believe the truth in this statement, because there are only a few
times `rm` really screws people over:

* You are a new user and do not understand how to use `rm` (or, more likely, paths or wildcards).

* You are an experienced user and make a stupid mistake when composing a command involving `rm`.

Nine times out of ten, these things don't happen. Usually, you're a
new user, you carefully delete a file, and you move on. Or you're an
experienced user, and you skillfully construct a `for loop` and `rm` a
few directories, and then you move on. Not a problem.

Nine times out of ten.

In order for you to hit that tenth time, statistically speaking, you
have to use the unix terminal enough that you mess something up
involving `rm`. There are users out there who use the unix terminal
pretty infrequently, or maybe they use it frequently, but they don't
have the occasion to use `rm` all that often. A great example of the
former are visual effects artists; they use Linux for the special
effects art that they make, but they only use the terminal (if at all;
depends on the studio's workflow) to launch an application or send a
job to the render farm.  Sys admins are often a good example of the
latter; they use Linux to maintain servers and network infrastructure,
but they don't usually go around deleting files. They have scripts
that monitor disk space, and when they *do* delete files, it's just to
get rid of that one tarball they downloaded and can now ditch, not
something they repeat often.

So the dangers of `rm` are, basically, mitigated through sheer dis-use.

The problem really surfaces at its worst for people who use the unix
shell as a primary operating environment. You know the types; they're
the ones whose answer to "what's your favourite file manager?" is
`bash`. For them (I mean "us"), it's not a matter of *if*, but a
matter of *when*.


## A Sane Replacement

Why do we think it's acceptable for users to do this? Why are we not
only handing users `rm`, but actively encouraging them to use a tool
that does a bad job at erasing data but a great job at erecting a
barrier between the user and the [third-party] **undo** function?

> Let's face it: the responsible thing for `rm` to do would be to
> integrate something like `scalpel`, because it's already doing a
> poor job at performing the job for which it claims to exist, so if
> it's going to leave the remnants of an "erased" file lying around,
> it may as well provide an option to un-"erase" the file it didn't
> manage to erase.

I'm not going to write about it extensively here, but there is a
better way, and it's to stop using the `rm` command. Instead of doing
a bad job at erasing a file, do a good job at moving it to a temporary
holding area. We could call this area...oh, I don't know, a "trash"
bin (like when you throw a piece of paper in the trash, but do not
immediately incinerate it). It's a clumsy analogy that probably will
never catch on, but it's a starting point, right?

To this end, I developed a very simple `trash` command. You can
download and install it from <a href="http://slackermedia.ml/trashy"
target="_blank">slackermedia.ml&#47;trashy</a>. It's simple, it's
&#91;currently&#93; written in BASH so it's easy to reverse engineer
and modify, and it works. It works with wildcards, it works with
**find**, it works in loops, it works on files and directories. It
moves your stuff to a temporary directory (by default, it uses the
trash bin that your desktop uses) and only invokes `rm` when you tell
it explicitly to `--empty` the trash (it does not use `shred` by
design, as its goal is to maintain levels of **undo**, not zero out
data, although with the magic of environment variables, you could swap
`rm` for `shred` in `trashy`).

If your inner `rm`-elitist shudders at the thought of moving files
instead of pseudo-erasing them, then feel free to set up a cron job
that empties `trash` every hour or so. At least then you get a little
bit of buffer time before your "erased" files are only un-moved by way
of a third party application like `scalpel` or `PhotoRec`. But really,
you should be using `shred`, because I'm officially calling your
bluff.

If you use the shell on a daily basis for everyday computing tasks,
*use trashy*.

[EOF]

Made on Free Software.

