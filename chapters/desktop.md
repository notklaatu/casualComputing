# Dot Desktop Files and No-Install Integration

Any given operating system has at least four different ways to install
applications:

1. Download an application from the internet, launch an install wizard
2. Download an application from a software repository ("app store" or "software center")
3. Compile an application from source code
4. Don't install; just download and double-click on the icon to use the application

(I should mention here that most people used to accuse Linux of being
over-complicated and confusing because it had so many different ways
to install an application. I'll just point out politely that the above
four possible avenues apply to all operating systems, with the only
actual change being the addition of "app stores". Glad everyone else
finally came around.)

Anyway, the method I want to discuss here is the last one in the list:
the one where you don't really install at all, you just download a
*thing* and start the application by double-clicking the icon. You'll
see this with applications like [Processing](https://processing.org/)
and [Godot](https://godotengine.org/download), for instance.

There are a few technologies that make that possible; either the
*thing* you downloaded is an `executable binary` (meaning it's been
compiled into one big lump of machine code that your computer executes
on command) or it's a bundle of some type (AppImage, FlatPak, or
Snap).

It's fine to use these bundles or binaries, but they don't always feel
integrated with your system. On Linux, we tend to like our systems to
be tidy, and the technology working behind the scenes encourages
that.

To get a self-contained app acting like it's a properly
installed app, you can perform an *ad hoc* install manually. It's easy
to do, and it's keeps your system organised and makes it easy for
Linux to understand what you really want it to do with the thing you
just launched.

It's really easy, and quick once you're familiar with the process.

## Wait, why are you doing this?

First of all, you should check to see if the application you're about
to not install is available in your software repository. You might be
wasting your time; if it's already in your software repository, then
you can install it with one command (or a few clicks, if you prefer),
and it will be integrated with your system without any further effort.

But maybe the version in the repository is too old for your liking, or
maybe it's just not available at all. Assuming that's the case, let's
proceed:


## 1. Gather assets

For this to work best, you need two things: the application (which you
presumably have downloaded by this point), and an icon for your
application. You can usually find the official icon for your
application by searching for one online, or else by looking through
what you downloaded when you got the application. Pretty much any kind
of image file will do. It can be a JPG or a PNG or a BMP (or TIFF or
TARGA or whatever). Size (resolution) isn't a huge issue; you know how
big your icons usually are, so shoot for whatever seems reasonable to
you.


## 2. Put your assets in common location

Now you should put both the application file and its tag-along icon in
*some* system-level location. It's a very common convention to
"install" your application and its icon to `/opt`.

Personally, I put the application in `/usr/local/bin` and the icon in
`/usr/local/share/pixmaps` (a directory that may not exist yet),
because that makes logical sense to me: assets that are "local" to my
install of Linux go in standard locations under
`/usr/local`. Admittedly, that's sys-admin stuff that you might not
care about, and the simpler convention is to just dump everything in
`/opt`.

Truth is, it doesn't actually matter where you "install" the
application and its icon to. You can even just install it into your
own home folder (perhaps in a directory called `bin` for "binaries" or
`apps` for "applications"). The important things:

* be consistent (for your own sanity)

* use a system-wide location (like `/opt` or `/usr/local`) if you have
  other user accounts on the computer that will want to use the
  application.


## 3. Create a dot-desktop file

If you are using the KDE desktop, you can edit the application menu
directly to add your "installed" app. Just right-click on the K-menu
icon and choose **Edit Menu** or **Edit Applications** and you can add
your application. With other desktops, look for a menu editor
application.

Whatever you use, menu edits only work for you; other user accounts on
your system won't see your entry, so if you need it to be system-wide,
you will need to do it by hand.

A `.desktop` file is the universal, system-wide filetype that acts as a
launcher for Linux applications. They are really cool, and you should
learn about them and use them!

They're easy to make; all you need is a text editor. A minimal example
for a pretend application called `fooview`:

    [Desktop Entry]
    Type=Application
    Name=Foo Viewer
    Comment=View Foo and PNG files
    TryExec=/usr/local/bin/fooview
    Exec=/usr/local/bin/fooview %F
    Icon=/usr/share/pixmaps/fooview.png
    MimeType=image/x-foo;image/x-png;
    Categories=Graphics;2DGraphics;RasterGraphics;Viewer;

I think it's pretty intuitive, aside from the `MimeType` and `Category` entries.

The **MimeType** entry defines what kind of files this application is
applicable to. If you're installing the Arduino programming
environment, and you know that it opens *text files* with the file
extension `.ido`, then look up online what the mimetype of a text file
is, and modify it to suit: `text/x-ido` (that is, a plain text type,
with the custom extension of .ido).

To find out what type of file an application produces, open it up and
save a test file. Then use the `file` command on that test file. For
example:

    $ file ~/test.file
    test.blah: ASCII text

OK, so it's plain text, with the extension `.blah`. Look up the
MimeType for plain text, and construct a MimeType for it.

If you get it wrong, it's not a big deal. Your application will still
work, it just might not be one of the suggested apps to open a file if
you right-click on a file and choose **Open With...**. Not a
deal-breaker, so don't sweat this one too much.

The **Categories** entry is used to determine what category in an
application menu your application shows up in. Since different
desktops have different ways of categorizing things, this is not an
exact science. Just look at your application menu and see where you
think this entry should be, and use that.

To install a `.desktop` file, just copy it to
`/usr/share/applications` (this makes it available system-wide, to all
user accounts). If you don't need it to be system-wide, you may copy
the file to `~/.local/share/applications` instead.


## That's it

That's all. Your stand-alone application is now "installed". You can add it to docks, create launchers, and treat it pretty much the same as any other application. It's fully integrated into your environment now.

Three easy steps.

Enjoy!

[EOF]

Made on Free Software.


