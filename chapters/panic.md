# Don't Panic

There's a disconcerting tendency I've noticed when my people try,
unsuccessfully, to do something multimedia-related on Linux: they
panic.

I'm not talking about "panic" in the sense that I have a low threshold
for helplessness, I'm talking about otherwise intelligent people
acting like there's no known solution to a problem as small as audio
not being audible through speakers...and failing to notice that they
left their headphones plugged in.

I say this is a trend because I've witnessed it several times among
"normal" people who use Linux on a daily basis; co-workers using Linux
as their workstation, professors teaching computer science, geeks who
use several operating systems. And it does seem to be fairly specific
to multimedia activities, for some reason; they're happy to
troubleshoot why a shell variable isn't updating correctly, or why a
library isn't compiling, and any number of issues that would send the
everyday user running in fear, but the minute they open a web browser
and a youtube video fails to play, they toss the mouse aside with a
sigh, saying under their breath, "Oh, Linux..."

I witness this pretty much on a weekly basis at this point, mostly
because I work in an industry that uses Linux on a massive scale. I
have personally witnessed perfectly intelligent users:

* blame poor youtube frame rates on Linux, switch to a Mac or Windows
  box and discover that actually, it's just the network. Even funnier?
  on more than one occasion, the Mac or Windows boxes refuse to play the
  audio (true story); once, the user just settled for the same poor
  frame rate and no sound rather than switch back to Linux to have the
  same frame rate *with* audio, either because he was too embarrassed to
  admit he was wrong or he was just pressed for time.

* give up on playing music on Linux because of "audio issues" which
  turned out to be headphones left plugged in.

* give up on playing music on Linux because they left Kmix muted.

* bemoan Linux web video compatibility due to temporary network issues.

* chat about how difficult it is to edit video on Linux whilst waiting
  for a presentation on editing video on Linux by a professional
  editor.

I assume this is indicative of something.

## Drivers

There's this stupid thing about some hardware manufacturers where they
fail to publish the code required to actually *drive* the hardware. I
could go on and on about this, but complaining'll get us nowhere, but
that's the state of things. Still.

Usually, some very brilliant developers manage, somehow, to work with
that. But there are times when audio drivers fail. When I worked at
Apple for a year, I was shocked to find that this actually happened
during development of Apple computers, too, but the difference is that
when it happens on the vendor side, then the vendor just calls up the
hardware manufacturer and either gets code that works or gets a
programmer on site to fix the issue.

Most people using Linux, or trying to use it, are installing it
themselves, so they haven't got the luxury of just calling up the
designer of the audio chip to get things squared away.

So sometimes, when you're installing Linux, you have to do a little
bit of manual configuration. You have to jiggle the virtual
wires. Historically, that often meant "open alsamixer in a
terminal..." type replies in forums, and now it tends to be "set up a
.asoundrc file..." hacks.

That's something I guess we Linux users will be living with until...

* vendors publish appropriate, preferably standardised, specs for
* their hardware non-technical users accept that Linux can be purchased pre-installed

I prefer the latter option; it feels unjust to earn a reputation for
poor drivers when the obvious answer is pretty much the same to every
other computer problem a member of the general public encounters: buy
a new computer. I'm not saying that's the *right* answer to computer
problems, but I am saying that too often people look at Linux as a
fallback OS for an otherwise useless computer, but complain when they
have to configure it, or don't get the performance of a new computer
just off the shelf. They aren't *wrong*, but they aren't exactly
*right*, either.

All of that aside, the impression remains: if there's a problem on
Linux, blame a driver. If you don't understand drivers, panic.


## Practise

Sometimes there's nothing wrong at all. Sometimes it's just a matter
of remembering the basics of a typical computer interface.

I think the problem here is lack of *latent comfort* (if I may invent
a term) with the UI. You know the sort of comfort I mean: the kind of
comfort that you become aware of when you think "I should adjust the
volume" and then you look up at your volume icon and see that your
mouse is already there adjusting the volume. That kind of non-thinking
"oh yeah...I knew that" comfort.

Lacking that, when a problem arises that can be solved easily but that
only gets done every now again, it comes across as intimidating at
best and insurmountable at worst.

That's the long way of saying that practise makes perfect.


## Stopping the Cliche

No matter what, it seems some clichės just won't go away. I don't know
how they get started; some are marketed very deliberately, others are
coerced into the public's mind. I never saw, for instance, an Apple
Mac ad saying that Mac was "better for artists" specifically, but
somehow that's the reputation it got. I feel like a lot of these get
reinforced by clueless computer sales people at (name your least
favourite big box store), but that might be a bias I developed from
working at computer stores back in school. Still, I can just hear them
explaining the world of home computing to a customer: "Well Windows is
what everyone uses and it's good for businesses, and Macs are for
artists. Oh Linux? no no you don't want that, that's for servers."

Oh, the pain.

Still, the trite definition, the total absence of reason, the lack of
explanations, they stick. When something doesn't play on Linux, the
"obvious" reason is that Linux is an impostor, masquerading as a
desktop. It doesn't *really* belong on a PC, it's for a server. Quick,
switch over to a "real" PC operating system. Why? well, so there's
less panic when something goes wrong.

Yes, we don't switch because it will eliminate problems, we switch
because we feel less uncomfortable when there is a problem.

The answer is actually pretty simple: don't panic. Stop, think about
how computers work, and do some simple troubleshooting. Do it every
time it happens, and before you know it, they aren't problems; it's
just part of the computing experience. Yes, you have to un-mute your
speakers to hear sound, and you have to play a video in a player that
can play the kind of movie file you are trying to watch, and you have
to plug in your headphones to get sound through your headphone.

[EOF]

Made on Free Software.

