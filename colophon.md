# Colophon

As you may expect after reading this book, nothing but the very best
open source software was used to produce it. Since the whole stack is
open source, it's difficult to list *every* open source application I
used, but the ones most applicable are:

* [Slackware Linux](http://slackware.com) is the operating system I
  run (Linux is, technically, just the kernel). According to its
  mission statement, Slackware is the "most Unix-like of all
  Linuxes". With it, I get all the benefits I feel I'd get from unix,
  plus all the benefits of a truly remarkable kernel, drivers,
  libraries, and community.

* You thought your text editor was good? Don't even enter the
  competition until you've tried [GNU
  Emacs](https://www.gnu.org/software/emacs). It is to text editors
  what Linux is to operating systems; total control, total
  customisation, and ultimate, terrible, awe-inspiring power.

* Illustrations were done in [Krita](http://krita.org). I'm
  not a good illustrator but Krita makes it fun to try.

* The cover of this book was designed in
  [Inkscape](http://inkscape.org), and it uses artwork from the
  Creative Commons site openclipart.org; specifically, a computer
  icon uploaded by user `jcartier`.

  Additional art includes:
  
  * http://openclipart.org/detail/183205/pipe
  * https://openclipart.org/detail/175961/file-icons
  * https://openclipart.org/detail/174316/games

* All fonts used are also open source. Specifically, I use Kabel (the
  old official KDE font) and Bazaronite. IBM_Nouveau is the code
  font. There are some other fonts (like the Liberation family) that
  snuck in here and there, but mostly those are what I used.

The workflow for this book revolved largely around
[Pandoc](http://pandoc.org), an amazing little application that parses
text from nearly any format and converts it to nearly any other
format, and even renders and packages it up as EPUB or similar. Aside
from having written a paragraph singing its praises, I am left
speechless. This is one of those tools that you use, and then later
you sit back and think about, and find yourself shaking your head and
mmuttering "This should just *not* be free!"

But of course, it is. And that's the beauty of this whole culture,
isn't it? It's about sharing information, labour, and resources. Small
wonder that I'd write in excess of 60,000 words marveling at it.

Thanks for reading.

Oh yeah, by the way, I do a podcast at [GNU World
Order](http://gnuworldorder.info). You should listen to it.

