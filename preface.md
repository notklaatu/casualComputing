# Preface

You like reading about technology? So do I. I follow lots of technical
blogs and I check in with tech news websites, and I read magazines,
the ones that are [online](http://opensource.com) with
[occasional](http://www.lulu.com/shop/opensourcecom/open-source-video-editing-for-beginners-kdenlive-series/ebook/product-21299080.html)
[book](http://www.lulu.com/shop/opensourcecom/open-source-yearbook-2015-hardcover/hardcover/product-22637091.html)
[releases](http://www.lulu.com/shop/search.ep?contributorId=1209059),
as well as [those](http://bsdmag.org) that go straight to
[print](http://linux-magazine.com). And of course there are plenty of
user manuals and technical specifications out there.

But there's just something about a casual *book*. And there just
aren't that many casual books about technology out there. Sometimes
you'll find a good one, like Hazel Russman's [The Charm of
Linux](http://www.lulu.com/shop/hazel-russman/the-charm-of-linux/paperback/product-21229401.html)
or Mike Gancarz's [Linux and the Unix
Philosophy](https://www.amazon.com/gp/offer-listing/1555582737/ref=dp_olp_all_mbc?ie=UTF8&condition=all),
or the enduring classic [The Cathedral and the
Bazaar](http://shop.oreilly.com/product/9781565927247.do), but aside
from those, books about technology are usually appropriately terse;
they cover a topic so that you can understand it.

I feel like there's a place, though, for tech books that aren't about
the technology, exactly, and are more about tech culture. Instead of
declaring that at the end of the book, you'll know how to do Foo, Bar,
and Baz, I think there's a place for books that just talk about Foo,
and maybe ruminate about why Bar is Bar, and maybe sneak in a
nonchalant lesson or two about Baz.

My book [Computing without
Compromise](https://www.smashwords.com/books/view/582453) was my first
book of that type.

The book you're holding in your hand now is my second.

The point of this book is not to educate, convince, or sell. It's a
book about life as an open source geek, and its intended audience is
primarily other open source geeks. That means there are some pieces
about Linux, another about generic Unix, a few about gaming, a few
about programming. It's a loose collection, a mix, and it's yours to
read and enjoy as you will.

And it is a book, not a blog and not a magazine. That's something I
like a lot; I like the experience of picking up a book, and seeing
exactly where it begins and where it ends. I can see the journey laid
out before me, and I can decide whether I want to embark on it or
not. You don't get that same experience when you start reading a blog
that could last two weeks or ten years, or when you search the
internet for articles about Linux or Open Source and get back millions
and millions of results with no indication of where to begin. I like
the journey that a book implies. I like picking up a book, looking at
it face to face, and agreeing that we, the book and I, are going to
spend time together. It's going to be quiet time, quality time, filled
with silent conversations as we agree and disagree over the topics
covered. It's going to be eye-opening and stimulating and it's going
to cause some interesting mental detours, and maybe open a few doors
to a few brand new ideas that the book didn't even mean to inspire.

Admittedly, that may or may not be your *exact* experience with *this*
particular book (although, one can hope...), but ideally you'll find something
enjoyable in this volume. It's not a technical manual, it's not a
critical view of electrical engineering or an analysis of firmware
implementation. You won't learn many hard facts or a bunch of useful
skills. But hopefully you'll enjoy this collection of random thoughts, ideas,
notes, and musings. If not, maybe you'll write your own book of
thoughts and ideas and musings. If you do, let me know; I'll happily
read it. Because I like casual tech books, and I think there ought to
be more.

For your approval, I submit this one.

Thanks for reading.

- Klaatu

